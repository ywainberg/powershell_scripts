$inputxml = @"
<Window x:Class="ClientDC_App.MainWindow"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:ClientDC_App"
        mc:Ignorable="d"
        Title="DCClient_Gamefly" Height="350" Width="525">

    <Grid>
        <Image x:Name="Logo" HorizontalAlignment="Left" Height="56" VerticalAlignment="Top" Width="63" Margin="444,6,0,0"/>


        <ComboBox HorizontalAlignment="Left" Margin="18,32,0,0" VerticalAlignment="Top" Width="111" Name="Gateway" AutomationProperties.HelpText="Please select Gateway Address">
            <ComboBoxItem>ipaddress</ComboBoxItem>
        </ComboBox>

        <Label Content="Gateway" HorizontalAlignment="Left" Margin="50,6,0,0" VerticalAlignment="Top"/>
        <CheckBox Content="Https" HorizontalAlignment="Left" Margin="159,39,0,0" VerticalAlignment="Top"/>


        <Label Content="GSS" HorizontalAlignment="Left" VerticalAlignment="Top" Margin="59,74,0,0"/>

        <ComboBox HorizontalAlignment="Left"  Margin="18,100,0,0"  VerticalAlignment="Top" Width="111" Name="GSS">
        </ComboBox>

        <TextBox Name="Proxy" HorizontalAlignment="Left" Height="22" Margin="18,156,0,0" Text="Enter Proxy Address" VerticalAlignment="Top" Width="111" FontSize="12"/>
        <Label Content="Customer" HorizontalAlignment="Left" VerticalAlignment="Top" Margin="309,10,0,0"/>

        <ComboBox HorizontalAlignment="Left" Margin="277,32,0,0" VerticalAlignment="Top" Width="114" Name="CustomerID">
        </ComboBox>

        <TextBox Name="UserID" HorizontalAlignment="Left" Height="22" Margin="277,156,0,0" Text="Enter User ID" VerticalAlignment="Top" Width="114" FontSize="12"/>



        <TextBox Name="HardwareID" HorizontalAlignment="Left" Height="22" Margin="277,100,0,0" Text="Enter Hardware ID" VerticalAlignment="Top" Width="114" FontSize="12"/>
        <Button Content="Run it" x:Name="Run" ClickMode="Press" HorizontalAlignment="Left" Margin="432,289,0,0" VerticalAlignment="Top" Width="75"/>
        <Button Content="Install jq" x:Name="JQ" ClickMode="Press" HorizontalAlignment="Left" Margin="10,245,0,0" VerticalAlignment="Top" Width="75"/>


    </Grid>
</Window>

"@
$inputXML = $inputXML -replace 'mc:Ignorable="d"','' -replace "x:N",'N'  -replace '^<Win.*', '<Window'
 
[void][System.Reflection.Assembly]::LoadWithPartialName('presentationframework')
[xml]$XAML = $inputXML
#Read XAML
 
    $reader=(New-Object System.Xml.XmlNodeReader $xaml)
  try{$Form=[Windows.Markup.XamlReader]::Load( $reader )}
catch{Write-Host "Unable to load Windows.Markup.XamlReader. Double-check syntax and ensure .net is installed."}
 
#===========================================================================
# Load XAML Objects In PowerShell
#===========================================================================
 
$xaml.SelectNodes("//*[@Name]") | %{Set-Variable -Name "WPF$($_.Name)" -Value $Form.FindName($_.Name)}
 
Function Get-FormVariables{
if ($global:ReadmeDisplay -ne $true){Write-host "If you need to reference this display again, run Get-FormVariables" -ForegroundColor Yellow;$global:ReadmeDisplay=$true}
write-host "Found the following interactable elements from our form" -ForegroundColor Cyan
get-variable WPF*
}
 
Get-FormVariables
 
#===========================================================================
# Actually make the objects work
#===========================================================================
 
#Sample entry of how to add data to a field
 
#$vmpicklistView.items.Add([pscustomobject]@{'VMName'=($_).Name;Status=$_.Status;Other="Yes"})
 
#===========================================================================
# Shows the form
#===========================================================================
write-host "To show the form, run the following" -ForegroundColor Cyan
# remove the invoke-webrequest
if (Get-Alias curl){
remove-item alias:curl -Confirm:$false
}



$WPFJQ.Add_Click({powershell.exe -command Find-Package jq |Install-Package -Force})
$Proxy = $WPFProxy.Text.ToString()

#### GSS LIST
$GSS_List = (curl http://ipaddress/api/Streaming/GetGssList |jq -r '[.[]]') |ConvertFrom-Json
foreach($gss in $GSS_List.ip){
    $WPFGSS.items.add($gss)
}



##### customer involvment 
$Customer_list = (curl http://ipaddress/api/Customers/GetAll |jq '[.[]]' |ConvertFrom-Json)

foreach($customer in $Customer_list.name){
    $WPFCustomerID.items.add($customer.trim())
   
}




    
     $GSS_ID = ($GSS_List|Where-Object{$_.ip -eq $WPFGSS.SelectedItem}).id
     $AREA_ID = ($GSS_List |Where-Object{$_.ip -eq $WPFGSS.SelectedItem}).areaId

     if ($WPFCustomerID.SelectedItemText -ne $null){
     $cus = ($Customer_list|Where-Object{$_.name -eq $WPFCustomerID.SelectedItem}).id


    switch ($WPFCustomerID.SelectedItem ) {

   
        
        'SAMSUNG' { $WPFRun.Add_Click({cmd.exe /c C:\myuser\myuserDC\myuserdc.exe -gw $WPFGateway.SelectedItem -sa "AREAID=$AREA_ID GSSID=$GSS_ID" -cus $cus -uid $WPFUserID.Text -hid $WPFHardwareID.Text})}
        'AMAZON'  { $WPFRun.Add_Click({cmd.exe /c C:\myuser\myuserDC\myuserdc.exe -gw $WPFGateway.SelectedItem -sa "AREAID=$AREA_ID GSSID=$GSS_ID" -cus $cus -uid $WPFUserID.Text -hid $WPFHardwareID.Text})}  
        'SWISSCOM'{ $WPFRun.Add_Click({cmd.exe /c C:\myuser\myuserDC\myuserdc.exe -gw $WPFGateway.SelectedItem -sa "AREAID=$AREA_ID GSSID=$GSS_ID" -cus $cus -uid $WPFUserID.Text -hid $WPFHardwareID.Text})}
        
         Default {$WPFRun.Add_Click({cmd.exe /c C:\myuser\myuserDC\myuserdc.exe -gw $WPFGateway.SelectedItem -sa "AREAID=$AREA_ID GSSID=$GSS_ID" -cus $cus})}
    } 


     }




########################

else{

  
    $WPFRun.Add_Click({cmd.exe /c C:\myuser\myuserDC\myuserdc.exe -gw $WPFGateway.SelectedItem -sa "AREAID=$AREA_ID GSSID=$GSS_ID" })

}




try {Get-Command jq.exe -ErrorAction Stop
}


         catch{
 if ($_.Exception.Message -match "The term 'jq.exe' is not recognized as the name of a cmdlet"){
  [System.Windows.MessageBox]::Show("jq is not installed ,please click on 'install jq' button and after install reopen the app.") }

}




$Form.ShowDialog() | out-null