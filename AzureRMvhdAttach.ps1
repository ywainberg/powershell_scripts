﻿ 
 $rgName = 'IT'
 $location = 'North Europe'
 $pipName = 'app1pip'
 $pip = New-AzureRmPublicIpAddress -Name $pipName -ResourceGroupName $rgName -Location $location -AllocationMethod Dynamic
 $subnet1Name = 'Vlan2'
 $nicname = 'app1_nic'
 $vnetName = 'SC_NET'
 $vnetAddressPrefix = '10.50.2.0/24'
 $subnetconfig = Get-AzureRmVirtualNetworkSubnetConfig -Name $subnet1Name -
 $vnet = Get-AzureRmVirtualNetwork -Name $vnetName -ResourceGroupName $rgName -Location $location -AddressPrefix $vnetAddressPrefix -Subnet $subnetconfig
 $nic = New-AzureRmNetworkInterface -Name $nicname -ResourceGroupName $rgName -Location $location -SubnetId $vnet.Subnets[0].Id -PublicIpAddressId $pip.Id
 $vmName = 'srv-azr-app1'
 $vmConfig = New-AzureRmVMConfig -VMName $vmName -VMSize 'Standard_A2'
 $computerName = $vmName
 $cred = (Get-Credential)
 $vm = Set-AzureRmVMOperatingSystem -VM $vmConfig -Windows -ComputerName $computerName -Credential $cred -ProvisionVMAgent -EnableAutoUpdate
 $vm = Add-AzureRmVMNetworkInterface -VM $vm -Id $nic.Id
 $osDiskName = 'app1'
 $osDiskUri = '{0}vhds/{1}{2}.vhd' -f $storageAcc.PrimaryEndpoints.Blob.ToString(), $vmName.ToLower(), $osDiskName
 $urlOfUploadedImageVhd = 'https://scstoragemain.blob.core.windows.net/vhds/srv-azr-app1_01.vhd'
 $vm = Set-AzureRmVMOSDisk -VM $vm -Name $osDiskName -VhdUri $osDiskUri -CreateOption fromImage -SourceImageUri $urlOfUploadedImageVhd -Windows
 $result = New-AzureRmVM -ResourceGroupName $rgName -Location $location -VM $vm 
 $result


