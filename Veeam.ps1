﻿$Node = 'srv-irg-veeam01'
$session = New-PSSession -ComputerName $Node -Name Veeam

Invoke-Command -Session $session -ScriptBlock {Add-PsSnapin -Name VeeamPSSnapIn}
Invoke-Command -Session $session -ScriptBlock {Get-VBRBackupRepository | select Name,Path | Sort-Object -Property Path}


