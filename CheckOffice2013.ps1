﻿$compil = Get-ADComputer -SearchBase "OU=Desktops,OU=Israel,OU=Workstations,DC=example,DC=com" -Filter * |Select-Object -ExpandProperty name
if(Test-Connection -ComputerName $compil -Quiet -Count 1) {

Invoke-Command -ComputerName $compil -ScriptBlock {Get-ItemProperty HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\* |where {$_.DisplayVersion -like "15.*" -and $_.InstallSource  -like "*Microsoft Office 15*" }| select PSComputerName, DisplayVersion | ft PSComputerName, DisplayVersion  -AutoSize -Wrap  }
} 