$GSS = '2.4.39.2'
$Title = '2.0.432.1'
$Trailer = '3.0.57'
$UI = '2.4.36.12'
$npcap = 'Npcap 0.86'


$Titlecheck = Test-Path C:\games\UIRedemption\Titles\Titles_$Title.ver
$trailercheck = Test-Path "C:\games\UIRedemption\Trailers\$Trailer.ver"
$GSSversion = [System.Diagnostics.FileVersionInfo]::GetVersionInfo("C:\myuser\GameStreamServer\Bin\GameServerManager.exe").FileVersion
$UIVersion = [System.Diagnostics.FileVersionInfo]::GetVersionInfo("C:\Games\UIRedemption\UI\UIRedemption.exe").FileVersion
$Npcap_version = (Get-Item HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\NpcapInst).GetValue('DisplayName')

<#$Games_list = "Bridge Constructor", "Dead Island" , "Legend of Kay Anniversary" , "REVOLVER360REACTOR" , "Thief" , "XCOM - Enemy Within"
foreach ($game in $Games_list){
   
    if ( Test-Path -Path C:\games\$game) {
        Write-Host "$game found -OK"
    }
    else {
        Write-Host "Alert - $game not found in "
    }
}#>

if ($GSSversion -eq $GSS) {
    write-host "GSS version is $GSS -- OK " -BackgroundColor DarkGreen }
else {write-host "GSS version is $GSSversion should be $GSS -- WRONG" -BackgroundColor Red}

if ($Titlecheck -eq 1){
    write-host "Titles Version $Title -- OK " -BackgroundColor DarkGreen
    }
else {
    $Current_title = (Get-Item C:\Games\UIRedemption\Titles\Titles_*).Name
    write-host "Titles version is $Current_title should be $Title -WRONG " -BackgroundColor Red
    }
if ($trailercheck -eq 1) {
    write-host "Trailer version $Trailer -- OK" -BackgroundColor DarkGreen
    }
else {
    $Current_trailer = (Get-Item C:\Games\UIRedemption\Trailers\*.ver).Name
    write-host "Trailer version is $Current_trailer should be $Trailer -- WRONG " -BackgroundColor Red
    }
if ($UIVersion -eq $UI) {
    write-host "UI version is $UI -- OK " -BackgroundColor DarkGreen
    }
else {
    write-host "UI version is $UIVersion should be $UI -- WRONG " -BackgroundColor Red 
    }
if ($Npcap_version -eq $npcap) {
    write-host "Npcap version is $npcap -- OK " -BackgroundColor DarkGreen
    }
else {
    write-host "Npcap version is $Npcap_version should be $npcap -- WRONG " -BackgroundColor Red
    }
$ip = (Get-NetIPAddress -InterfaceAlias 'Mng' -AddressFamily IPv4).IPv4Address

Write-Host "Host = $ip " -ForegroundColor Cyan