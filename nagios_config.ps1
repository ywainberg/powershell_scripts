﻿function myvmlist{


# Import the VMware commands into powershell console
$VCenter = Read-Host 'Enter Your VCenter Name'
Add-PSSnapin VMware*
#connect to the VCenter server
Connect-VIServer -Server $VCenter -WarningAction SilentlyContinue

#Get the required Parameters from the query

$vmlist = (Get-VMGuest -VM * | Select VmName, IPAddress ) 
$targertlist = 'C:\windows.txt'


foreach ($vm in $vmlist)
{
 $Name =  $vm.VmName.ToUpper()
 $IP =    $vm.IPAddress[0]
  

# add the desired content to the file

Add-Content -Value(

"define host{
        use             windows-server  ; Inherit default values from a template
        host_name       $Name           ; The name we're giving to this host
        alias           $Name           ; A longer name associated with the host
        address         $IP             ; IP address of the host
        }") -Path $targertlist}

            }
        
myvmlist