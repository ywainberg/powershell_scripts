﻿Select-AzureSubscription 'Microsoft Azure Enterprise MSDN Dev/Test' 

# VHD blob to copy #
$blobName = 'srv-azr-appbo1_01.vhd' 

# Source Storage Account Information #
$sourceStorageAccountName = 'scstoragemain'
$sourceKey = 'NE9dZsYXq/O/nK8Oi79jG4Y1KcepaerNTKY0ijgRIS32Vhfdna+7EY4kvq1efXfo+EHxrthhXVn4k8cwtKfyIg=='
$sourceContext = New-AzureStorageContext –StorageAccountName $sourceStorageAccountName -StorageAccountKey $sourceKey  
$sourceContainer = 'vhds'

# Destination Storage Account Information #
$destinationStorageAccountName = 'scclassic'
$destinationKey = 'A5Wm6cJML3N0P/Brn7gOM+7O3DI6DIXd91LdpSv+xO5eVxQ+DUcG7fc+oYZIHPjA5hSzr65vcRaGkMWOjPdF1A=='
$destinationContext = New-AzureStorageContext –StorageAccountName $destinationStorageAccountName -StorageAccountKey $destinationKey  

# Create the destination container #
$destinationContainerName = 'vhds'
#New-AzureStorageContainer -Name $destinationContainerName -Context $destinationContext 

# Copy the blob # 
$blobCopy = Start-AzureStorageBlobCopy -DestContainer $destinationContainerName `
                        -DestContext $destinationContext `
                        -SrcBlob $blobName `
                        -Context $sourceContext `
                        -SrcContainer $sourceContainer

while(($blobCopy | Get-AzureStorageBlobCopyState).Status -eq "Pending")
{
    Start-Sleep -s 30
    $blobCopy | Get-AzureStorageBlobCopyState
}