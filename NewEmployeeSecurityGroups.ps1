﻿[System.Reflection.Assembly]::LoadWithPartialName('Microsoft.VisualBasic') | Out-Null
$wshell = New-Object -ComObject Wscript.Shell
$smtp = 'example.com'
Import-Module Activedirectory
$Currentuser = [Microsoft.VisualBasic.Interaction]::InputBox('Please enter existing employee username from which you want to copy permission', 'Username') 
$Newuser = [Microsoft.VisualBasic.Interaction]::InputBox('Please enter new employee username ', 'Username') 
$manager = [Microsoft.VisualBasic.Interaction]::InputBox('Please enter Manager Username', 'Manager') 
$file = "$env:USERPROFILE\Permission.txt"
$Groups = Get-ADPrincipalGroupMembership $Currentuser | Select-Object Name | Format-Table -HideTableHeaders |Out-File "$env:USERPROFILE\Permission.txt"

$emailbody = @"
 <h1><b>New Employee permission review</b></h1>
 <h3>In the Attached file we listed the groups that your new employee will be added to</h3>
 <h3>The Group were selected upon similar user in your department</h3>
 <h3>or upon your request in the IT form for new employee that was sent to you</h3>
 <h3> style="font-size:120%>Please review and send us your approval</h3>
 <p style="font-size:120%><please forward this email to helpdesk-il@example.com></p><br>
 <p style="font-size:120%>For any question you might have, please contact IT department</p>

 
  
"@

 


Send-MailMessage -From $env:USERNAME@$smtp -To $manager@$smtp -Body $emailbody -Subject "Security Groups For new Employee $Newuser" -SmtpServer 172.16.0.90 -BodyAsHtml -Attachments $file