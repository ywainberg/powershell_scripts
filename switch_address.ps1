
# prompt for myuser credentials
$cred = Get-Credential

#Variable (example " ")
$gns_ip = ''
# ip id we want to move (example: eipalloc-5b4f6f6a)
$gns_ip_id = ''

# the instance id for current gns (example : i-ad705ca3)
$gns_inst_id =  ''

# the instance id we want to move to (example : i-ad705ca3)
$new_inst_id =  ''

# security group we all block except rdp (example : sg-9b1f3bee)
$sg_block_id = ''

# security group for gns (example : sg-9b1f3bee)
$sg_gns_id = ''

#define session option in case of self sign certificate
$so = New-PsSessionOption -SkipCACheck -SkipCNCheck

# stop services on original GNS
Invoke-Command -UseSSL -ComputerName $gns_ip -Credential $cred -SessionOption $so -ScriptBlock {Get-Service GnsProvisionService, W3SVC |ForEach-Object{Stop-Service $_}} 

#change security group for original gns to new ami gns with block sg
aws ec2 modify-instance-attribute --instance-id $gns_inst_id --groups $sg_block_id

#change security group  for new to original allow sg
aws ec2 modify-instance-attribute --instance-id $new_inst_id --groups $sg_gns_id

#disconnect ip from original gns
aws ec2 disassociate-address --association-id $gns_ip_id

#attach ip to new gns 
aws ec2 associate-address --instance-id $new_inst_id --allocation-id $gns_ip_id

start-sleep -Seconds 10
#start services on new gns
Invoke-Command -UseSSL -ComputerName $gns_ip -Credential $cred -SessionOption $so -ScriptBlock { 
   (Get-WmiObject win32_service | Where-Object{$_.Name -eq 'GnsProvisionService'}).ChangeStartMode('Automatic')
   (Get-WmiObject win32_service | Where-Object{$_.Name -eq 'W3SVC'}).ChangeStartMode('Automatic') 
    Get-Service GnsProvisionService, W3SVC |ForEach-Object{Start-Service $_}} 



