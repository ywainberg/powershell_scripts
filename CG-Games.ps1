

$bucket = "eaprod-ops"
$keyPrefix = "Games"
$localPath = "C:\Games"
$serverPath = "\\il-fs\Games\Patch - To Check\"
$smtp = new-object Net.Mail.SmtpClient("mailhost.ea.com")
$recp = "ops@ea.com"
$send = "Games-upload@ea.com"

 function get-game($game_id){
 
    $list[$game_id]
 }


 function upload-game ($game) {

        $aws_game = $game.Split('\') |Select-String 'zip'
        aws s3 cp $game s3://$bucket/$keyPrefix/$aws_game
        if ($? -ne 'True' ){
            Write-Host "Upload failed ,please try again"
            $smtp.Send($send, "gdinovich@ea.com", "GAME UPLOAD TO S3 FAILED", $keyPrefix + "Failed to upload") 
        }
        else {
            Write-Host "Upload $game is completed"
            $smtp.Send($send, $recp, "GAME UPLOAD TO S3 FINISHED SUCCESFULY", $keyPrefix + "Finish to upload") 
        }

    }
 function download-game ($game){

        Set-Location $localPath
        $aws_game = $game.Split('\') |Select-String 'zip'
        aws s3 cp s3://$bucket/$keyPrefix/$aws_game .
        if ($? -ne 'True' ){
            Write-Host "Download failed ,please try again"
        }
        else {
            Write-Host "Download $aws_game is completed"
        }

}




    try{

        $action_message = "Type action number: `n1. Upload`n2. Download`n`n Your Selection: "
        [int]$action = Read-Host -Prompt $action_message

    }
        catch {
            Write-Host "$_.Exception.ItemName"
            [int]$action = Read-Host -Prompt $action_message
        }

    try{
        if ([int]$action -eq 1) {
		$fs_list = Get-ChildItem $serverPath -Recurse -Filter *.zip -ErrorAction SilentlyContinue
		$list=($fs_list |Select-Object FullName -ExpandProperty FullName)
		}
        elseif ([int]$action -eq 2) {
		$aws_list=((aws s3 ls s3://$bucket/$keyPrefix/ --recursive |Select-String '.zip').line).Substring(38)
		$list=$aws_list
		}

        foreach($ls in $list){

        Write-Host $list.IndexOf($ls) .$ls

                }
        $game_message = "Type game number: `n: "
        [int]$game_number = Read-Host -Prompt $game_message

    }
        catch {
            Write-Host "$_.Exception.ItemName"
            [int]$game_number = Read-Host -Prompt $game_message
        }


$game_name=get-game($game_number)
if ($action -eq 1){upload-game($game_name)}
elseif($action -eq 2){download-game($game_name)}
else{"something is wrong"}