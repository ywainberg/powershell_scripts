﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2016 v5.2.125
	 Created on:   	27/07/2016 11:30
	 Created by:   	yaronw
	 Organization: 	example
	 Filename:     	
	===========================================================================
	.DESCRIPTION
		restart 'Transmission Engine' on srv-azr-gwbo1.
#>

$user = 'example\dev-gateway'
$passwordfile = "\\srv-irg-file01\companyshare\qa_pass.txt"
$keyfile = "\\srv-irg-file01\companyshare\AES.key"
$key = Get-Content $keyfile
$usercred = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $user, (Get-Content $passwordfile | ConvertTo-SecureString -Key $key)

Invoke-Command -ComputerName 'srv-azr-gwbo1' -ScriptBlock { Get-Service -Name 'Transmission Engine' | Restart-Service} -Credential $usercred
