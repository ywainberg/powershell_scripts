#Example for RemoteAccess software installation via System manager

#### Install & Configure RealVnc

msiexec /i "c:\install\VNC-Server-6.3.0-Windows-en-64bit.msi" transforms="no_mirror.mst" /qn

start-sleep 20
& "C:\Program Files\RealVNC\VNC Server\vnclicense.exe" -add "LiceneKey"


#### Install & Configure LogMeIn

msiexec /i "c:\install\LogMeIn.msi" /quiet DEPLOYID=deploymentkey INSTALLMETHOD=5 FQDNDESC=1



