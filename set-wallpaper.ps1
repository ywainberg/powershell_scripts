﻿Function Set-WallPaper
{
   param
   (
     [Object]
     $Value
   )

 Set-ItemProperty -path 'HKCU:\Control Panel\Desktop\' -name wallpaper -value $value
 rundll32.exe user32.dll, UpdatePerUserSystemParameters
}