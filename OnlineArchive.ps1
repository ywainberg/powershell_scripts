﻿$groupname = 'YourGroupName'
$dirsync = 'DirSyncServerName'
$RetentionPolicy = 'RetentionPolicyName'

#get-group members and set the online archive attribute
Get-ADGroupMember -Identity $groupname | ForEach-Object {Set-ADUser -Identity $_.SamAccountName -Replace @{msExchRemoteRecipientType = 3} }

#sync the changes to office 365
Invoke-Command -ComputerName $dirsync -ScriptBlock {C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -psconsolefile "C:\Program Files\Windows Azure Active Directory Sync\DirSyncConfigShell.psc1" -command "Start-OnlineCoexistenceSync"}

#wait for change to take effect
Start-Sleep -Seconds 60

#Login to Exchange Online Shell

$LiveCred = Get-Credential -Message 'Enter Global Admin credentials'
$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $LiveCred -Authentication Basic -AllowRedirection
Import-PSSession $Session -AllowClobber
Connect-MsolService -Credential $LiveCred

#Set the retention policy
Get-DistributionGroupMember $groupname |  ForEach-Object { Set-Mailbox -Identity $_.Identity -RetentionPolicy $RetentionPolicy  }