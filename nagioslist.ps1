﻿$nodelist = Get-Content 'C:\servers.txt'
$targertlist = 'C:\windows.txt'

foreach ($node in $nodelist)
{
 
Add-Content -Value(

"define host{
        use             windows-server  ;## Inherit default values from a template
        host_name       $node       ;## The name we're giving to this host
        alias           $node       ;## A longer name associated with the host
        address         172.31.41.53    ; ##IP address of the host
        }") -Path $targertlist}
