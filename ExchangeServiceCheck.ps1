﻿function EXSERVICE 

  {

  #Set Varibles
  $Node= 'srv-irg-ex02'
  $MailServer = '172.16.0.90'
  $Sender = 'corporate_alerts@example.com'
  $Name =  $Sender.Split('@')[0]
  $Recipient = 'yaronw@example.com'
  $Title = " 'Alert from' $Node"
  $ContentNode = "This is an automatically generated message.<br>. <br><strong>$Node 'is down</strong><br> "
  $ContentService = "This is an automatically generated message.<br>. <br><strong>$myservice 'is down on' $target</strong><br> "

  #Testing Connectivity to the target node
  $connectivity = Test-Connection -ComputerName $Node -Quiet
  if ($connectivity = $false)
  {
     Send-MailMessage -From "$Name <$Sender>" -To $Recipient -Subject $Title -Body $ContentNode `
     -Priority High -SmtpServer $MailServer -BodyAsHtml
     }
  
  else
  {
    
    $ServiceGroup = Get-WmiObject win32_service -computername $computer | Where-Object {$_.DisplayName.startswith('Microsoft Exchange ')} 
  
  

    foreach ($EXService in $ServiceGroup)
  
    {
      if($EXService.state -eq 'Stopped' -and $EXService.StartMode -eq 'Auto' )
      {
    
         Start-Service
         
        Send-MailMessage -From "$Name <$Sender>" -To $Recipient -Subject $Title -Body $ContentService `
        -Priority High -SmtpServer $MailServer -BodyAsHtml
      
   
    
      }
  
     
      else{
       
       
        exit
      }

      }

     }
  }
  EXSERVICE