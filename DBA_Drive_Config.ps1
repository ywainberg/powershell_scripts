try {
       $PageFile = Get-CimInstance -Query "Select * From Win32_PageFileSetting Where Name='d:\\pagefile.sys'" -ErrorAction Stop
       }
        catch { 
            Write-Warning -Message "Failed to query the Win32_PageFileSetting class because $($_.Exception.Message)"
                    }
            If($PageFile) {
                        try {
                            $PageFile | Remove-CimInstance -ErrorAction Stop 
                            restart-computer -force
                        } catch {
                            Write-Warning -Message "Failed to delete pagefile the Win32_PageFileSetting class because $($_.Exception.Message)"
                        }
                    } Else {
                      
$drive1 = Get-WmiObject win32_volume -Filter "DriveLetter = 'D:'"
$drive1.DriveLetter = 'T:'
$drive1.put() 

$drive1 = Get-WmiObject win32_volume -Filter "DriveLetter = 'E:'"
$drive1.DriveLetter = 'K:'
$drive1.put() 

$drive1 = Get-WmiObject win32_volume -Filter "DriveLetter = 'F:'"
$drive1.DriveLetter = 'D:'
$drive1.put() 

$drive1 = Get-WmiObject win32_volume -Filter "DriveLetter = 'G:'"
$drive1.DriveLetter = 'E:'
$drive1.put() 


$drive1 = Get-WmiObject win32_volume -Filter "DriveLetter = 'K:'"
$drive1.DriveLetter = 'F:'
$drive1.put() 

$drive1 = Get-WmiObject win32_volume -Filter "DriveLetter = 'T:'"
$drive1.DriveLetter = 'G:'
$drive1.put() 

#set page file
New-CimInstance -ClassName Win32_PageFileSetting -Property  @{Name= "$("G"):\pagefile.sys"}

                    }



