﻿# Set OU For query

$S2003 = Get-ADComputer -SearchBase 'ou=NY,ou=servers,dc=mydomain,dc=com' -Filter 'OperatingSystem -eq "Windows Server 2003"' 

foreach ($server in $S2003.name)

  {
  
  # Test Connection to check if alive
  $test = Test-Connection  -ComputerName $server -Quiet -ErrorAction SilentlyContinue
  
  # Get Server IP Address
  $NINFO = Get-WmiObject -ComputerName $server Win32_NetworkAdapterConfiguration | Where-Object { $_.IPAddress -ne $null } -ErrorAction SilentlyContinue
  
  # Create Lable for readble format 
  $object = New-Object psobject
  $object | Add-Member -TypeName Noteproperty Name($server) 
  $object | Add-Member -TypeName Noteproperty IpAddress($NINFO.IPAddress) 
  
  
  
  if($test -eq $true)
  
    {
    
    $object
    
     }
     
  else{
  
    Write-Host $server Ipaddress = $NINFO.IPAddress 'is Dead'
    
    }
  }
