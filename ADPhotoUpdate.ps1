﻿param (
	
	[Parameter(Mandatory = $true)]
	[string]$user,

	[Parameter(Mandatory = $true)]
	[string]$path


)

Set-ADUser $user -Replace @{thumbnailPhoto=([byte[]](Get-Content -Path $path -Encoding byte))}
