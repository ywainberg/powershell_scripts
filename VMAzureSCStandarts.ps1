<#
.Synopsis
  Create Machine on Azure in SC Standarts
.DESCRIPTION
  Create Machine on Azure in SC Standarts
  Default machine will be Standard_A2_v2
  * Creates the VM
  * Removes CD ROM / DVD
  * Created data disk
  * Removes Temporary Disk to E:
  * Adds to AD
.EXAMPLE
  Create-exampleAzureRMVM 'srv-azr-miky3'
.EXAMPLE
  Create-exampleAzureRMVM -vmName 'srv-azr-miky3' -vmSize 'Standard_A2_v2'
#>
function Create-AzureRMVM

{
  [CmdletBinding()]
  [Alias()]
  [OutputType([int])]
  Param
  (
  # Param1 help description
  [Parameter(Mandatory=$true,
  ValueFromPipelineByPropertyName=$false,
  Position=0)]
  $vmName,
  [Parameter(Mandatory=$false,
  ValueFromPipelineByPropertyName=$false,
  Position=1)]
  $vmSize = "Standard_A2_v2"
  )

  Begin
  {
  }
  Process
  {
  ## VMname, Computer Name, nic Name -- all will be infuenced by this ##

  $dataDiskSize = 10
  #### MOSTLY NOT FOR CHANGE ##
  ## Azure Location ##
  $locName = "northeurope"

  ## Resource Groups ##
  $rgName = "Application"
  $rgIT = "IT"

  ## Storage Account Name ##
  $stName = "scstandard"

  ## Network ##
  $vnetName = "SC_NET"
  $subnetName = 'vlan2'

  $nicName = $vmName + "nic"
  $compName = $vmName
  $osDiskName = $vmName + ".windowsvmosdisk"
  $dataDiskName = $vmName + ".datadisk"

  #### NETWORK ####
  $vnet = Get-AzureRmVirtualNetwork -Name $vnetName -ResourceGroupName $rgIT #$singleSubnet #-Location $locName #-AddressPrefix 10.0.0.0/16 -Subnet $singleSubnet
  $singleSubnet = Get-AzureRmVirtualNetworkSubnetConfig -VirtualNetwork $vnet -Name $subnetName

  #network Security group
  $nsg = Get-AzureRmNetworkSecurityGroup -Name 'AppServer_Security' -ResourceGroupName $rgName

  #Network Card
  $nic = New-AzureRmNetworkInterface -Name $nicName -ResourceGroupName $rgName -Location $locName -SubnetId $singleSubnet.Id -NetworkSecurityGroupId $nsg.Id # $vnet.Subnets[0].Id # -PublicIpAddressId $pip.Id


  #### STORAGE ####
  $storageAcc = Get-AzureRmStorageAccount -ResourceGroupName $rgIT -Name $stName

  $blobPath = "vhds/"+ $vmName + ".WindowsVMosDisk.vhd"
  $blobPathDataDisk = "vhds/"+ $vmName + ".WindowsVMdataDisk.vhd"
  $osDiskUri = $storageAcc.PrimaryEndpoints.Blob.ToString() + $blobPath
  $dataDiskVhdUri = $storageAcc.PrimaryEndpoints.Blob.ToString() + $blobPathDataDisk

  #### COMPUTE ####
  #local Admin
  $cred = Get-Credential -Message "Type the name and password of the local administrator account."

  $vm = New-AzureRmVMConfig -VMName $vmName -VMSize $vmSize 
  $vm = Set-AzureRmVMOperatingSystem -VM $vm -Windows -ComputerName $compName -Credential $cred -ProvisionVMAgent -EnableAutoUpdate
  $vm = Set-AzureRmVMSourceImage -VM $vm -PublisherName MicrosoftWindowsServer -Offer WindowsServer -Skus 2012-R2-Datacenter -Version "latest"
  $vm = Add-AzureRmVMNetworkInterface -VM $vm -Id $nic.Id
  $vm = Set-AzureRmVMOSDisk -VM $vm -Name $osDiskName -VhdUri $osDiskUri -CreateOption fromImage
  $vm = Add-AzureRmVMDataDisk -VM $vm -Name $dataDiskName -DiskSizeInGB $dataDiskSize -Lun 0 -VhdUri $dataDiskVhdUri -CreateOption Empty

  New-AzureRmVM -ResourceGroupName $rgName -Location $locName -VM $vm

  #### ACTIVE DIRECTORY EXTENSTION ####
  $string1 = '{"Name":"DOMAIN","User":"DOMAIN\\USER","Restart":"true","Options":"3","OUPath":"ou=Application,ou=azure,ou=servers,dc=com"}'
  $string2 = '{"Password":"PASSWORD"}'
  $resourceGroupName = 'Application'
  $location ='North Europe'
  Set-AzureRmVMExtension -ResourceGroupName $resourceGroupName -ExtensionType "JsonADDomainExtension" -Name "joindomain" -Publisher "Microsoft.Compute" -TypeHandlerVersion "1.0" -VMName $vmName -Location $location -SettingString $string1 -ProtectedSettingString $string2

  Restart-AzureRmVM -ResourceGroupName $resourceGroupName -Name $vmName
  Start-Sleep -Seconds 20

  ## Wait for Session ##
  $session = Create-Session $vmName


  Invoke-Command -Session $session -ScriptBlock {
  #### SETTING POROXY ####
  $reg = "HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings"
  $settings = Get-ItemProperty -Path $reg
  $settings.ProxyServer
  $settings.ProxyEnable
  Set-ItemProperty -Path $reg -Name ProxyServer -Value "PROXY:PORTS"
  Set-ItemProperty -Path $reg -Name ProxyEnable -Value 1
  }

  # disable DVD drive
  Invoke-Command -Session $session -ScriptBlock {
  Set-ItemProperty -Path HKLM:\SYSTEM\CurrentControlSet\Services\cdrom -Name Start -Value 4 -Type DWord
  }


  #http://clemmblog.azurewebsites.net/change-temporary-drive-azure-vm-use-d-persistent-data-disks/ 
  # move pagefile to OS disk
  Invoke-Command -Session $session -ScriptBlock {
  $CurrentPageFile = Get-WmiObject -Query 'select * from Win32_PageFileSetting'
  $CurrentPageFile.delete()
  Set-WMIInstance -Class Win32_PageFileSetting -Arguments @{name='c:\pagefile.sys';InitialSize = 0; MaximumSize = 0}
  }


  Restart-VM $session $vmName $rgName
  $session = Create-Session $vmName

  # change drive letter of scratch disk
  Invoke-Command -Session $session -ScriptBlock {
  $drive = Get-WmiObject -Class win32_volume -Filter "DriveLetter = 'd:'"
  Set-WmiInstance -input $drive -Arguments @{ DriveLetter='e:' }
  }




  # initialize & format data disk
  Invoke-Command -Session $session -ScriptBlock {
  # initialize data disk
  Get-Disk | Where partitionstyle -eq 'raw'|
  Initialize-Disk -PartitionStyle MBR -PassThru |
  New-Partition -AssignDriveLetter -UseMaximumSize |
  Format-Volume -FileSystem NTFS -NewFileSystemLabel 'Data Disk' -Confirm:$false
  }


  Restart-VM $session $vmName $rgName
  $session = Create-Session $vmName

  # move pagefile back to scratch disk
  Invoke-Command -Session $session -ScriptBlock {
  $CurrentPageFile = Get-WmiObject -Query 'select * from Win32_PageFileSetting'
  $CurrentPageFile.delete()
  Set-WMIInstance -Class Win32_PageFileSetting -Arguments @{name='e:\pagefile.sys';InitialSize = 0; MaximumSize = 0}
  }


  Restart-VM $session $vmName $rgName

  }
  End
  {
  }
}
function Restart-VM($session,[string]$vmName,[string]$resourceGroup)
{

  Write-Warning ('removing session ' + $vmName)

  Remove-PSSession -Session $session
  Write-Warning ('Restarting VM ' + $vmName)
  Restart-AzureRmVM -ResourceGroupName $resourceGroup -Name $vmName
  Start-Sleep -Seconds 20
}

function Create-Session([string]$vmName)
{

  ## Start the timer
  $timer = [Diagnostics.Stopwatch]::StartNew()
  Write-Warning -Message "Waiting for [$($vmName)] to become pingable..."
  ## Keep in the loop while the computer is not pingable
  while (-not (Test-Connection -ComputerName $vmName -Quiet -Count 1))
  {
  Write-Warning -Message "Waiting for [$($vmName)] to become pingable..."
  ## If the timer has waited greater than or equal to the timeout, throw an exception exiting the loop
  if ($timer.Elapsed.TotalSeconds -ge 600) # 10 minutes
  {
  throw "Timeout exceeded. Giving up on ping availability to [$vmName]"
  }
  ## Stop the loop every $CheckEvery seconds
  Start-Sleep -Seconds 10
  }

  ## When finished, stop the timer
  $timer.Stop()

  $session = $null
  do
  {
  Write-Warning -Message ('Trying to create session to ' + $compName)
  $session = New-PSSession -ComputerName $compName
  }
  while ($session -eq $null )
  Write-Warning ('session to ' + $compName + ' Created')
  return ,$session

}