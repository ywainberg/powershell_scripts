﻿
   $instance = Get-ADUser -Identity 'plameny' -Properties GivenName, SurName, SamAccountName
   $firstname = $instance.GivenName
	 $lastname =  $instance.SurName + '-su'
	 $Username =  $instance.SamAccountName + '-su'
	 
   $Name = $firstname + ' ' +  $lastname
   $UPN  = $Username + '@example.com'
	 $Description = 'cyberArk shadow user for Internal reporter'
	 . .\New-Password.ps1
   $password = New-Password -Length 12 -Type Complex -IncludeSymbols -IncludeNumbers -IncludeUppercaseCharacters -AlwaysStartWith Letter



NEW-ADUSER -GivenName $firstname -Surname $lastname -Instance $instance.SamAccountName -DisplayName $Name –SamAccountName $Username -AccountPassword ($password | ConvertTo-SecureString -AsPlainText -Force) -UserPrincipalName $UPN -Name $Name -Path 'OU=CyberArk,OU=Employees,DC=example,DC=com' -Enabled $true -Description $Description