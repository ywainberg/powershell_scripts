﻿



$rgName = 'IT'
$locName = 'NorthEurope'
$vnetName = 'SC_NET'

$subnetIndex = 1
$name = 'srv-azr-dbtemplate'

$vnet = Get-AzureRmvirtualNetwork -Name $vnetName -ResourceGroupName $rgName

$vm = New-AzureRmVMConfig -VMName $name -VMSize Standard_DS3
#vm | Set-AzureRmVMOSDisk -VhdUri 'https://scpremium.blob.core.windows.net/vhds/dbtemplatenew-20160218-141856.vhd' –Name $name -CreateOption attach -Windows -Caching ReadWrite
$vm | Set-AzureRmVMOSDisk -VhdUri 'https://scstandard.blob.core.windows.net/vhds/srv-azr-netd1_os.vhd' –Name $name -CreateOption attach -Windows -Caching ReadWrite

#$vm | Set-AzureRmVMOSDisk -VhdUri "https://scstandard.blob.core.windows.net/vhds/srv-azr-savlc-0.vhd" –Name $name -CreateOption attach -Linux -Caching ReadWrite

#Add-AzureRmVMDataDisk -VM $vm -VhdUri "https://scstandard.blob.core.windows.net/vhds/srv-azr-bopst1-01.vhd" –Name $name -Lun 0 -CreateOption attach 

$nicName = $name + '_nic'
$pipName = $name + '_pip'
$domName = $name
#$pip = New-AzureRmPublicIpAddress -Name $pipName –ResourceGroupName $rgName -DomainNameLabel $domName -Location $locName -AllocationMethod Dynamic
$nic = New-AzureRmNetworkInterface -Name $nicName –ResourceGroupName $rgName -Location $locName -SubnetId $vnet.Subnets[$subnetIndex].Id -PrivateIpAddress $privIP
$vm = Add-AzureRmVMNetworkInterface -VM $vm -Id $nic.Id


New-AzureRMVM -ResourceGroupName $rgName -Location $locName -VM $vm –Verbose 

<#

$RG = 'Application'
$VirtualMachine = Get-AzureRmVM -ResourceGroupName $RG -Name $name
Add-AzureRmVMDataDisk -VM $VirtualMachine -Name "disk1" -VhdUri "https://scstandard.blob.core.windows.net//vhds/srv-azr-app1_02.vhd" -LUN 0 -CreateOption attach
Update-AzureRmVM -ResourceGroupName $RG -Name $name -VM $VirtualMachine

#>