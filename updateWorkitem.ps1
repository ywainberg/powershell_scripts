param(

 

$RestUserName,

$RestPassword

)

 

$basicAuth = ("{0}:{1}" -f $RestUserName, $RestPassword)

$basicAuth = [System.Text.Encoding]::UTF8.GetBytes($basicAuth)

$basicAuth = [System.Convert]::ToBase64String($basicAuth)

$headers = @{Authorization=("Basic {0}" -f $basicAuth)}

 

[String] $CollectionURL = “$env:SYSTEM_TEAMFOUNDATIONCOLLECTIONURI“

[String] $BuildUrl = “$env:BUILD_BUILDURI“

[String] $project = “$env:SYSTEM_TEAMPROJECT“

[String] $BuildId = "$env:BUILD_BUILDID"

[String] $BuildNumber = "$env:BUILD_BUILDNUMBER"

 

 

Try

{

 

$WorkItemAssociatedURL = $collectionURL + $project + "/_apis/build/builds/" + $BuildId + "/workitems?api-version=2.0"

$ResponseJSON = Invoke-RestMethod -Uri $WorkItemAssociatedURL -ContentType "application/json" -headers $headers -Method GET

 

$CountWorkitems = $ResponseJSON.count

$WorkitemUrlArray = $ResponseJSON.value

 

for($i = 0; $i -lt $CountWorkitems ; $i++)

{

$body =

'[

{

"op": "add",

"path": "/fields/Microsoft.VSTS.Build.IntegrationBuild",

"value":' + "$BuildNumber" +'

}

]'

 

$WorkitemUpdateURL = $WorkitemUrlArray[$i].url + "?api-version=1.0"

 

Invoke-RestMethod -Uri $WorkitemUpdateURL -Body $body -ContentType "application/json-patch+json" -headers $headers -Method Patch

}

}

 

Catch

{

Write-Host "No work item associated with this build. Kindly check the changesets"

}