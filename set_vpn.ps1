﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2016 v5.2.122
	 Created on:   	21/06/2016 16:19
	 Created by:   	yaronw
	 Organization: 	example
	 Filename:     	
	===========================================================================
	.DESCRIPTION
		A description of the file.
#>
$debug = $false

## Check if we are running under 32 or 64 bit system 
if (Test-Path "C:\Program Files (x86)\")
{
	$folder_path = "C:\Program Files (x86)\"
}
else
{
	$folder_path = "C:\Program Files\"
}
#Set log file to TEMP directory. 
$logfile = $env:temp + "\add_site.log"

function check_run
{
	return (Test-Path $logfile)
}

function sendEmail
{
	send-mailmessage -to "Guy <guym@example.com>" -from "VPN Script <vpn_script@example.com>" -subject "Add VPN script - notification" -smtpServer 172.16.0.235 -Attachment "$logfile"
}

function quit_and_show($message)
{
	log_message($message)
	sendEmail
	Exit 1
}

function log_message($message)
{
	$now = Get-Date -format "dd-MMM-yyyy HH:mm:ss"
	$now + " | " + $message | out-file $logfile -append
	if ($debug)
	{
		Write-Host $message
	}
}

function display_message ($message)
{
	#Log message to file also .. 
	log_message($message)
}

function Test-Administrator
{
	$user = [Security.Principal.WindowsIdentity]::GetCurrent();
	(New-Object Security.Principal.WindowsPrincipal $user).IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)
}

function backup_VPN_Files()
{
	try
	{
		#Copy trac.deafult to backup 
		Copy-Item $tracDef_File $tracDef_File".backup"
		#Copy trac.config to backup 
		Copy-Item $tracCon_File $tracCon_File".backup"
		return $true
	}
	catch
	{
		#There is error ... 
		$ErrorMessage = $_.Exception.Message
		#Log it .. 
		log_message("Error when trying to backup files - " + $ErrorMessage)
		return $false
	}
	
}
function stop_service
{
	try
	{
		#Check if we have this service running 
		if (Get-Process TrGUI -ErrorAction SilentlyContinue)
		{
			#Stop TrGUI process 
			Stop-Process -Name TrGUI
		}
		else
		{
			log_message("Process TrGUI is not running")
		}
		
		#Check if serevice is running 
		$arrService = Get-Service -Name "TracSrvWrapper"
		if ($arrService.Status -ne "Stopped")
		{
			log_message("TracSrvWrapper service is on state - " + $arrService.Status + " Trying to stop")
			#Stop the check point service 
			Stop-Service TracSrvWrapper
		}
		else
		{
			log_message("TracSrvWrapper is already stopped")
		}
		
		return $true
	}
	catch
	{
		#There is error ... 
		$ErrorMessage = $_.Exception.Message
		#Log it .. 
		log_message("Error when trying to stop service - " + $ErrorMessage)
		return $false
	}
	
}

function start_service
{
	try
	{
		#Check if service is Stopped
		$arrService = Get-Service -Name "TracSrvWrapper"
		if ($arrService.Status -eq "Stopped")
		{
			#restart the checkpoint service 
			Start-service TracSrvWrapper
		}
		else
		{
			log_message("TracSrvWrapper service is not stopped. Status is - " + $arrService.Status)
		}
		
		#check process is not running 
		if (-not (Get-Process TrGUI -ErrorAction SilentlyContinue))
		{
			#restart TrGui
			Start-Process $trGui_Process
		}
		else
		{
			log_message("Process TrGUI is already running")
		}
		
		#Sleep for 5 seconds
		Start-Sleep -Seconds 5
		
		return $true
	}
	catch
	{
		#There is error ... 
		$ErrorMessage = $_.Exception.Message
		#Log it .. 
		log_message("Error when trying to start service - " + $ErrorMessage)
		return $false
	}
}
function rollback
{
	#running rollback .. 
	log_message("Running rollback... ")
	
	#Check that we have the files backup
	if (-not (Test-Path $tracDef_File".backup"))
	{
		quit_and_show("Error - Unable to rollback.`n`nNo $tracDef_File backup file.")
	}
	if (-not (Test-Path $tracCon_File".backup"))
	{
		quit_and_show("Error - Unable to rollback.`n`nNo $tracCon_File backup file.")
	}
	
	#Stop the service , give it 3 tests, wait 5 seconds between each test . 
	log_message("Stopping VPN services")
	$counter = 1
	while (-not (stop_service))
	{
		log_message("Trying to stop service - counter = $counter")
		Start-Sleep -Seconds 5
		$counter++
		if ($counter -gt 3)
		{
			#We are unable to continue .. Exit and show error message to the user 
			quit_and_show("Unable to stop VPN service")
		}
	}
	log_message("Stopped VPN services successfully")
	
	log_message("Copy backup files")
	try
	{
		#Copy trac.deafult from backup 
		Copy-Item $tracDef_File".backup" $tracDef_File
		#Copy trac.config from backup 
		Copy-Item $tracCon_File".backup" $tracCon_File
		
		quit_and_show("Successfully rollback configuration files.`n`nPlease restart your machine.`nThank you")
	}
	catch
	{
		#There is error ... 
		$ErrorMessage = $_.Exception.Message
		#Log it .. 
		quit_and_show log_message("Error when trying to restore files - " + $ErrorMessage)
	}
}

$hostname = $env:computername
$authentication_password_string = '<PARAM authentication_method="username-password"></PARAM>'
$authentication_certificate_string = '<PARAM authentication_method="p12-certificate"></PARAM>'
$tracDef_File = $folder_path + "CheckPoint\Endpoint Connect\trac.defaults"
$trGui_Process = $folder_path + "CheckPoint\Endpoint Connect\TrGUI.exe"
$tracCon_File = $folder_path + "CheckPoint\Endpoint Connect\trac.config"
$New_Site_Info = @"
<SITE name="New VPN to Office" user="USER">
		<FROM_GW>
			<PARAM client_enabled="true"></PARAM>		
			<PARAM trac_upgrade_url="/CSHELL/"></PARAM>		
			<PARAM default_authentication_method="client_decide"></PARAM>		
			<PARAM gw_bc_mode="false"></PARAM>		
			<PARAM hello_protocol_ver="100"></PARAM>		
			<PARAM natt_transport_port="4500"></PARAM>		
			<PARAM server_cn="*.example.com"></PARAM>		
			<PARAM neo_upgrade_mode="ask_user"></PARAM>		
			<PARAM internal_ca_fingerprint="CROW HARM AN SING COAL LIND TRAG BAT CLUB THIN TUNG HA"></PARAM>		
			#CLIENT_VERSION_PH#
			<PARAM gw_hostname="212.25.122.125"></PARAM>		
			<PARAM gw_ipaddr="212.25.122.125"></PARAM>		
			<PARAM transport="Auto-Detect"></PARAM>		
			<PARAM cookie_name="CPCVPN_SESSION_ID"></PARAM>		
			<PARAM last_connect_time="Tue Jun 14 10:57:06 2016"></PARAM>		
			<PARAM vpnd_ipaddr="212.25.122.125"></PARAM>		
			<PARAM conn_type="IPSec"></PARAM>		
			<PARAM tcpt_transport_port="443"></PARAM>		
			<PARAM ccc_fingerprint="TUSK BOCK STAG RIM GREY SHUT KIRK FLIT GROW MAIN DEBT NOAH"></PARAM>		
			<PARAM scuiapi_connect_time="1465891026"></PARAM>		
			<PARAM hotspot_detection_enabled="" client_decide="true"></PARAM>		
			<PARAM last_connect_time_interval="972828"></PARAM>		
			<PARAM certificate_url="/clients/cert/"></PARAM>
		</FROM_GW>	
		<FROM_USER>
			<PARAM is_saa="false"></PARAM>		
			<PARAM gw_hostname="212.25.122.125"></PARAM>		
			<PARAM gw_ipaddr="212.25.122.125"></PARAM>		
			<PARAM vpnd_ipaddr="212.25.122.125"></PARAM>		
			#AUTHENTICATION_PH#	
			#CERTIFICATE_PH#
			<PARAM user_upgrade_mode="ASK_USER"></PARAM>		
			<PARAM display_name="New VPN to Office"></PARAM>
		</FROM_USER>
	</SITE>
"@
#check if need to roolback... 
foreach ($arg in $args)
{
	if ($arg -eq "rollback")
	{
		log_message("Starting rollback operation ...")
		rollback
		#Roolback finished successfuly .. 
		sendEmail
		Exit 1
	}
}

#Check if already run .. 
if (check_run)
{
	#already run ... just exit .. 
	Exit 2
}

#Starting..
log_message("Starting VPN add site script: machine name = $hostname")

#Check for admin privilege
if (Test-Administrator)
{
	log_message("Test for admin - true")
}
else
{
	quit_and_show("This script must be run as administrator.")
}

#Starting ... Show message to user
display_message("Hello,`n`nexample IT is about to update your VPN site list.`nThis process will disconncet you from the Corporate VPN.`nOnce finished, you will be able to reconnect.`n`nThank you")

#Log .. 
log_message("Starting backup process")

#Check we have the relevant files .. 
log_message("Check for relevant files")
if (-not (Test-Path $tracDef_File) -or -not (Test-Path $tracCon_File))
{
	log_message("Unable to find file $tracDef_File or file $tracCon_File ")
	#We are unable to continue .. Exit and show error message to the user 
	quit_and_show("Unable to find VPN files")
}
log_message("Files exists")
#Starting ... Creating backup of the files ... 
log_message("Start with backup of files")
try
{
	$backup_res = backup_VPN_Files
	$tracDef_Cont = Get-Content $tracDef_File
	#Change trac file 
	$tracDef_Cont[0] = $tracDef_Cont[0].replace("1", "0")
	
}
catch
{
	log_message($_.Exception.Message)
	#We are unable to continue .. Exit and show error message to the user 
	quit_and_show("Unable to change VPN Trac file.")
}
log_message("Backup completed successfully")

#Stop the service , give it 3 tests, wait 2 seconds between each test . 
log_message("Trying to stop VPN services")
$counter = 1
while (-not (stop_service))
{
	log_message("Trying to stop service - counter = $counter")
	Start-Sleep -Seconds 5
	$counter++
	if ($counter -gt 3)
	{
		#We are unable to continue .. Exit and show error message to the user 
		quit_and_show("Unable to stop VPN service")
	}
}
log_message("VPN services stopped")

#write the file - trac.defaults
log_message("write trac.defaults file")
try
{
	$writer = [System.IO.StreamWriter]$tracDef_File
	foreach ($line  in $tracDef_Cont)
	{
		$writer.Write($line)
		$writer.Write("`n")
	}
	$writer.close()
}
catch
{
	#There is error ... 
	$ErrorMessage = $_.Exception.Message
	#Log it .. 
	quit_and_show("Error while writing file trac.defaults - " + $ErrorMessage)
}
finally
{
	if ($writer -ne $null)
	{
		$writer.dispose()
	}
}

#restart the checkpoint service 
log_message("Restart VPN services")
try
{
	if (-not (start_service))
	{
		#We will try one more time to start the service .. 
		if (-not (start_service))
		{
			#Error .. 
			quit_and_show("Unable to start VPN service.")
		}
	}
}
catch
{
	#Lets log the error message
	log_message("Unable to restart VPN service " + $_.Exception.Message)
	#Again, one more try to restart the service .. 
	#Sleep for 5 seconds
	Start-Sleep -Seconds 5
	try
	{
		if (start_service -eq $false)
		{
			#Error .. 
			quit_and_show("Unable to start VPN service.")
		}
	}
	catch
	{
		#Lets log the error message
		log_message("Unable to restart VPN service " + $_.Exception.Message)
		#Error .. 
		quit_and_show("Unable to start VPN service.")
	}
}
log_message("VPN services restart successfully")

#Stop the service , give it 3 tests, wait 5 seconds between each test . 
log_message("Stopping VPN services")
$counter = 1
while (-not (stop_service))
{
	log_message("Trying to stop service - counter = $counter")
	Start-Sleep -Seconds 5
	$counter++
	if ($counter -gt 3)
	{
		#We are unable to continue .. Exit and show error message to the user 
		quit_and_show("Unable to stop VPN service")
	}
}
log_message("Stopped VPN services successfully")

#Now we can read the trac.config file 
#Read the certificate and the version lines ... 
log_message("Read trac.config file")
$tracCon_Cont = Get-Content $tracCon_File

#Check if we are using certificate or username and password ... 
foreach ($line in $tracCon_Cont)
{
	if ($line -like '*username-password*')
	{
		$New_Site_Info = $New_Site_Info.replace("#AUTHENTICATION_PH#", $authentication_password_string)
		log_message("Found VPN site - username-password")
	}
	elseif ($line -like '*p12-certificate*')
	{
		$New_Site_Info = $New_Site_Info.replace("#AUTHENTICATION_PH#", $authentication_certificate_string)
		log_message("Found VPN site - certificate")
	}
}

#If using something else to authenticate ... 
$New_Site_Info = $New_Site_Info.replace("#AUTHENTICATION_PH#", "")

foreach ($line in $tracCon_Cont)
{
	#Check if you can replace the certificate file .. 
	if ($line -like '*certificate_path*')
	{
		$New_Site_Info = $New_Site_Info.replace("#CERTIFICATE_PH#", $line)
		log_message("Replacing certificate path line - $line")
	}
	#Check if can change the version ... 
	elseif ($line -like '*client_version*')
	{
		$New_Site_Info = $New_Site_Info.replace("#CLIENT_VERSION_PH#", $line)
		log_message("Add Client version - $line")
	}
}

#Check if you didn't replace the certificate line or the client version line , remove the hash tag .. 
$New_Site_Info = $New_Site_Info.replace("#CERTIFICATE_PH#", "")
$New_Site_Info = $New_Site_Info.replace("#CLIENT_VERSION_PH#", "")

#add the new site to trac.config file
#We will read all the lines and then add the string after the </SITE> part

$i = 0
do
{
	if ($tracCon_Cont[$i] -like '*</SITE>*')
	{
		$tracCon_Cont[$i] += "`n"
		$tracCon_Cont[$i] += $New_Site_Info
	}
	$i++
}
while ($tracCon_Cont[$i])
log_message("Done setting trac.config file .. Write to file")

#write the file - trac.config
try
{
	$writer = [System.IO.StreamWriter]$tracCon_File
	foreach ($line  in $tracCon_Cont)
	{
		$writer.WriteLine($line)
	}
	$writer.close()
}
catch
{
	#Lets log the error message
	log_message("Failed to write trac.config file - " + $_.Exception.Message)
	#Error .. 
	quit_and_show("Error occurred while writing trac.config file.`n`nPlease run recover to rollback configuration changes")
}
finally
{
	if ($writer -ne $null)
	{
		$writer.dispose()
	}
}

#restart the checkpoint service 
log_message("Finished with configuration, restarting VPN services")
try
{
	if (-not (start_service))
	{
		#We will try one more time to start the service .. 
		if (-not (start_service))
		{
			#Error .. 
			quit_and_show("Unable to start VPN service - Setup completed successfully.")
		}
	}
}
catch
{
	#Lets log the error message
	log_message("Unable to start VPN service - Setup completed successfully." + $_.Exception.Message)
	#Again, one more try to restart the service .. 
	#Sleep for 5 seconds
	Start-Sleep -Seconds 5
	try
	{
		if (start_service -eq $false)
		{
			#Error .. 
			quit_and_show("Unable to start VPN service - Setup completed successfully.")
		}
	}
	catch
	{
		Write-Host "In this place i am .."
		#Lets log the error message
		log_message("Unable to start VPN service - Setup completed successfully." + $_.Exception.Message)
		#Error .. 
		quit_and_show("Unable to start VPN service - Setup completed successfully.")
	}
}

#Done , finished successfully 
display_message("Configuration completed successfully.`nThe new VPN site was added.`n`nPlease email IT the installation log file.`n`nThank you.")

#Send email .. 
sendEmail
Exit 0
#Done

