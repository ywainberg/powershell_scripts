Login-AzureRmAccount

$VMNAME = 'GNS1'
$RG = "GNS-Cluster"
$id = "/subscriptions/19c97cb3-028b-4c17-90af-de2186d4e29c/resourceGroups/GNS-Cluster/providers/Microsoft.Network/networkInterfaces/GNS1_Secondary"
$VirtualMachine = Get-AzureRmVM -ResourceGroupName $RG -Name $VMNAME
$VirtualMachine.NetworkProfile.NetworkInterfaces.Item(0).Primary = $true
Update-AzureRmVM -ResourceGroupName $RG -VM $VirtualMachine
Add-AzureRmVMNetworkInterface -VM $VirtualMachine -Id $id
Update-AzureRmVM -ResourceGroupName $RG -VM $VirtualMachine

