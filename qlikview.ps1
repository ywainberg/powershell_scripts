Import-Module sqlps -WarningAction SilentlyContinue
$output = 'C:\\QlikView\\CSV\\ConnectedDevices.xml'
$query = Invoke-Sqlcmd -ServerInstance bi-db -Username 'test_user' -Password 'uAAbcTu5RSknUCyUOGRt' -Query "

SELECT '<UNIQUE_ID>', GSH_UNIQUE_ID, '</UNIQUE_ID>', GSH_CONNECTED_DEVICE

FROM [WWD_OTT].[dbo].[SESSIONS_HISTORY_TB]

"
$query | Export-Clixml $output
