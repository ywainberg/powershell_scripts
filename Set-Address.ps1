﻿function Set-Address
{
  param(
  
    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [String]$ip,
    
    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [string]$Default_Gateway ,
    
    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [string[]]$DNS_Addreess
    
    
  )
  
  $IntAlias = 'Ethernet'
  
  New-NetIPAddress -IPAddress $ip -DefaultGateway  $Default_Gateway -InterfaceAlias $IntAlias -PrefixLength '24' | Out-Null
  
  Set-DnsClientServerAddress -InterfaceAlias $IntAlias -ServerAddresses $DNS_Addreess | Out-Null

  $NewIP = (Get-NetIPAddress -InterfaceAlias $IntAlias |Select-Object -ExpandProperty IPv4Address)
  $NewDNS = (Get-DnsClientServerAddress -InterfaceAlias $IntAlias -AddressFamily IPv4).ServerAddresses
  $newDG = (Get-NetIPConfiguration |Select-Object -ExpandProperty IPv4DefaultGateway).nexthop
  
 
  
  $systemNet = New-Object -TypeName PSObject
  
  Add-Member -InputObject $systemNet -MemberType NoteProperty -Name 'IP Address' -Value $NewIP
  Add-Member -InputObject $systemNet -MemberType NoteProperty -Name 'DNS Address' -Value $NewDNS
  Add-Member -InputObject $systemNet -MemberType NoteProperty -Name 'Default Gateway' -Value $newDG
  
  $systemNet | Format-List
  
 
}
