﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2016 v5.2.122
	 Created on:   	14/06/2016 10:32
	 Created by:   	yaronw
	 Organization: 	example
	 Filename:     	
	===========================================================================
	.DESCRIPTION
		A description of the file.
#>
function New-VmRMFromImage
{
	
<#
      .SYNOPSIS
      Create Azure vm from Image

      .DESCRIPTION
      Create an Azure vm from a captured image or Azure image

      .PARAMETER Image
      image name to choose from :Windows server 2012R2, Latest version of Centos

      .PARAMETER vmname
      desired vm name

      .PARAMETER vmsize
      vm size to choose from Medium=3.5GB + 2Core , Large=4GB + 4Core, DS3=14GB + 4Core(SSD support)

      .PARAMETER subsc
      Subscription Name (if have more than one)
      
      .PARAMETER staccount
      Storage account 

      .PARAMETER srcgroup
      Resource Group Name

      .PARAMETER vnetname
      Virtual network name(predefined)

      .PARAMETER subnetname
      Select Your desired Vlan 

      .PARAMETER cred
      vm local admin credentials.Need to add(Get-Credentials)

      .EXAMPLE
      New-VmRMFromImage -image Centos -vmname srv-linux -vmsize Medium -subsc c641763a-1467-4bg5-rt1c-se9a5f6241bc -staccount scclassic -svcname srv-linux -vnetname 'vnet' -SubnetName Vlan2 -cred(Get-Credential)
			
			In This Example we create a Centos linux vm with 2 core and 3.5GB RAM, placed in Vlan 2 the in sclassic storage account	
      
			.EXAMPLE
       New-VmRMFromImage -label '2012R2' -vmname 'srv-app1' -vmsize Standard_DS3 -subsc 'c641763a-1467-4bg5-rt1c-se9a5f6241bc' -staccount 'scclassic' -svcname 'srv-sql' -vnetname 'vnet' -SubnetName 'Vlan1' -cred(Get-Credential) -Verbose
			
			In this Example we create a VM from a 2012R2 image ,set a size of 14GB RAM and 4 core ,placed it in Vlan1 in scclassic storage account
      
      .NOTES
	
			You need access to azure powershell 
			Run Login-AzureRmAccount before running this function


#>
	
	[CmdLetBinding(SupportsShouldProcess = $true)]
	param (
		
		
		[Parameter(Position = 0)]
		[ValidateSet('2012-R2-Datacenter', 'Centos')]
		[string]$image,
		
		[Parameter(Mandatory = $true)]
		[string]$vmname,
		
		[Parameter(Mandatory = $true)]
		[string]$srcgroup,
		
		[Parameter(Mandatory = $true,
							 HelpMessage = 'A2=3.5GB + 2Core , A2=4GB + 4Core, DS3=14GB + 4Core(SSD support)')]
		[ValidateSet('Standard_DS3', 'Standard_A2', 'Standard_A2')]
		[string]$vmsize,
		
		[Parameter(HelpMessage = 'Please specify size in GB')]
		[int]$datadisk,
		
		[Parameter(Mandatory = $true)]
		[string]$subsc,
		
		[Parameter(Mandatory = $true)]
		[string]$staccount,
		
		[Parameter(Mandatory = $true)]
		[ValidateSet('sc_net')]
		[string]$vnetname,
		
		[Parameter(Mandatory = $true)]
		[ValidateSet('Vlan1', 'Vlan2')]
		[string]$SubnetName,
		
		[Parameter(Mandatory = $true)]
		[ValidateNotNull()]
		[System.Management.Automation.PSCredential][System.Management.Automation.Credential()]
		$cred
		
	)
	
	
	
	
	
	$Storage = Get-AzureRmStorageAccount -ResourceGroupName $srcgroup | Where-Object { $_.StorageAccountName -match $staccount }
	$location = $Storage.location
	$Disk = 'OS'
	$Diskname = $vmname + '_' + $Disk
	$blobPath = "vhds/$Diskname.vhd"
	$datablobPath = "vhds/$vmname" + "_data.vhd"
	$osDiskUri = $Storage.PrimaryEndpoints.Blob.ToString() + $blobPath
	$dataDiskUri = $Storage.PrimaryEndpoints.Blob.ToString() + $datablobPath
	$SubnetList = Get-AzureRmVirtualNetwork | Select-Object Name, @{ Name = 'AddressSpace'; Expression = { $_.AddressSpace.AddressPrefixes } }, @{ Name = 'SubnetName'; Expression = { $_.Subnets.Name } }, @{ Name = 'SubnetAddressPrefix'; Expression = { $_.Subnets.AddressPrefix } }, @{ Name = 'SubnetId'; Expression = { $_.Subnets.Id } }
	$SubnetID = ((Get-AzureRmVirtualNetwork -Name $vnetname -ResourceGroupName $srcgroup | Select-Object Subnets).subnets | Where-Object{ $_.Name -like $SubnetName }).Id
	$NIC = New-AzureRMNetworkInterface –Name $vmname –ResourceGroupName $srcgroup –Location $location –SubnetID $SubnetID
	
	
	switch ($image)
	{
		
		'2012-R2-Datacenter'{
			
			$images = Get-AzureRmVMImage -Location $location -PublisherName 'MicrosoftWindowsServer' -Offer 'WindowsServer' -Skus $image | Sort-Object -Descending -Property PublishedDate
			$AzureVM = New-AzureRMVMConfig –VMName $vmname –VMSize $vmsize
			$AzureVM = Add-AzureRmVMNetworkInterface -VM $AzureVM -Id $NIC.Id
			Set-AzureRmVMOperatingSystem -VM $AzureVM -Windows -ComputerName $vmname -Credential $cred -ProvisionVMAgent -EnableAutoUpdate
			Set-AzureRmVMSourceImage -VM $AzureVM -PublisherName $images[0].PublisherName -Offer $images[0].Offer -Skus $images[0].Skus -Version $images[0].Version
			Set-AzureRmVMOSDisk -VM $AzureVM -VhdUri $osDiskUri -name $DiskName -CreateOption fromImage -Caching ReadWrite
			if ($datadisk){ Add-AzureRmVMDataDisk -VM $AzureVM -Name "data1"  -DiskSizeInGB $datadisk -Lun 0 -VhdUri $dataDiskUri -CreateOption Empty}
			New-AzureRMVM –ResourceGroupName $srcgroup –location $location –VM $AzureVM
		}
		
		'Centos' {
			$AzureVM = New-AzureRMVMConfig –VMName $vmname –VMSize $vmsize
			$sku = Get-AzureRMVMImageSKU –location 'northeurope' –PublisherName 'openlogic' -Offer 'CentOS' | Select-Object -ExpandProperty skus -Last 1
			$AzureVM = Set-AzureRmVMOperatingSystem -VM $AzureVM -ComputerName $VMname -Credential $cred -linux
			$AzureVM = Set-AzureRmVMSourceImage -VM $AzureVM -PublisherName 'openlogic' -Offer 'CentOS' -Skus $sku -Version "latest"
			$AzureVM = Add-AzureRmVMNetworkInterface -VM $AzureVM -Id $NIC.Id
			$AzureVM = Set-AzureRmVMOSDisk -VM $AzureVM -VhdUri $osDiskUri -name $DiskName -CreateOption fromImage -Caching $Caching
			if ($datadisk) { $AzureVM = Add-AzureRmVMDataDisk -VM $AzureVM -Name $vmname + "_data1.vhd" -Caching 'ReadOnly' -DiskSizeInGB $datadisk -Lun 0 -VhdUri $dataDiskUri -CreateOption Empty }
			New-AzureRMVM –ResourceGroupName $srcgroup –location $location –VM $AzureVM
		}
		
	}
	

	
}



	