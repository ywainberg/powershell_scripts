﻿Import-Module ActiveDirectory
Import-Module importexcel 
Get-ADUser -Properties givenname, surname, displayname, Department, description, mail, enabled, passwordlastset, office `
 -SearchBase "ou=employees,dc=example,dc=com" -Filter * `
| where{ $_.mail -ne $null -and $_.Enabled -eq $true -and $_.passwordlastset -gt (Get-Date).AddDays(-60) } `
| select givenname, surname, displayname, Department, description, mail ,office | Export-Excel c:\users\userlist.xlsx -Show