﻿function New-User{
  <#
      .SYNOPSIS
      New-HybridUser create a user ready for office 365 in hybrid configuration.
      .DESCRIPTION
      This Script is intended to create a user ready for office 365 with all required attribute using ineractive session
      The Proccess let you choose between 2 option ,copy from an existing user of create a new user from scratch 
      After you created the user ,you have the option to license the user with office365.
      In the end of the proceess the user is fully configured in office 365 enviorment.

      .PARAMETER FullName
      New User Full Name .

      .PARAMETER SourceUserName
      The UserName from which you want to copy from

      .PARAMETER Country
      New User Country name. 
  
      .PARAMETER UserPassword
      New User password.

      .PARAMETER DirSyncServer
      Name of local DirSync Server.

      .EXAMPLE
      New-HybridUser -SourceUserName MaryJ -TargetUserName JackW -FirstName "Jack" -LastName "White" -Country "USA" -FullName "Jack White" -UpnAddress "JackW@contoso.com" -UserPassword P@ssword!3 -DirSyncServer "server-dirsync1"
 
      this command create a new user "Jack White" from an existing AD user "MaryJ" with the same groups and OU location ,then create a mailbox for newly created user in the on-premises exchange server, sync the new object changes to office 365
      when changes are synced ,the command then connects to microsoft online services, assing a user office 365 license and migrate his mailbox to exchange online, 

 
      .EXAMPLE
      New-HybridUser -TargetUserName JackW -FirstName "Jack" -LastName "White" -Country "USA" -FullName "Jack White" -UpnAddress "JackW@contoso.com" -UserPassword P@ssword!3 -DirSyncServer "server-dirsync1"
 
      this command create a new user "Jack White" ,then create a mailbox for newly created user in the on-premises exchange server, sync the new object changes to office 365
      when changes are synced ,the command then connects to microsoft online services, assing a user office 365 license and migrate his mailbox to exchange online, 



      .NOTES
      1.Active Directory Powershell module
      2.Office 365 sign-in assistant - http://www.microsoft.com/en-us/download/details.aspx?id=28177
      3.Windows Azure Active Directory Powershell module - http://technet.microsoft.com/library/jj151815.aspx
      4.DirSync
      5.run the script with at least an AD user that has rights to created and manage users
      6.have credentials of an office 365 global admin
      7.Set-ExecutionPolicy should be set to unrestricted        
  #>
 
  [CmdletBinding()]
  param(
  
    [Parameter(Mandatory = $true,Position = 0)]
    [String]
    $FullName,

    [Parameter(Mandatory = $true,Position = 1)]
    [ValidateSet('example.com','example.com')]
    [String]
    $UpnAddress,
    
 
    [Parameter(Mandatory = $false)]
    [String]
    $CopyFrom,
 
 
    [Parameter(Mandatory = $false,ParameterSetName = 'Office365')]
    [String]
    $CountryCode,


    [Parameter(Mandatory = $false,ParameterSetName = 'Office365')]
    [String]
    $DirSyncServer,
    
    
    [Parameter(Mandatory = $false,ParameterSetName = 'Office365')]
    [ValidateSet('E1','E3','E4')]
    [String]
    $License,

 
    [Parameter(Mandatory = $true,Position = 2)]
    [System.Security.Securestring]
    $UserPassword


  )
  
  
  Write-Host 'Please wait while creating the new user in AD' -ForegroundColor Magenta
  
  Import-Module -Name Activedirectory -WarningAction SilentlyContinue

  
  
  $username = $FullName.Split(' ')[0] + ($FullName.Split(' ')[1])[0]
  
  
  # Checking that the username is availble
   
  

  if(!$CopyFrom)
  {
     
 

    New-ADUser -SamAccountName $username -Enabled $true -UserPrincipalName "$username@$UpnAddress" -GivenName $FullName.Split(' ')[0] -Surname $FullName.Split(' ')[1] -Name $FullName -DisplayName $FullName -AccountPassword $UserPassword -OtherAttributes @{'mail'= "$username@$UpnAddress";'mailNickname' = $username}


  }
  elseif($CopyFrom)
  {
    New-ADUser -SamAccountName $username -Enabled $true -UserPrincipalName "$username@$UpnAddress" -GivenName $FullName.Split(' ')[0] -Surname $FullName.Split(' ')[1] -Name $FullName -DisplayName $FullName -AccountPassword $UserPassword -OtherAttributes @{'mail'= "$username@$UpnAddress";'mailNickname' = $username} -Instance $CopyFrom  
    
    Start-Sleep -Seconds 5
      
    # Copy Selected user Groups to the new user you just created

    Get-ADUser -Identity $CopyFrom -Properties memberof |Select-Object -ExpandProperty memberof | Add-ADGroupMember -Members $username
    

    Start-Sleep -Seconds 3

    $u = Get-ADUser -Identity $CopyFrom 
    $DN = $U.DistinguishedName 
    $path = $DN.Substring($dn.indexof(',') + 1)

    Get-ADUser -Identity $username | Move-ADObject -TargetPath $path 

  }
  
  $domainname = $UpnAddress.split('.')[0]
  Set-ADUser -Identity $username -Add @{'ProxyAddresses'= "smtp:$username@$domainname.mail.onmicrosoft.com"}
  Set-ADUser -Identity $username -Add @{'ProxyAddresses'= "smtp:$username@$domainname.onmicrosoft.com"}
  Set-ADUser -Identity $username -Add @{'ProxyAddresses'= "SMTP:$username@$UpnAddress"}
  Set-ADUser -Identity $username -Add @{'TargetAddress'= "$username@$domainname.mail.onmicrosoft.com"}

  # Sync User to office 365 (Azure AD)
  
  Write-Host 'Syncing User to Office 365' -ForegroundColor Cyan -BackgroundColor Black

  $DirsyncAction = Invoke-Command -ComputerName $DirSyncServer -ScriptBlock {& 'C:\Program Files\Microsoft Azure AD Sync\Bin\DirectorySyncClientCmd.exe'  } -ArgumentList delta | Out-Null
  
  $DirsyncAction
  
  for($i = 1 ; $i -le 55 ; $i++)
  
  {Write-Progress -Activity 'Syncronize to the cloud' -Status 'Copy changes to cloud for user' -PercentComplete $i
  
    Start-Sleep 1
  }



  ###Connect to Microsoft Online###
  $LiveCred = Get-Credential -Message 'Please Enter office 365 Global Admin Credentials'
  Write-Host 'Connecting to office 365 online services.Please wait...' -ForegroundColor Magenta
  $Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $LiveCred -Authentication Basic -AllowRedirection -WarningAction SilentlyContinue
  Import-PSSession $Session -AllowClobber | Out-Null -WarningAction SilentlyContinue
  Import-Module -Name MSOnline -WarningAction SilentlyContinue
  Connect-MsolService -Credential $LiveCred -WarningAction SilentlyContinue
 

  ### Check for availble license and assign it to the user ### 
  $LicenseCheck =  Get-MsolAccountSku | Where-Object {($_.SkuPartNumber -eq ($License.split(':')[1])) -and ($_.ConsumedUnits -lt $_.ActiveUnits) }

  if ($LicenseCheck = $true){
    Set-MsolUser -UserPrincipalName "$username@$UpnAddress" -UsageLocation $CountryCode -BlockCredential $false
    
    
    
     switch($License){
     
       'E1'{Set-MsolUserLicense -UserPrincipalName "$username@$UpnAddress" -AddLicenses 'example:STANDARDPACK'}
       
       'E3'{Set-MsolUserLicense -UserPrincipalName "$username@$UpnAddress" -AddLicenses 'example:ENTERPRISEPACK'}
       
       'E4'{Set-MsolUserLicense -UserPrincipalName "$username@$UpnAddress" -AddLicenses 'example:ENTERPRISEWITHSCAL'}
     }
    Start-Sleep -Seconds 10

   

  }

  
  else{
    Write-host "You have reached the maximum number of license `nyou cannot add any more licenses n`Please contact your Tenent Admin" -ForegroundColor Yellow
  }


    Start-Sleep -Seconds 30

   $test_newuser = Get-Mailbox -identity $username
   $test_newlicense = (Get-MsolUser -UserPrincipalName "$username@$UpnAddress").isLicensed -eq $true
   
   while($test_newuser -and $test_newlicense){
    
     Write-Host -Object "You Have Succesfully created $username@$UpnAddress" -ForegroundColor Green -BackgroundColor Black
     break
   }
  
  Remove-PSSession $Session
}


 
