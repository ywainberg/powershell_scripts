﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2016 v5.2.127
	 Created on:   	05/09/2016 16:57
	 Created by:   	yaronw
	 Organization: 	example
	 Filename:  
	 URL Referance = https://azure.microsoft.com/en-us/documentation/articles/virtual-machines-windows-expand-os-disk/
	===========================================================================
	.DESCRIPTION
		A description of the file.
#>
function Set-AzureRmDiskSize
{
	
	[CmdletBinding()]
	param (
		
		[Parameter(Mandatory = $true)]
		[string]$VM,
		
		[Parameter(Mandatory = $true)]
		[string]$RG,
		
		[Parameter(Mandatory = $true)]
		[ValidateSet('OS' , 'Data')]
		[string]$DiskType,
		
		[Parameter(Mandatory = $true)]
		[string]$DiskSize
		
		
		
	)
	try
	{
		Get-AzureRmContext | Out-Null
		
		}
	
	catch
	{
		[System.InvalidOperationException]
		Write-Error -Message 'You are not logged in to azure account' -RecommendedAction 'Complete the login process'
		Login-AzureRmAccount
	}
	
	finaly{
		
		$vm1 = Get-AzureRmVM -ResourceGroupName $RG -Name $VM
		
		Stop-AzureRmVM -ResourceGroupName $RG -Name $VM
		
		switch ($DiskType)
		{
			'OS' {
				$vm1.StorageProfile.OSDisk.DiskSizeGB = $DiskSize
			}
			'Data' {
				$number = Read-Host "Do you have more than 1 data disk? (Yes/No) "
				if ($number -eq 'No')
				{
					
					$vm.StorageProfile.DataDisks[0].DiskSizeGB = $DiskSize
				}
				elseif ($number -eq 'Yes')
				{
					[int]$disk_num = Read-Host ('Enter the desired disk number (0 for number 1 ,1 for number2...')
					$vm.StorageProfile.DataDisks[$disk_num].DiskSizeGB = $DiskSize
				}
			}
			
		}
		
		Update-AzureRmVM -ResourceGroupName $RG -VM $vm1
		
		Start-AzureRmVM -ResourceGroupName $RG -Name $VM
	}
}