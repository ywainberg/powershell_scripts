﻿$mailbox = Get-Mailbox -Identity * | where{ $_.ProhibitSendReceiveQuota -lt '11GB'} | select -ExpandProperty Identity
foreach($m in $mailbox)
{            
Set-Mailbox -Identity $m -ProhibitSendReceiveQuota 50GB -ProhibitSendQuota 49.5GB -UseDatabaseQuotaDefaults $false -IssueWarningQuota 49GB

}