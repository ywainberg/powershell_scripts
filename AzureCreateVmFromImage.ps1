﻿function New-VmFromImage{
<#
      .SYNOPSIS
      Create Azure vm from Image

      .DESCRIPTION
      Create an Azure vm from a captured image or Azure image

      .PARAMETER Image
      image name to choose from :Windows server 2012R2,2012R2 + MSSQL2014, Latest version of Centos

      .PARAMETER vmname
      desired vm name

      .PARAMETER vmsize
      vm size to choose from Medium=3.5GB + 2Core , Large=4GB + 4Core, DS3=14GB + 4Core(SSD support)

      .PARAMETER subsc
      Subscription Name (if have more than one)
      
      .PARAMETER staccount
      Storage account 

      .PARAMETER svcname
      Cloud service name (could be also the vm name)

      .PARAMETER vnetname
      Virtual network name(predefined)

      .PARAMETER subnetname
      Select Your desired Vlan 

      .PARAMETER cred
      vm local admin credentials.Need to add(Get-Credentials)

      .EXAMPLE
      New-VmFromImage -image Centos -vmname srv-linux -vmsize Medium -subsc c641763a-1467-4bg5-rt1c-se9a5f6241bc -staccount scclassic -svcname srv-linux -vnetname 'group it sc_net' -SubnetName Vlan2 -cred(Get-Credential)
			
			In This Example we create a Centos linux vm with 2 core and 3.5GB RAM, placed in Vlan 2 the in sclassic storage account	
      
			.EXAMPLE
       New-VmFromImage -label 'newwithsql' -vmname 'srv-sql' -vmsize Standard_DS3 -subsc 'c641763a-1467-4bg5-rt1c-se9a5f6241bc' -staccount 'scclassic' -svcname 'srv-sql' -vnetname 'group it sc_net' -SubnetName 'Vlan1' -cred(Get-Credential) -Verbose
			
			In this Example we create a VM from a coustom SQL image we created earlier ,set a size of 14GB RAM and 4 core ,placed it in Vlan1 in scclassic storage account
      
      .NOTES


#>

  [CmdLetBinding(SupportsShouldProcess = $true)]

param(
		
		
[Parameter(Position = 0)]
[ValidateSet('newwithsql', '2012R2', 'Centos')]		
[string]$image,
			
[Parameter(Mandatory=$true)]
[string]$vmname,
		
[Parameter(Mandatory = $true,
								HelpMessage = 'Medium=3.5GB + 2Core , Large=4GB + 4Core, DS3=14GB + 4Core(SSD support)')]
[ValidateSet('Standard_DS3', 'Medium', 'Large' )]	
[string]$vmsize,


[Parameter(Mandatory=$true)]
[string]$subsc,
		
[Parameter(Mandatory = $true)]
[ValidateSet('scclassic')]		
[string]$staccount,


[Parameter(Mandatory=$true)]
[string]$svcname,
		
[Parameter(Mandatory = $true)]
[ValidateSet('group it sc_net')]	
[string]$vnetname,
		
[Parameter(Mandatory = $true)]
[ValidateSet('Vlan1', 'Vlan2')]	
[string]$SubnetName,



[Parameter(Mandatory=$true)]
[ValidateNotNull()]
[System.Management.Automation.PSCredential]
[System.Management.Automation.Credential()]
$cred

)

Select-AzureSubscription -SubscriptionId $subsc

Set-AzureSubscription -SubscriptionId $subsc -CurrentStorageAccountName $staccount
switch ($image) {
		'newwithsql' {
			
			$label = Get-AzureVMImage | Where-Object { $_.Label -match 'newwithsql' } |  Select-Object -ExpandProperty ImageName
			
	}
		'2012R2'{
			
			$label = Get-AzureVMImage | Where-Object { $_.Label -match 'Windows Server 2012 R2 Datacenter' } | Select-Object -ExpandProperty imageName -Last 1
	}
		'Centos' {
			
			$label = Get-AzureVMImage | Where-Object { $_.PublisherName -match 'openlogic' -and $_.OS -eq 'Linux' } | Select-Object -ExpandProperty imageName -Last 1
	}
	
}
#$label = Get-AzureVMImage | Where-Object { $_.Label -match "$image*" } | Sort-Object PublishedDate -Descending | Select-Object -ExpandProperty ImageName -First 1
	
	if ((Get-AzureService).ServiceName -contains $svcname)
	{
		
		$ServiceName -eq $svcname
		
		Start-Sleep -Seconds 120
		if ($image -eq 'Centos')
		{ New-AzureVMConfig -Name $vmname -InstanceSize $vmsize -ImageName $label | Add-AzureProvisioningConfig -Linux -LinuxUser $cred.GetNetworkCredential().Username -Password $cred.GetNetworkCredential().Password | Set-AzureSubnet -SubnetNames $SubnetName | New-AzureVM -ServiceName $svcname -Location 'North Europe' -VNetName $vnetname -WaitForBoot }
		
		else { New-AzureVMConfig -Name $vmname -InstanceSize $vmsize -ImageName $label | Add-AzureProvisioningConfig -Windows -AdminUsername $cred.GetNetworkCredential().Username -Password $cred.GetNetworkCredential().Password | Set-AzureSubnet -SubnetNames $SubnetName | New-AzureVM -ServiceName $svcname -Location 'North Europe' -VNetName $vnetname -WaitForBoot }
		
		
	}
	
	else
	{
		
		$ServiceName = New-AzureService -ServiceName $svcname -Label $svcname -Location 'North Europe'
		
		
		Start-Sleep -Seconds 120
		
		if ($image -eq 'Centos')
		{ New-AzureVMConfig -Name $vmname -InstanceSize $vmsize -ImageName $label | Add-AzureProvisioningConfig -Linux -LinuxUser $cred.GetNetworkCredential().Username -Password $cred.GetNetworkCredential().Password | Set-AzureSubnet -SubnetNames $SubnetName | New-AzureVM -ServiceName $svcname -Location 'North Europe' -VNetName $vnetname -WaitForBoot }
		
		else { New-AzureVMConfig -Name $vmname -InstanceSize $vmsize -ImageName $image | Add-AzureProvisioningConfig -Windows -AdminUsername $cred.GetNetworkCredential().Username -Password $cred.GetNetworkCredential().Password | Set-AzureSubnet -SubnetNames $SubnetName | New-AzureVM -ServiceName $svcname -VNetName $vnetname -WaitForBoot }
	}
}
