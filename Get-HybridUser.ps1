﻿<#
.SYNOPSIS
Get-HybridUser get user detailes on office 365 in hybrid configuration.
.DESCRIPTION
 This Script is design to get Microsoft online user mailbox and license information, focusing on the real world requirements .
 .PARAMETER UserName
 The UserName you want to query

 .PARAMETER MailboxInfo
 The type of maibox information you want to retrieve.
 Option:
 1.statics = mailbox general information
 2.id = mailbox size information
 3.migration -user migration status

 .PARAMETER LicensedUser 
 information about user license status.

 .EXAMPLE
 Get-HybridUser -Username BillG -MailboxInfo migration

 this command gets a migration status for the user BillG
 
 .EXAMPLE
 Get-HybridUser -Username BillG -MailboxInfo statics

 this command get full mailbox statics for a user BillG


 .EXAMPLE
 Get-HybridUser -Username BillG -LicensedUser BillG@microsoft.com

 this command get the license inforamtion about the user BillG

  .NOTES
 1.Office 365 sign-in assistant - http://www.microsoft.com/en-us/download/details.aspx?id=28177
 2.Windows Azure Active Directory Powershell module - http://technet.microsoft.com/library/jj151815.aspx
 3.have credentials of an office 365 global admin
 4.Set-ExecutionPolicy should be set to unrestricted
#>
function Get-HybridUser

{
[cmdletbinding()]

 param (
 

 [Parameter(Mandatory = $False)]
           [System.Management.Automation.PsCredential]$Credential,

 [Parameter(Mandatory = $true)]
 [string] $Username,

[Parameter(ValueFromPipelineByPropertyName=$true)]
[String]$MailboxInfo,


[string]
$LicensedUser 


 )

 
 while((Get-PSSession).ConfigurationName -ne "Microsoft.Exchange")

    { 
 
$Credential = Get-Credential -Message "Enter the username and password of an Exchange Online Global Administrator account."

Write-Host "Connecting to office 365 online services.Please wait..." -ForegroundColor Magenta

$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://ps.outlook.com/powershell/ -Credential $Credential -Authentication Basic -AllowRedirection -WarningAction SilentlyContinue

Import-PSSession $Session -AllowClobber | Out-Null

Import-Module -Name MSOnline

Connect-MsolService -Credential $Credential

    
    Break
    }

if($LicensedUser -and !$MailboxInfo){

 Get-MsolUser -UserPrincipalName $LicensedUser | select UserPrincipalName, IsLicensed, Licenses | fl
 return
    
    }

elseif($LicensedUser -and $MailboxInfo){

    Get-MsolUser -UserPrincipalName $LicensedUser | select UserPrincipalName, IsLicensed, Licenses | fl

    }

foreach($user in $Username)

    {

switch($MailboxInfo){
 
 statics{Get-MailboxStatistics -Identity $Username}
  
 id{Get-MailboxStatistics -Identity $UserName | select DisplayName, TotalItemSize, DatabaseProhibitSendReceiveQuota | ft Displayname, @{label='MailboxSize';Expression={$_.TotalItemSize}},@{label='Mailbox Limit Size';Expression={$_.DatabaseProhibitSendReceiveQuota}} }
    
 migration  { Get-MoveRequestStatistics -Identity $UserName  | fl  DisplayName,Status,QueuedTimestamp,StartTimestamp,FinalSyncTimestamp,CompletionTimestamp,OverallDuration }

 default { Get-MailboxStatistics -Identity $Username | fl }

        }
      }
    }
    