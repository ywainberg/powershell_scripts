﻿<#
    .Synopsis 
        Remove AutoMap to a mailbox in exchange online.

    .Description
        This scripts  Remove AutoMap to a mailbox in exchange online to help control mailbox settings in outlook.

    .Parameter Mailbox_list
        Mailbox list(s) on which you want to remove the automapping.

	.Parameter user
		    The user you want to apply the mailbox settings

    .Example
        block_automap.ps1 -Mailbox_list 'accounting@mycompany.com','finance@mycompany.com' -user 'RogerM'

        Remove the automapping from accounting@mycompany.com and finance@mycompany.com mailbox's for the user RogerM 


    .Notes
		Author : Yaron Wainberg
		WebSite: https://anyanydrop.com

#>
   
  [CmdletBinding()]
  
  param
  (
  
    [Parameter(Mandatory = $true,position=1)]
    [string[]]$Mailbox_list,
    
    [Parameter(Mandatory =$true,position=2)]
    [string]$user
  
  
  )
  
  Write-Verbose 'Please follow the instruction to remove automapping' 

  Write-Verbose 'Connecting to Exchange Online' 

  # Log in to Exchange Online 

    $LiveCred = Get-Credential -Message 'Enter office 365 Global Admin credentials'
    $Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $LiveCred -Authentication Basic -AllowRedirection -WarningAction SilentlyContinue -InformationAction SilentlyContinue 
    Import-PSSession $Session -AllowClobber -WarningAction SilentlyContinue |Out-Null
  

  
  foreach($m in $Mailbox_list)

  {

   $test_accessrights =  (Get-MailboxPermission -Identity $m -User $user).AccessRights
    $test_inherited =  (Get-MailboxPermission -Identity $m -User $user).IsInherited
   
   if ($test_accessrights -eq 'FullAccess'){
    # Remove User Permission for the mailbox's
    Write-Verbose "Remove User Permission for the mailbox's" 

    Remove-MailboxPermission -Identity $m -User $user -AccessRights FullAccess -Confirm:$false |Out-Null
    Write-Verbose "Add User Pemission to mailbox's without automap" 

    # Add User Pemission for the mailbox's without automap
  
    Add-MailboxPermission -Identity $m -User $user -AccessRights FullAccess -InheritanceType all -AutoMapping:$false | Out-Null
    }

    else
        {
    Write-Verbose "Add User Pemission to mailbox's without automap" 

    # Add User Pemission for the mailbox's without automap
  
    Add-MailboxPermission -Identity $m -User $user -AccessRights FullAccess -InheritanceType all -AutoMapping:$false | Out-Null
    }

  $i = 1
  write-progress -activity 'Working on Mailbox' -Status $m -PercentComplete ($i /$Mailbox_list.Count * 100)

  }

  # validating actions
  foreach($m in $Mailbox_list){
    $test_accessrights =  (Get-MailboxPermission -Identity $m -User $user).AccessRights
    $test_inherited =  (Get-MailboxPermission -Identity $m -User $user).IsInherited

    if (($test_accessrights -eq 'FullAccess') -and ($test_inherited -eq $false))

    {
      Write-Host 'The Process is successfully' -ForegroundColor Green
   

    }
    else

    {

      Write-Host "The Process failed for $M" -ForegroundColor Red
    
    }
  }

  # Log out from exchange online
  
  Get-PSSession | Where-Object {$_.ComputerName -eq 'outlook.office365.com'} | Remove-PSSession


