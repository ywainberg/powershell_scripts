﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2016 v5.2.124
	 Created on:   	17/07/2016 11:48
	 Created by:   	yaronw
	 Organization: 	example
	 Filename:     	
	===========================================================================
	.DESCRIPTION
		Display azure rm vm status.
#>
function Test-Session ()
{
	$Error.Clear()
	
	#if context already exist
	Get-AzureRmContext -ErrorAction SilentlyContinue |Out-Null
	foreach ($eacherror in $Error)
	{
		if ($eacherror.Exception.ToString() -like "*Run Login-AzureRmAccount to login.*")
		{
			Login-AzureRmAccount
		}
	}
	
	$Error.Clear();
}





function Get-AzureRmVmStatus
{
	
	
	[cmdletbinding()]
	 param
	(
		[parameter(Mandatory = $true, ValueFromPipeline = $true , Position = 0)]
		[string]$Name,
		
		[parameter(Mandatory = $true, Position = 1)]
		[string]$ResourceGroup
		
	)
	
	Check-Session
	$status = (Get-AzureRmVM -Name $Name -ResourceGroupName $ResourceGroup -Status | Select-Object -ExpandProperty statuses | Where-Object{ $_.code -match 'powerstate' }).code.split('/')[1]
	Write-Output "`n $Name status is: $status `n"

}