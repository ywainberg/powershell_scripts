﻿<#Usage: New-DomainSignedCertificate -Hostname "serverName" -Organization "example" -OrganizationalUnit "IT" -Locality "IL" -State "AZR" -Country "AZR" -CertificateAuthority "srv-irg-subca01.example.com\example-SRV-IRG-SUBCA01-CA"#>
function New-DomainSignedCertificate {
    [CmdletBinding()]
    param(
        [parameter(Mandatory=$true)]
        [string]
        $Hostname,

        [parameter(Mandatory=$true)]
        [string]
        $Organization,

        [parameter(Mandatory=$true)]
        [string]
        $OrganizationalUnit,

        [parameter(Mandatory=$true)]
        [string]
        $Locality,

        [parameter(Mandatory=$true)]
        [string]
        $State,

        [parameter(Mandatory=$true)]
        [string]
        $Country,

        [parameter(Mandatory=$true)]
        [string]
        $CertificateAuthority,

        [parameter(Mandatory=$false)]
        [string]
        $Keylength = "1024",

        [string]
        $workdir = $env:Temp
    )

    $fileBaseName = $Hostname -replace "\.", "_" 
    $fileBaseName = $fileBaseName -replace "\*", ""

    $infFile = $workdir + "\" + $fileBaseName + ".inf"
    $requestFile = $workdir + "\" + $fileBaseName + ".req"
    $CertFileOut = $workdir + "\" + $fileBaseName + ".cer"

    Try {
        Write-Verbose "Creating the certificate request information file ..."
        $inf = @"
[Version] 
Signature="`$Windows NT`$"

[NewRequest]
Subject = "CN=$Hostname, OU=$OrganizationalUnit, O=$Organization, L=$Country, S=$State, C=$Locality"
KeySpec = 1
KeyLength = $Keylength
Exportable = TRUE
FriendlyName = "$Hostname"
MachineKeySet = TRUE
KeyUsage = 0xf0
"@


        $inf | Set-Content -Path $infFile

        Write-Verbose "Creating the certificate request ..."
        & certreq.exe -new "$infFile" "$requestFile"

        Write-Verbose "Submitting the certificate request to the certificate authority ..."
        & certreq.exe -submit -config "$CertificateAuthority" -attrib "CertificateTemplate:WebServer" "$requestFile" "$CertFileOut"

        if (Test-Path "$CertFileOut") {
            Write-Verbose "Installing the generated certificate ..."
            & certreq.exe -accept "$CertFileOut"
        }

    }
    Finally {
        Get-ChildItem "$workdir\$fileBaseName.*" | remove-item
    }
}