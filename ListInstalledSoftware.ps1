﻿$compil = Get-ADComputer -SearchBase "OU=Desktops,OU=Israel,OU=Workstations,DC=example,DC=com" -Filter * |Select-Object -ExpandProperty name
if(Test-Connection -ComputerName $compil -Quiet -) {

Invoke-Command -ComputerName $compil -ScriptBlock { Get-ItemProperty HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\* |where {$_.DisplayName -like "*Microsoft Office*2010*" }| select DisplayName, Publisher, InstallDate }
}