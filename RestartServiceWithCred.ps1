﻿$cred = New-Object System.Management.Automation.PsCredential 'example.com\svc-irg-tech', (Get-Content '\\172.16.0.211\companyshare\IT\Script\svc-irg-tech.txt'| ConvertTo-SecureString)

(Get-WmiObject -Class win32_service -Credential $cred -ComputerName 'srv-irg-tech01' |where {$_.name -like 'Apache2.4'}).StopService()

Start-Sleep -Seconds 2

(Get-WmiObject -Class win32_service -Credential $cred  -ComputerName 'srv-irg-tech01' |where {$_.name -like 'Apache2.4'}).StartService()



