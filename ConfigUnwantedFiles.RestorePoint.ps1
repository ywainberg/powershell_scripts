﻿<#	
	.NOTES
	===========================================================================
	 Created by:   	yaronw
	 Organization: 	example
	 Filename:     	
	===========================================================================
	.DESCRIPTION
		Find unwanted config file and send a report.
#>
#Path for files containing the word 'config' in their names
$configpath_root = Get-ChildItem c:\ | where{ ($_.Name -match 'config') -and ($_.Extension -ne '.config' -or '.xml') }
$configpath_d = Get-ChildItem d:\GW -Recurse | where{ ($_.Name -match 'config') -and ($_.Extension -ne '.config' -or '.xml') }
$configpath_c_gw = Get-ChildItem c:\GW -Recurse | where{ ($_.Name -match 'config') -and ($_.Extension -ne '.config' -or '.xml') }

# HTML style configuration for reports
$Header = @"
<style>
TABLE {border-width: 1px;border-style: solid;border-color: black;border-collapse: collapse;}
TH {border-width: 1px;padding: 3px;border-style: solid;border-color: black;background-color: #6495ED;}
TD {border-width: 1px;padding: 3px;border-style: solid;border-color: black;}
</style>
"@


# Convert output to html and save it in the directory
$report_root = $configpath_root | Select Directory, Name, CreationTime, LastWriteTime | ConvertTo-HTML -Head $Header | Out-File "c:\ConfigReport\Report_for_c_root $(Get-Date -Format dd-MM-yyyy).html"
$report_d = $configpath_d | Select 'Folder Name', Owner, 'Created On', 'Last Updated', Size | ConvertTo-HTML -Head $Header | Out-File "c:\ConfigReport\Report_for_d $(Get-Date -Format dd-MM-yyyy).html"
$report_c = $configpath_c_gw | Select 'Folder Name', Owner, 'Created On', 'Last Updated', Size | ConvertTo-HTML -Head $Header | Out-File "c:\ConfigReport\Report_for_c_gw $(Get-Date -Format dd-MM-yyyy).html"


# Mail Variabels 
$recipient = 'yaronw@example.com'
$sender = 'config_log@example.com'
$subject = "Unwanted Config FileReport from $env:COMPUTERNAME"
$mail_server = '172.16.0.235'

# Send report to recipient
Send-MailMessage -From $sender -To $recipient -SmtpHost $mail_server -Subject $subject -Body 


