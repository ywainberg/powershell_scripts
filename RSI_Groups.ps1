﻿New-ADGroup -Name 'RSI_Billing' -Description 'RSI Group Access' -DisplayName 'RSI_Billing' -GroupCategory Security -GroupScope Universal -Path 'OU=User Groups,OU=Groups,OU=Production,DC=prod,DC=example,DC=com'
New-ADGroup -Name 'RSI_ACQ' -Description 'RSI Group Access' -DisplayName 'RSI_ACQ' -GroupCategory Security -GroupScope Universal -Path 'OU=User Groups,OU=Groups,OU=Production,DC=prod,DC=example,DC=com'
New-ADGroup -Name 'RSI_Admin' -Description 'RSI Group Access' -DisplayName 'RSI_Admin' -GroupCategory Security -GroupScope Universal -Path 'OU=User Groups,OU=Groups,OU=Production,DC=prod,DC=example,DC=com'
New-ADGroup -Name 'RSI_Risk-Support' -Description 'RSI Group Access' -DisplayName 'RSI_Risk-Support' -GroupCategory Security -GroupScope Universal -Path 'OU=User Groups,OU=Groups,OU=Production,DC=prod,DC=example,DC=com'
New-ADGroup -Name 'RSI_Risk-AM' -Description 'RSI Group Access' -DisplayName 'RSI_Risk-AM' -GroupCategory Security -GroupScope Universal -Path 'OU=User Groups,OU=Groups,OU=Production,DC=prod,DC=example,DC=com'


$Billing = Get-ADGroup -Identity 'RSI_Billing' -Server 'example.com'
$ACQ = Get-ADGroup -Identity 'RSI_ACQ' -Server 'example.com'
$Admin = Get-ADGroup -Identity 'RSI_Admin' -Server 'example.com'
$Support = Get-ADGroup -Identity 'RSI_Risk-Support' -Server 'example.com'
$AM = Get-ADGroup -Identity 'RSI_Risk-AM' -Server 'example.com'

Add-ADGroupMember 'RSI_Billing' -Server Domain1.com -Member $Billing
Add-ADGroupMember 'RSI_ACQ' -Server Domain1.com -Member $ACQ
Add-ADGroupMember 'RSI_Admin' -Server Domain1.com -Member $Admin
Add-ADGroupMember 'RSI_Risk-Support' -Server Domain1.com -Member $Support
Add-ADGroupMember 'RSI_Risk-AM' -Server Domain1.com -Member $AM