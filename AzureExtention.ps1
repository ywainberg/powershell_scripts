﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2016 v5.2.128
	 Created on:   	22/11/2016 10:45
	 Created by:   	yaronw
	 Organization: 	example
	 Filename:     	
	===========================================================================
	.DESCRIPTION
		A description of the file.
#>

# install extention to join vm to domain
Get-AzureRmVM -Name 'vmname' -ResourceGroupName 'resourcegroupname' | `
Set-AzureRmVMCustomScriptExtension -Name "CustomScriptExtension" -VMName 'vmname' -ResourceGroupName 'resourcegroupname' -StorageAccountName scstandard `
																	 -StorageAccountKey "Iz5T8+hQ9L4tfi2/F59piHEj8MCIwcrQD2SDHvwUnt6neKjr0urc4sAHcUFHGMDW0Yp0qocAXm88007IwwecYg==" -ContainerName "scripts" `
																	 -FileName "VmprovisionAzurerm.ps1" -Run "VmprovisionAzurerm.ps1"


#check if domain join succeded 
Get-ADComputer -Identity 'vmname'

# remove extention after finish
Remove-AzureRmVMExtension -ResourceGroupName 'resourcegroupname' -VMName 'vmname' -Name "CustomScriptExtension"