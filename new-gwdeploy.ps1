﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2016 v5.2.118
	 Created on:   	04/04/2016 13:37
	 Created by:   	yaronw
	 Organization: 	example
	 Filename:     	new-gwdeploy
	===========================================================================
	.DESCRIPTION
		A description of the file.
#>
workflow new-gwdeploy 
{
	
	[CmdLetBinding()]
	param
	(
		[parameter(Mandatory = $true,
						 		ParameterSetName = 'Vmware')]
		[string]$Vcenter,
		
		[Parameter(Mandatory = $true,
						 		ParameterSetName = 'Vmware')]
		[Parameter(Parametersetname = 'Azure')]
		[string]$VMName,
		
		[Parameter(Mandatory = $true,
						 		ParameterSetName = 'Vmware')]
		[Parameter(Parametersetname = 'Azure')]
		[ValidateSet("Standard","Large")]
		[string]$VMSize,
		
		
		[Parameter(Mandatory = $true,
							 ParameterSetName = 'Vmware')]
		[Parameter(Parametersetname = 'Azure')]
		[ValidateSet("2012VmwareGW", "2012AzureGW")]
		[string]$VMTemplate
		
			
		
		
	
	)
	
	
	
	
	
	
}