﻿<#
.SYNOPSIS
	This is a utility script that contains a series of common functions for snapshotting BLOBs.
	
.DESCRIPTION
	This is a utility script that contains a series of common functions for snapshotting BLOBs.  This is not meant for direct consumption.
	
.NOTES
	Author: Chris Clayton
	Date: 2013/08/30
	Revision: 1.1
#>
Import-Module 'C:\Program Files (x86)\Microsoft SDKs\Windows Azure\PowerShell\ServiceManagement\Azure\Azure.psd1'

<#
================================================================================================================================================================
														Configure the subscriptions
================================================================================================================================================================
#>

<#
.SYNOPSIS
	Deletes the Windows Azure subscriptions file used by PowerShell in the context of this script. 
	
.DESCRIPTION
	Deletes the Windows Azure subscriptions file used by PowerShell in the context of this script. It is recommended that this is called at the beginning and 
	end of the script.  This file is to be encrypted based on the Windows Azure PowerShell documentation, but based on actual use it is plain text XML.

.PARAMETER subscriptionDataFile
	The name of the data file that stores the subscription information.

.NOTES
	Author: Chris Clayton
	Date: 2013/07/11
	Revision: 1.0

.EXAMPLE
	Clear-SubscriptionData 'c:\subscriptiondata.xml'
#>
function Clear-SubscriptionData
{	
	param
	(
		[string]$subscriptionDataFile
	)

	if(Test-Path -PathType Leaf -Path $subscriptionDataFile)
	{
		Remove-Item $subscriptionDataFile
	}
}

<#
.SYNOPSIS
	Adds a new subscription to the Windows Azure subscription file that is used in this scripts context.
	
.DESCRIPTION
	Adds a new subscription to the Windows Azure subscription file that is used in this scripts context.  Calling this function
	adds entries that can later be used with a call to Select-AzureSubscription.  Specify the name as the SubscriptionName 
	parameter and the file using the SubscriptionDataFile.	

.PARAMETER subscriptionName
	The name of the subscription entry that will be used later to reference the subscription id and X509 certificate pair.
	
.PARAMETER subscriptionId
	The subscription id as seen in the Windows Azure Developer portal.

.PARAMETER thumbprint
	The thumbprint of the X509 certificate to be used for Windows Azure management functions.  This must already be uploaded to the
	portal.
	
.PARAMETER storageAccount
	If provided this is the name of the storage account within the specified subscription that is used as the default.  Some
	PowerShell cmdlets require this to be set (ex. creating virtual machines).  If none is provided no current storage account
	setting will be assigned for this subscription.
	
.PARAMETER isDefault
	If $true this subscription is the default to be used when no other Windows Azure subscription has been defined.  If this 
	parameter is not specified it will default to not being the default.
	
.PARAMETER subscriptionDataFile
	The name of the data file that stores the subscription information.
	
.NOTES
	Author: Chris Clayton
	Date: 2013/07/11
	Revision: 1.0

.EXAMPLE
	New-SubscriptionByCertificate 'MySubscription' 'XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX' 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX' 'MyStorageAccount' $false 'c:\subscriptiondata.xml'

.LINK
	http://manage.windowsazure.com
#>
function New-SubscriptionByCertificate
{
	param
	(	
		[string]$subscriptionName,		
		[string]$subscriptionId,
		[string]$thumbprint,
		[string]$storageAccount = $null,
		[bool]$isDefault = $false,
		[string]$subscriptionDataFile
	)
		
	$managementCertificate = Get-Item "cert:\CurrentUser\MY\$($thumbprint)"	
	
	if($managementCertificate -eq $null)
	{
		$managementCertificate = Get-Item "cert:\LocalMachine\MY\$($thumbprint)"	
	}
	
	if(($storageAccount -ne $null) -and (![string]::IsNullOrWhiteSpace($storageAccount)) -and ($managementCertificate -ne $null))
	{
		Set-AzureSubscription -Certificate $managementCertificate -SubscriptionId $subscriptionId -CurrentStorageAccount $storageAccount -SubscriptionName $subscriptionName -SubscriptionDataFile $subscriptionDataFile
	}
	else
	{
		Set-AzureSubscription -Certificate $managementCertificate -SubscriptionId $subscriptionId -SubscriptionName $subscriptionName -SubscriptionDataFile $subscriptionDataFile
	}
}

<#
.SYNOPSIS
	Adds the subscription details to the Windows Azure data file.
	
.DESCRIPTION
	Adds the subscription details to the Windows Azure data file.  This information is taken from the csv file.

.PARAMETER subscriptionFileName
	The csv file that contains all of the subscription id and certificate details.

.PARAMETER dataFileName
	The Windows Azure subscription data file to be used by the cmdlets.

.NOTES
	Author: Chris Clayton
	Date: 2013/07/11
	Revision: 1.0

.EXAMPLE
	Prepare-SubscriptionDataFile 'c:\subscriptions' 'c:\subscriptions\wadata.xml'
#>
function Prepare-SubscriptionDataFile
{
	param
	(
		[string]$subscriptionFileName,
		[string]$dataFileName
	)
	
	# If previous runs did not finish clear them out
	Clear-SubscriptionData $dataFileName

	# Read in the jobs to be monitored and the subscription details
	[System.Array]$subscriptionList = Get-CsvContents $subscriptionFileName
		
	# Setup the subscriptions to be used
	foreach($subscription in $subscriptionList)
	{
		New-SubscriptionByCertificate $subscription.SubscriptionName $subscription.SubscriptionId $subscription.CertificateThumbprint $null $false $dataFileName
	}
}

<#
================================================================================================================================================================
														CSV Files
================================================================================================================================================================
#>

<#
.SYNOPSIS
	Retrieves a list of items from the specified CSV file.
	
.DESCRIPTION
	Retrieves a list of items from the specified CSV file.  Returns an array of hashtables each representing a seperate entry in the csv file.  If the file is not found it will return $null.
	
.PARAMETER filePath
	The fully qualified path to the CSV file.
	
.NOTES
	Author: Chris Clayton
	Date: 2013/07/11
	Revision: 1.0

.EXAMPLE
	Get-CsvContents 'c:\myfile.csv'
#>
function Get-CsvContents
{
	param
	(
		[string]$filePath
	)
	
	[System.Array]$contentList = $null
	
	if(Test-Path -Path $filePath -PathType Leaf)
	{
		$contentList = Import-Csv $filePath
	}
	
	return $contentList
}

<#
================================================================================================================================================================
														BLOBs
================================================================================================================================================================
#>

<#
.SYNOPSIS
	Returns the storage account connection string for the specified blob.  
	
.DESCRIPTION
	Returns the storage account connection string for the specified blob.  The Azure subscription in use must be the one that contains the storage account.

.PARAMETER storageAccountName
	The name of the storage account the connection  string is to be retreived for.

.PARAMETER useHttps
	True if the protocol used should be HTTPS.

.NOTES
	Author: Chris Clayton
	Date: 2013/07/11
	Revision: 1.0

.EXAMPLE
	Get-StorageConnectionString 'mystorageaccount' $true
#>
function Get-StorageConnectionString
{
	param
	(
		[string]$storageAccountName,			# The name of the storage account 
		[bool]$useHttps = $true					# $true if https is to be used
	)
	
	# Determine the primary key and protocol
	$accountKeys = Get-AzureStorageKey -StorageAccountName $storageAccountName
	
	[string]$protocol = 'https'
	[string]$connectionString = $null

	# If the account keys could not be retrieved the connection string is null
	if($accountKeys -ne $null)
	{
		if($useHttps -eq $false)
		{
			$protocol = 'http'
		}
		
		# Get the primary key for the storage account
		[string]$primaryKey = (Get-AzureStorageKey -StorageAccountName $storageAccountName).Primary		
		$connectionString = "DefaultEndpointsProtocol=$($protocol);AccountName=$($storageAccountName);AccountKey=$($primaryKey)"
	}
	
	return $connectionString
}

<#
.SYNOPSIS
	Returns the elements (storage account, container, blob and protocol) that is represented by a BLOB URL.
	
.DESCRIPTION
	Returns the elements (storage account, container, blob and protocol) that is represented by a BLOB URL.  Returns a 4 value TUPLE<string,string,string,bool> containing the storage account, container, blob, https if true.

.PARAMETER blobUri
	The BLOB uri that is to be broken down.
	
.NOTES
	Author: Chris Clayton
	Date: 2013/07/11
	Revision: 1.0

.EXAMPLE
	Get-BlobUriElements 'https://mystorageaccount.blob.windows.net/vhds/MyCDrive.vhd' 
#>
function Get-BlobUriElements
{
	param
	(
		[string]$blobUri
	)
	[System.Text.RegularExpressions.RegexOptions]$regexOptionFlags = [System.Text.RegularExpressions.RegexOptions]::CultureInvariant -bor [System.Text.RegularExpressions.RegexOptions]::IgnoreCase -bor [System.Text.RegularExpressions.RegexOptions]::Singleline

	$results = $null
	$matchList = [System.Text.RegularExpressions.Regex]::Match($blobUri, "^(?<Protocol>http[s]?)://(?<StorageAccountName>[^/]*)\.blob\.core\.windows\.net/(?<ContainerName>[^/]*)/(?<BlobName>.*)$", $regexOptionFlags)

	if($matchList.Groups.Count -gt 3)
	{
		[bool]$isHttps = $matchList.Groups['Protocol'].Value -match 'https'
		$results = New-Object "System.Tuple``4[[System.String],[System.String],[System.String],[System.Boolean]]" -ArgumentList @($matchList.Groups['StorageAccountName'].Value, $matchList.Groups['ContainerName'].Value, $matchList.Groups['BlobName'].Value, $isHttps)
	}	
		
	return $results	
}

<#
.SYNOPSIS
	Retrieves a blob based on the Storage APIs V2.0
	
.DESCRIPTION
	Retrieves a blob based on the Storage APIs V2.0.  The V2.0 storage APIs must be installed for this to work.  Returns an ICloudBlob object.

.PARAMETER blobUri
	The BLOB uri of the blob to retrieve.
	
.NOTES
	Author: Chris Clayton
	Date: 2013/07/11
	Revision: 1.0

.EXAMPLE
	Get-Blob 'https://mystorageaccount.blob.windows.net/vhds/MyCDrive.vhd' 
#>
function Get-Blob
{
	param
	(
		[string]$blobUrl
	)

	[Microsoft.WindowsAzure.Storage.Blob.ICloudBlob]$blob = $null
	
	$urlElements  = Get-BlobUriElements $blobUrl				
	[string]$storageAccountName = $urlElements.Item1
	
	# Get the connection string to be used by the CloudStorageAccount
	[string]$connectionString = Get-StorageConnectionString $storageAccountName $true
	
	if($connectionString -ne $null)
	{
		[Microsoft.WindowsAzure.Storage.CloudStorageAccount]$storageAccount = [Microsoft.WindowsAzure.Storage.CloudStorageAccount]::Parse($connectionString)
		[Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient]$client = $storageAccount.CreateCloudBlobClient()			
		$blob = $client.GetBlobReferenceFromServer($blobUrl)
	}	
	
	return $blob
}

<#
.SYNOPSIS
	Monitors a blob that is being copied and returns only after it is no longer pending.
	
.DESCRIPTION
	Monitors a blob that is being copied and returns only after it is no longer pending.  This relies on the 
	Storage V2.0 library.  Returns True if the CopyStatus was set to Success

.PARAMETER blobUri
	The BLOB uri of the blob to be monitored.

.NOTES
	Author: Chris Clayton
	Date: 2013/07/11
	Revision: 1.0

.EXAMPLE
	[bool]$success = Wait-BlobCopyComplete 'https://mystorageaccount.blob.windows.net/vhds/MyCDrive.vhd' 
#>
function Wait-BlobCopyComplete
{
	param
	(
		[string]$blobUrl
	)
	do
	{
		Start-Sleep -Seconds 5
		$blob = Get-Blob $blobUrl
	}while($blob.CopyState.Status -eq [Microsoft.WindowsAzure.Storage.Blob.CopyStatus]::Pending)
	
	return ($blob.CopyState.Status -eq  [Microsoft.WindowsAzure.Storage.Blob.CopyStatus]::Success)
}

<#
.SYNOPSIS
	Copies a BLOB from one location to another. 
	
.DESCRIPTION
	Copies a BLOB from one location to another.  Returns a HashTable of the source blob, destination blob and copy contexts.

.PARAMETER sourceSubscriptionName
	The friendly name of the subscription that contains the source BLOB.

.PARAMETER sourceBlobUrl
	The BLOB url of the source BLOB.

.PARAMETER destinationSubscriptionName
	The friendly name of the subscription that the BLOB is to be copied to.

.PARAMETER destinationBlobUrl
	The BLOB url it is to be copied to.

.PARAMETER subscriptionDataFileName
	The name of the file the Windows Azure subscription details are to be stored in.

.NOTES
	Author: Chris Clayton
	Date: 2013/07/11
	Revision: 1.0

.EXAMPLE
	$copyContexts = Copy-Blob 'sourceSubscription' 'https://mystorageaccount.blob.windows.net/vhds/MyCDrive.vhd' 'destinationSubscription' 'https://destinationstorageaccount.blob.windows.net/vhds/MyCDrive.vhd' 'c:\snapshots\subscriptiondata.xml'
#>
function Copy-Blob
{
	param
	(
		[string]$sourceSubscriptionName,
		[string]$sourceBlobUrl,
		[string]$destinationSubscriptionName,
		[string]$destinationBlobUrl,
		[string]$subscriptionDataFileName
	)
	
	# Set the subscription context to the source subscription
	Select-AzureSubscription -SubscriptionDataFile $subscriptionDataFileName -SubscriptionName $sourceSubscriptionName
	$sourceUrlElements  = Get-BlobUriElements $sourceBlobUrl	
	$sourceStorageContext = New-AzureStorageContext -StorageAccountName $sourceUrlElements.Item1 -StorageAccountKey (Get-AzureStorageKey -StorageAccountName $sourceUrlElements.Item1).Primary -Protocol Https

	# Set the subscription context to the destination subscription
	Select-AzureSubscription -SubscriptionDataFile $subscriptionDataFileName -SubscriptionName $destinationSubscriptionName
	$destinationUrlElements  = Get-BlobUriElements $destinationBlobUrl	
	$destinationStorageContext = New-AzureStorageContext -StorageAccountName $destinationUrlElements.Item1 -StorageAccountKey (Get-AzureStorageKey -StorageAccountName $destinationUrlElements.Item1).Primary -Protocol Https

	# Returns a copy operation that can later be used to wait etc.
	[Microsoft.WindowsAzure.Management.Storage.Model.ResourceModel.AzureStorageBlob]$copyOperation = Start-CopyAzureStorageBlob -SrcContext $sourceStorageContext -SrcBlob $sourceUrlElements.Item3 -SrcContainer $sourceUrlElements.Item2 -DestContext $destinationStorageContext -DestContainer $destinationUrlElements.Item2 -DestBlob $destinationUrlElements.Item3	

	# The consuming method should hold the return object to be sure it does not fall out of scope.  In testing it was found that there are timing related
	# things that could cause the operation to succeed but identify as a failure if the destination or source contexts are collected
	return @{ 'CopyContext' = $copyOperation; 'SourceContext' = $sourceStorageContext; 'DestinationContext' = $destinationStorageContext }
}

<#
================================================================================================================================================================
														BLOB Snapshots
================================================================================================================================================================
#>

<#
.SYNOPSIS
	Creates a new snapshot of a blob.
	
.DESCRIPTION
	Creates a new snapshot of a blob.  This uses the Storage library V2.0.  Returns a handle to the snapshot blob.

.PARAMETER blobUrl
	The Url of the BLOB to have the snapshot taken.

.NOTES
	Author: Chris Clayton
	Date: 2013/07/11
	Revision: 1.0

.EXAMPLE
	$snapshot = Create-BlobSnapshot 'https://mystorageaccount.blob.windows.net/vhds/MyCDrive.vhd'
#>
function Create-BlobSnapshot
{
	param
	(
		[string]$blobUrl = $null	
	)

	$blob = Get-Blob $blobUrl
	return	$blob.CreateSnapshot()
}

<#
.SYNOPSIS
	Retrieves a list of snapshot dates for the requested blob. 
	
.DESCRIPTION
	Retrieves a list of snapshot dates for the requested blob.  This relies on the Storage V2.0 library.  Returns an array of snapshot times.

.PARAMETER blobUri
	The BLOB uri of the BLOB the snapshots are related to.

.NOTES
	Author: Chris Clayton
	Date: 2013/07/11
	Revision: 1.0

.EXAMPLE
	$snapshotTimes = Get-BlobSnapshots 'https://mystorageaccount.blob.windows.net/vhds/MyCDrive.vhd' 
#>
function Get-BlobSnapshots
{
	param
	(
		[string]$blobUrl
	)
	$results = @() 
	[Microsoft.WindowsAzure.Storage.Blob.ICloudBlob]$blob = $null
	
	$urlElements  = Get-BlobUriElements $blobUrl				
	[string]$storageAccountName = $urlElements.Item1
	
	# Get the connection string to be used by the CloudStorageAccount
	[string]$connectionString = Get-StorageConnectionString $storageAccountName $true
	
	if($connectionString -ne $null)
	{		
		[Microsoft.WindowsAzure.Storage.CloudStorageAccount]$storageAccount = [Microsoft.WindowsAzure.Storage.CloudStorageAccount]::Parse($connectionString)
		[Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient]$client = $storageAccount.CreateCloudBlobClient()			
		[Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer]$container = $client.GetContainerReference($urlElements.Item2)
		
		$blobListing = $container.ListBlobs('', $true, [Microsoft.WindowsAzure.Storage.Blob.BlobListingDetails]::Snapshots)
		
		foreach($blob in $blobListing)
		{
			if(($blob.Name -match $urlElements.Item3) -and ($blob.SnapshotTime -ne $null))
			{
				$results += $blob
			}
		}
		
	}	
		
	return $results
}

<#
.SYNOPSIS
	Copies the snapshot version of the BLOB back over the current data.
	
.DESCRIPTION
	Copies the snapshot version of the BLOB back over the current data.  The V2.0 storage APIs must be installed for this to work.  Returns an ICloudBlob object referring to the new BLOB.

.PARAMETER blobUri
	The BLOB uri of the blob to be restored.

.PARAMETER snapshot
	A reference to a BLOB snapshot to be restored.

.NOTES
	Author: Chris Clayton
	Date: 2013/07/11
	Revision: 1.0

.EXAMPLE
	Restore-BlobFromSnapshot 'https://mystorageaccount.blob.windows.net/vhds/MyCDrive.vhd' $juneSnapshot
#>
function Restore-BlobFromSnapshot
{
	param
	(
		[string]$blobUrl,
		$snapshot
	)
	
	$blob = Get-Blob $blobUrl	
	$copyOperation = $blob.StartCopyFromBlob($snapshot, $null, $null, $null, $null)
	
	return Wait-BlobCopyComplete $blobUrl
}
