﻿<#
.SYNOPSIS
	Creates a new snapshot of the virtual machines disks. 
	
.DESCRIPTION
	Creates a new snapshot of the virtual machines disks. This uses the BLOB snapshotting capability so it is recommended that virtual machines are shutdown before the snapshot is taken for consistency.
	
.PARAMETER subscriptionName
	The user friendly subscription name as defined in the Subscriptions.csv file.

.PARAMETER cloudServiceName
	The cloud service that contains the virtual machine snapshot.

.PARAMETER virtualMachineName
	The name of the virtual machine to snapshot.

.PARAMETER shutdownMachine 
	If present the virtual machine will be shutdown before the snapshots are taken.  This is recommended for consistency purposes.  By default the machine will not be shutdown.

.PARAMETER snapshotOsDisk 
	If present this will take a snapshot of the OsDisk.  

.PARAMETER snapshotDataDisks
	If present this will create a snapshot of all data disks.

.NOTES
	Author: Chris Clayton
	Date: 2013/08/30
	Revision: 1.1

.EXAMPLE
	./SnapshotVirtualMachine.ps1 -subscriptionName "ContosoSubscription" -cloudServiceName "ContosoCloud" -virtualMachineName "DC1" -shutdownMachine -snapshotOsDisk -snapshotDataDisks
#>
param
(
	[string]$subscriptionName,				# The user friendly name of the subscription that contains the virtual machine.  This must match the subscription name in the Subscriptions.csv file.
	[string]$cloudServiceName,				# The name of the cloud service that contains the virtual machine.
	[string]$virtualMachineName,			# The name of the virtual machine that is to have the snapshots taken.
	[switch]$shutdownMachine = $false,		# If present the virtual machine will be shutdown first for consistency.  If the virtual machine was running when the script started it will also be restarted when complete.
	[switch]$snapshotOsDisk = $false,		# If present the operating system disk will have a snapshot created.
	[switch]$snapshotDataDisks = $false		# If present then each data disk will have a snapshot created of it.
)

<#
================================================================================================================================================================
														Common Script Header
================================================================================================================================================================
#>

# Import the Windows Azure PowerShell cmdlets
#Import-Module 'C:\Program Files (x86)\Microsoft SDKs\Windows Azure\PowerShell\ServiceManagement\Azure\Azure.psd1'

<#
.SYNOPSIS
	Determines the directory that the current script is running from.
	
.DESCRIPTION
	Determines the directory that the current script is running from.  If the call depth is not set or is set to 0 it assumes that this method is being called
	from within the script body.  If this is being called from within a function adjust the call depth to reflect how many levels deep the call chain is.

.PARAMETER callDepth
	The depth in the call chain that the script is at when this is being called.  If this is called from within the script body it should be set to 0 or from 
	within a function called from the script body it would be 1 etc.

.NOTES
	Author: Chris Clayton
	Date: 2013/07/18
	Revision: 1.0

.EXAMPLE
	[string]$scriptDirectory = Get-ScriptDirectory 0
#>
function Get-ScriptDirectory
{
	param
	(
		[int]$callDepth = 0		# 0 for main script body, add 1 for each call depth
	)
	
	$callDepth++
	
	# Retrieve the MyInvocation variable representitive of the call depth
	$invocation = (Get-Variable MyInvocation -Scope $callDepth).Value
	
	# return the directory portion of the script
	return Split-Path $invocation.MyCommand.Path
}

<#
.SYNOPSIS
	Determines the relative path of an file or directory that is based on the current location the script is running from.
	
.DESCRIPTION
	Determines the relative path of an file or directory that is based on the current location the script is running from.  If the call depth is not set 
	or is set to 0 it assumes that this method is being called from within the script body.  If this is being called from within a function adjust the 
	call depth to reflect how many levels deep the call chain is.

.PARAMETER callDepth
	The depth in the call chain that the script is at when this is being called.  If this is called from within the script body it should be set to 0 or from 
	within a function called from the script body it would be 1 etc.

.NOTES
	Author: Chris Clayton
	Date: 2013/07/18
	Revision: 1.0

.EXAMPLE
	[string]$scriptDirectory = Get-LiteralPath '..\Data\MyData.csv' 0
#>
function Get-LiteralPath
{
	param
	(
		[string]$relativePath,
		[int]$callDepth = 0		# 0 for main script body, add 1 for each call depth
	)
	$callDepth++	
	$scriptDirectory = Get-ScriptDirectory $callDepth 
	
	return [System.IO.Path]::GetFullPath((Join-Path $scriptDirectory $relativePath))
}

<#
================================================================================================================================================================
														Virtual Machine Snapshots
================================================================================================================================================================
#>

<#
.SYNOPSIS
	Creates a snapshot of the requested data and os disks that are attached to a virtual machine.
	
.DESCRIPTION
	Creates a snapshot of the requested data and os disks that are attached to a virtual machine.  For a consistency it is recommended to shutdown the virtual
	machines when possible if they contain multiple VHDs.  

.PARAMETER subscriptionName
	The user friendly name of the subscription that contains the virtual machine.  This must match the subscription name in the Subscriptions.csv file.
	
.PARAMETER subscriptionDataFile
	The data file that the Windows Azure cmdlets will use to store the subscription details.  The default one for the user is not used to reduce the chances of
	collision.
	
.PARAMETER cloudServiceName
	The name of the cloud service that contains the virtual machine.
	
.PARAMETER virtualMachineName
	The name of the virtual machine that is to have the snapshots taken.
	
.PARAMETER shutdownMachineFirst
	If set to true the virtual machine will be shutdown first for consistency.  If the virtual machine was running when the script started it will also be 
	restarted when complete.
	
.PARAMETER includeOsDisk
	If set to true the operating system disk will have a snapshot created.

.PARAMETER includeDataDisks
	If set to true then each data disk will have a snapshot created of it.

.NOTES
	Author: Chris Clayton
	Date: 2013/07/18
	Revision: 1.0

.EXAMPLE
	Create-VirtualMachineSnapshot 'Development' 'c:\snapshots\mydatafile.xml' 'mycloudservice' 'applicationserver' $true $true $true 
#>
function Create-VirtualMachineSnapshot
{
	param
	(
		[string]$subscriptionName,
		[string]$subscriptionDataFile,
		[string]$cloudServiceName,
		[string]$virtualMachineName,
		[bool]$shutdownMachineFirst,
		[bool]$includeOsDisk,
		[bool]$includeDataDisks
	)
	[bool]$wasRunning = $false
	
	Write-Host "Using file: $subscriptionDataFile "
	# Set the Windows Azure Subscription Context to the desired subscription
	Select-AzureSubscription -SubscriptionName $subscriptionName -SubscriptionDataFile $subscriptionDataFile
	
	# Get the requested Windows Azure Virtual Machine
	$vm = Get-AzureVM -ServiceName $cloudServiceName -Name $virtualMachineName

	# Only continue if the virtual machine was found, if not throw an exception
	if($vm -ne $null)
	{
		# if the machine is to be shutdown before taking the snapshot request it now
		if($shutdownMachineFirst -and ($vm.InstanceStatus -eq 'ReadyRole') -and ($vm.PowerState -eq 'Started'))
		{
			$wasRunning = $true
			
			# Do not deprovision the machine so the IP address is not lost etc.
			$vm | Stop-AzureVM -StayProvisioned | Out-Null

			# Wait for the machine to shutdown
			do
			{
				Start-Sleep -Seconds 5
				$vm = Get-AzureVM -ServiceName $cloudServiceName -Name $virtualMachineName
			} while(($vm.InstanceStatus -eq 'ReadyRole') -and ($vm.PowerState -eq 'Started'))
		}

		# Create the snapshot of the operating system disk
		if($includeOsDisk)
		{
			$osDisk = $vm | Get-AzureOSDisk 
				
			if($osDisk -ne $null)
			{			
				Create-BlobSnapshot $osDisk.MediaLink											
			}		
		}
		
		# Create the snapshot of the data disks
		if($includeDataDisks)
		{
			$dataDisks = $vm | Get-AzureDataDisk
			
			foreach($dataDisk in $dataDisks)
			{
				Create-BlobSnapshot $dataDisk.MediaLink
			}			
		}			
		
		# if the machine was shutdown to perform the snapshot restart it
		if($wasRunning)
		{
			# Do not deprovision the machine so the IP address is not lost etc.
			$vm | Start-AzureVM | Out-Null			
		}
	}
	else
	{
		# Throw an exception the user running the script knows a problem occurred
		throw "An error occurred trying to obtain the virtual machine named $virtualMachine) information in the $cloudServiceName cloud service."
	}
}

<#
================================================================================================================================================================
														Script Body
================================================================================================================================================================
#>

# Add the snapshot common script functions
.$(Get-LiteralPath '.\Common\RepositoryCommon.ps1')

# Determine the required file locations based on relative paths
[string]$subscriptionsFileName = Get-LiteralPath '.\Subscriptions.csv'
[string]$subscriptionDataFile = Get-LiteralPath '.\SubscriptionData.xml'
[string]$vmExportFile = Get-LiteralPath ".\$($cloudServiceName)-$($virtualMachine).xml"

# Prepare the subscription data by moving entries from the Subscriptions.csv file into the Windows Azure data file.
Prepare-SubscriptionDataFile $subscriptionsFileName $subscriptionDataFile

# Generate a snapshot of the entire virtual machine
Create-VirtualMachineSnapshot $subscriptionName $subscriptionDataFile $cloudServiceName $virtualMachineName $shutdownMachine $snapshotOsDisk $snapshotDataDisks

# Clear subscription entries		
Clear-SubscriptionData $subscriptionDataFile

