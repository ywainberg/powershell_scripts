﻿Configuration TimeSet
{

param ([string]$ServerName)

Node $ServerName

    {

  #check for ntp server setting

  Registry ntp

        {

   Ensure='Present'
   Key= 'HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\W32Time\Parameters'
   ValueName ='Type'
   ValueData = 'NT5DS'
   
    }
        
        {
  #send output to mail recipient if ntp value is not correct 
  Send-MailMessage -From 'corporate@example.com' -To 'yaronw@example.com' -Subject "Time Diffrence Between $ServerName VS NTP" -Body "$TIMEDIFF" -SmtpServer 172.16.0.90
    
    }

}
         
        }

        


{
  #check for ntp server setting

   $TimeSet = Get-ItemProperty -Path Registry::HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\W32Time\Parameters
   $TimeSet.NtpServer
   $ntp = ($TimeSet.NtpServer).Split(",")[0].substring(0)
   
 
  #check for time diffrence with a domain controller

  $TIMEDIFF = w32tm /stripchart /computer:$ntp /dataonly /samples:5

  #send output to mail recipient

  Send-MailMessage -From 'corporate@example.com' -To 'yaronw@example.com' -Subject "Time Diffrence Between $ServerName VS NTP" -Body "$TIMEDIFF" -SmtpServer 172.16.0.90


}











