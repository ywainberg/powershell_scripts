﻿
[cmdletbinding()]

param(

  [Parameter(mandatory=$true)]
  [string]$User,

  [Parameter(mandatory=$true)]
  [string]$Group
)

Import-Module activedirectory

# get user membership 
$Usergroups = Get-ADUser -Identity $User -Property memberof | foreach {$_.memberof |Get-ADGroup |Select-Object Name} | Select-Object -ExpandProperty Name 
$Groupcompare = $Usergroups| Where-Object {$_ -contains $Group}

# loop all groups from the user group list
foreach($grp in $Usergroups){


  (Get-ADGroup -Identity $grp).Name | Out-Null 

}
# get all group member of main group
$MainGroupmember  = (Get-ADGroupMember -Identity $Group).Name 
foreach($G in $MainGroupmember){

  (Get-ADGroup -Identity $G).name | Out-Null  

}




if ( $Groupcompare -eq $true ) {

  Write-Host "The $User is part of $Group" -ForegroundColor Green -WarningAction SilentlyContinue
}

else{
      Write-Host "The $User is not part of $Group" -ForegroundColor Red

} 
     



