# Script: DownloadTechEdEurope14VideoAndSlides.ps1
# Version: 1.8 - 2014.10.31 - Change default folder to c:\TEE14
# Version: 1.7 - 2014.10.30 - Added Progress Bar to the download
# Version: 1.6 - 2014.10.29 - Added selection for video quality
# Version: 1.5 - 2014.10.28 - Bugfix if comments are equal null - thanks to Daniel Lauritzen (www.gwi.dk) for code review
# Version: 1.4 - 2014.10.28 - Fixed wrong RSS URLs
# Version: 1.3 - 2014.10.27 - Inital TechEd Europe 2014 version
# Author: Peter Schmidt (Exchange MVP, blog: www.msdigest.net)
# Originally published as a SharePoint Conf script by: Vlad Catrinescu (http://absolute-sharepoint.com/2014/03/ultimate-script-download-sharepoint-conference-2014-videos-slides.html)
# I have now updated my TechEd North Americia 2014 script for TechEd Europe 2014
# This script is based on previous versions of my download scripts for TechEd NA 2014, MEC 2014 and LyncConf 2014 (find more info about my script on www.msdigest.net)
# Thanks to David Coulter | @DCtheGeek for feedback on error in download parsing some sessions, in earlier versions of the script.
# If you like it, leave me a comment at www.msdigest.net

Write-host " "
Write-Host "###############################################################################################"  -ForegroundColor Yellow
Write-Host "                                                                                               "  -ForegroundColor Yellow
Write-Host "                  Download Microsoft TechEd Europe 2014 Videos and PPTX                        "  -ForegroundColor Green
Write-Host "                by Peter Schmidt (Exchange MVP) - http://www.msdigest.net                      "  
Write-Host "                                                                                               "  -ForegroundColor Yellow
Write-Host "         Based on the SharePoint Conference download script by Vlad Catrinescu                 "
Write-Host "                                                                                               "  -ForegroundColor Yellow
Write-Host "###############################################################################################"  -ForegroundColor Yellow

[Environment]::CurrentDirectory=(Get-Location -PSProvider FileSystem).ProviderPath 

#Download Progress Bar Function
$colorscheme = (Get-Host).PrivateData
$colorscheme.ProgressBackgroundColor = "black"
$colorscheme.ProgressForegroundColor = "green"
 Function MakingProgress
 {
param(
    [Parameter(Mandatory=$true)]
    [String] $url,
    [Parameter(Mandatory=$false)]
    [String] $localFile = (Join-Path $pwd.Path $url.SubString($url.LastIndexOf('/'))) 
)
    
begin {
	$client = New-Object System.Net.WebClient
    $Global:downloadComplete = $false
    $eventDataComplete = Register-ObjectEvent $client DownloadFileCompleted `
        -SourceIdentifier WebClient.DownloadFileComplete `
        -Action {$Global:downloadComplete = $true}
    $eventDataProgress = Register-ObjectEvent $client DownloadProgressChanged `
        -SourceIdentifier WebClient.DownloadProgressChanged `
        -Action { $Global:DPCEventArgs = $EventArgs }    
}
process {
    Write-Progress -Activity 'Downloading file' -Status $url
    $client.DownloadFileAsync($url, $localFile)
    
    while (!($Global:downloadComplete)) {                
        $pc = $Global:DPCEventArgs.ProgressPercentage
        if ($pc -ne $null) {
            Write-Progress -Activity 'Downloading file' -Status $url -PercentComplete $pc
        }
    }
    Write-Progress -Activity 'Downloading file' -Status $url -Complete
}
end {
    Unregister-Event -SourceIdentifier WebClient.DownloadProgressChanged
    Unregister-Event -SourceIdentifier WebClient.DownloadFileComplete
    $client.Dispose()
    $Global:downloadComplete = $null
    $Global:DPCEventArgs = $null
    Remove-Variable client
    Remove-Variable eventDataComplete
    Remove-Variable eventDataProgress
    [GC]::Collect()    
} 
 }

#Pick file type for download
$title = "Pick a file type"
$message = "What type of files would you like to download?"

$vids = New-Object System.Management.Automation.Host.ChoiceDescription "&Videos", `
    "Download videos only."

$ppt = New-Object System.Management.Automation.Host.ChoiceDescription "&PowerPoints", `
    "Download Powerpoint files only."
	
$both = New-Object System.Management.Automation.Host.ChoiceDescription "&Both", `
    "Download both videos and Powerpoints for all available sessions."

$options = [System.Management.Automation.Host.ChoiceDescription[]]($vids, $ppt, $both)

$result = $host.ui.PromptForChoice($title, $message, $options, 0) 

switch ($result)
    {
        0 {$downloadType = "vid"}
        1 {$downloadType = "ppt"}
		2 {$downloadType = "both"}
    }
	
$downloadType = $downloadType.ToLower()

#Check that downloadType is valid type
If ("vid","ppt","both" -NotContains $downloadType)
        {
            Throw "$($downloadType) is not a valid type! Please use video, powerpoint, or both.  Default is video."
        } 
##

#Pick file type for download
$title2 = "Video Quality"
$message2 = "What Video Quality do you Want to Download?"

$mp4low = New-Object System.Management.Automation.Host.ChoiceDescription "&Low (avg. size 150mb)", `
    "MP3 Low (avg 150mb)."

$mp4high = New-Object System.Management.Automation.Host.ChoiceDescription "&High (avg. size 400mb)", `
    "MP3 High (avg. 400mb)."

$options2 = [System.Management.Automation.Host.ChoiceDescription[]]($mp4low, $mp4high)

$result = $host.ui.PromptForChoice($title2, $message2, $options2, 0) 

switch ($result)
    {
        0 {$downloadQuality = "mp4low"}
        1 {$downloadQuality = "mp4high"}
    }
	
$downloadQuality = $downloadQuality.ToLower()

#Check that downloadQuality is valid type
If ("mp4low","mp4medium","mp4high" -NotContains $downloadQuality)
        {
            Throw "$($downloadQuality) is not a valid Quality selection! Please use Low or High.  Default is Low."
        } 

Write-Host ""
[string]$path= Read-Host "Enter the path you'd like files to be saved to (eg. c:\TEE14\) - default path is C:\TEE14. "
#if($path.Length -eq 0){Write-Host "Default path set to c:\TEE14"}
if($path.Length -eq 0){$path = "c:\TEE14"}

# Grab the RSS feed for the MP4 and PPTX downloads
write-host -ForegroundColor Yellow "Please wait, grabbing RSS feeds..."
$rss = (new-object net.webclient)

# TechEd 2014 Videos
if ($downloadQuality -eq "mp4low")
{
$a = ([xml]$rss.downloadstring("http://channel9.msdn.com/Events/TechEd/Europe/2014/RSS/mp4")) 
}
if ($downloadQuality -eq "mp4medium")
{

}
if ($downloadQuality -eq "mp4high")
{
$a = ([xml]$rss.downloadstring("http://channel9.msdn.com/Events/TechEd/Europe/2014/RSS/mp4high")) 
}
$b = ([xml]$rss.downloadstring("http://channel9.msdn.com/Events/TechEd/Europe/2014/RSS/slides")) 

#Preferably enter something not too long to not have filename problems! cut and paste them afterwards
#$downloadlocation = "C:\TEE14"
$downloadlocation = $path

	if (-not (Test-Path $downloadlocation)) { 
		Write-Host "Folder $fpath dosen't exist. Creating it..."  
		New-Item $downloadlocation -type directory 
	}
set-location $downloadlocation

$ElapsedTime = [System.Diagnostics.Stopwatch]::StartNew()

#Download all the slides
if ($downloadType -ne "vid")
{
Write-Host "DownloadType PPTX chosen:" $downloadType

$b.rss.channel.item | foreach{   
if ( $_.comments -eq $null) { return }
	$code = $_.comments.split("/") | select -last 1	   
	
	# Grab the URL for the PPTX file
	$urlpptx = New-Object System.Uri($_.enclosure.url)  
    $filepptx = $code + "-" + $_.creator + " - " + $_.title.Replace(":", "-").Replace("?", "").Replace("/", "-").Replace("<", "").Replace("|", "").Replace('"',"").Replace("*","")
	$filepptx = $filepptx.substring(0, [System.Math]::Min(120, $filepptx.Length))
	$filepptx = $filepptx.trim()
	$filepptx = $filepptx + ".pptx" 
	if ($code -ne "")
	{
		 $folder = $code + " - " + $_.title.Replace(":", "-").Replace("?", "").Replace("/", "-").Replace("<", "").Replace("|", "").Replace('"',"").Replace("*","")
		 $folder = $folder.substring(0, [System.Math]::Min(100, $folder.Length))
		 $folder = $folder.trim()
	}
	
#REM OUT during test
#    if ($folder.EndsWith("."))
#         {
#            $folder = $folder.substring(0, ($folder.Length) - 1).trim()
#         }

	else
	{
		$folder = "NoCodeSessions"
	}
	
	if (-not (Test-Path $folder)) { 
		Write-Host "Folder $folder dosen't exist. Creating it..."  
		New-Item $folder -type directory 
	}
	
	#text description from session . Thank you VaperWare
	$OutFile = New-Item -type file "$($downloadlocation)\$($Folder)\$($Code.trim()).txt" -Force  
    $Category = "" ; $Content = ""
    $_.category | foreach {$Category += $_ + ","}
    $Content = $_.title.trim() + "`r`n" + $_.creator + "`r`n" + $_.summary.trim() + "`r`n" + "`r`n" + $Category.Substring(0,$Category.Length -1)
   add-content $OutFile $Content

	# Make sure the PowerPoint file doesn't already exist
	if (!(test-path "$downloadlocation\$folder\$filepptx"))     
	{ 	
		# Echo out the  file that's being downloaded
		$filepptx
		#$wc = (New-Object System.Net.WebClient)  

		# Download the MP4 file
		MakingProgress $urlpptx "$downloadlocation\$filepptx"
		#$wc.DownloadFile($urlpptx, "$downloadlocation\$filepptx")
		mv $filepptx $folder 

	}
	}
}
#download all the mp4
if ($downloadType -ne "ppt")
{

Write-Host "DownloadType VID chosen:" $downloadType
# Walk through each item in the feed 
$a.rss.channel.item | foreach{   
if ( $_.comments -eq $null) { return }
	$code = $_.comments.split("/") | select -last 1	   

	# Grab the URL for the MP4 file
	$url = New-Object System.Uri($_.enclosure.url)  
	
	# Create the local file name for the MP4 download
	$file = $code + "-" + $_.creator + "-" + $_.title.Replace(":", "-").Replace("?", "").Replace("/", "-").Replace("<", "").Replace("|", "").Replace('"',"").Replace("*","")
	$file = $file.substring(0, [System.Math]::Min(120, $file.Length))
	$file = $file.trim()
	$file = $file + ".mp4"  
	
	if ($code -ne "")
	{
		 $folder = $code + " - " + $_.title.Replace(":", "-").Replace("?", "").Replace("/", "-").Replace("<", "").Replace("|", "").Replace('"',"").Replace("*","")
		 $folder = $folder.substring(0, [System.Math]::Min(100, $folder.Length))
		 $folder = $folder.trim()
	}
#REM OUT - during test    
#	if ($folder.EndsWith("."))
#         {
#            $folder = $folder.substring(0, ($folder.Length) - 1).trim()
#         }
	else
	{
		$folder = "NoCodeSessions"
	}
	
	# Make sure the MP4 file doesn't already exist
	if (!(test-path "$folder\$file"))     
	{ 	
		if (-not (Test-Path $folder)) { 
			Write-Host "Folder $folder) dosen't exist. Creating it..."  
			New-Item $folder -type directory 
		}
		# Echo out the  file that's being downloaded
		$file
		#$wc = (New-Object System.Net.WebClient)  

		# Download the MP4 file
		MakingProgress $url "$downloadlocation\$file"
		#$wc.DownloadFile($url, "$downloadlocation\$file")
		mv $file $folder
	}
	
		
	}
}

write-host -ForegroundColor Green "Finished!"
Write-Host -ForegroundColor Green "Total download time: " $ElapsedTime.Elapsed.ToString()
