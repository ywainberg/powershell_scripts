﻿# Set computers OU location
$workstations = Get-ADComputer -Filter * -SearchBase "OU=NY,OU=Workstations,DC=AAD,DC=com" | foreach{$_.Name} 

# Test file location
$filecheck = Test-Path c:\AgentInstaller_Win64

# Log file path
$log =  '\\172.16.0.211\Installs\DLP Agents\install_log.txt'

foreach ($node in $workstations)
{
    
    # copy the installation folder to local drive on client computer
    Copy-Item -Path '\\fs-01\DLP Agents\AgentInstaller_Win64' -Destination \\$node\c$ -Recurse
    
    # check that content was transferred succesfully 
    $filecheck
    
    
    
    if($filecheck = $true)
    {
        # invoke the installation on target computer
        Invoke-Command -ComputerName $node -ScriptBlock {C:\AgentInstaller_Win64\install_agent.bat}
    }
    else{
        
        $date = Get-Date
    
        Add-Content $log  "Installation folder was not found for $computer  $date "
    
    }
    
}
