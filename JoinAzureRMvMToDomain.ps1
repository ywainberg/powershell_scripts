﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2016 v5.2.128
	 Created on:   	10/11/2016 11:57
	 Created by:   	yaronw
	 Organization: 	example
	 Filename:     	
	===========================================================================
	.DESCRIPTION
		A description of the file.
#>

$ip = Read-Host 'type the server ip address'
#local cred
$username = "scadmin"
$password = ConvertTo-SecureString "Safe_123456" -AsPlainText -Force
$cred = new-object -typename System.Management.Automation.PSCredential -argumentlist $username, $password
#domain cred
Enter-PSSession -ComputerName $ip -Credential $cred
$username = "scadmin"
$password = ConvertTo-SecureString "Safe_123456" -AsPlainText -Force
$cred = new-object -typename System.Management.Automation.PSCredential -argumentlist $username, $password


$username1 = "example\domainjoin"
$password1 = ConvertTo-SecureString "yQ(^N00)-71EV" -AsPlainText -Force
$cred1 = new-object -typename System.Management.Automation.PSCredential -argumentlist $username1, $password1
Add-Computer -ComputerName $env:COMPUTERNAME -DomainName example.com -OUPath "ou=Application,ou=azure,ou=servers,dc=example,dc=com" -DomainCredential $cred1 -LocalCredential $cred -Restart
Exit-PSSession