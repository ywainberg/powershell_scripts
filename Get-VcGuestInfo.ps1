﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2016 v5.2.120
	 Created on:   	03/05/2016 10:21
	 Created by:   	yaronw
	 Organization: 	example
	 Filename:     	
	===========================================================================
	.DESCRIPTION
		Get info on VMware Guest vm's.
#>
function Get-vmGuestinfo
{
	
	[CmdletBinding()]
	param
	(
		[parameter(Mandatory = $true, Position = 0)]
		[Alias('vc')]
		[string]
		$VCserver,
		
		[parameter(Position = 2)]
		[System.Management.Automation.PSCredential]$Credentials,
		
	
		[Parameter(Position = 1, ParameterSetName = 'single')]
		[Alias('vm')]
		[string]$vmname,
		
	
		[Parameter(Position = 1, ParameterSetName = 'all')]
		[Alias('all')]
		[switch]$AllVm
		
		
	
	
	)
	
	
	Connect-VIServer -Server $VCserver -Force |Out-Null
	if ($AllVm)
	{
		$vmlist = Get-VMGuest -VM * | Where-Object{ $_.state -eq 'Running' }
		
		
		foreach ($vm in $vmlist)
		{
			
			$Name = $vm.VmName
			$ipaddress = ($vm.IPaddress)[0]
			$OSinfo = $vm.OSFullName
			
			
			
			$il_vm = New-Object -TypeName System.Management.Automation.PSObject
			
			$il_vm | Add-Member -NotePropertyName 'Name' -NotePropertyValue $Name
			$il_vm | Add-Member -NotePropertyName 'IP' -NotePropertyValue $ipaddress
			$il_vm | Add-Member -NotePropertyName 'OS' -NotePropertyValue $OSinfo
			Write-Output $il_vm
		}
	}
	elseif ($vmname)
		{
		Get-VMGuest -VM $vmname |select Vmname,IPaddress,OSfullName
	}
}
