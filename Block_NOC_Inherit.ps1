﻿
Write-Host 'Please follow the instruction to stop mailbox inherit for a NOC user' 
[System.Reflection.Assembly]::LoadWithPartialName('Microsoft.VisualBasic') | Out-Null
$wshell = New-Object -ComObject Wscript.Shell
$NOC_LIST = ('tech-alerts@example.com','NOC@example.com','tech-support@example.com')
$noc_user = [Microsoft.VisualBasic.Interaction]::InputBox('Please enter NOC Username', 'Username') 

Write-Verbose 'Connecting to Exchange Online' 

# Log in to Exchange Online 
$LiveCred = Get-Credential -Message 'Enter Global Admin credentials'
$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $LiveCred -Authentication Basic -AllowRedirection -WarningAction SilentlyContinue
Import-PSSession $Session -AllowClobber |Out-Null

Write-Verbose "Remove User Permission for the mailbox's" 


# Remove User Permission for the mailbox's
Remove-MailboxPermission -Identity NOC@example.com -User $noc_user -AccessRights FullAccess -Confirm:$false |Out-Null
Remove-MailboxPermission -Identity Tech-Alerts@example.com -User $noc_user -AccessRights FullAccess -Confirm:$false |Out-Null
Remove-MailboxPermission -Identity Tech-Support@example.com -User $noc_user -AccessRights FullAccess -Confirm:$false |Out-Null

Write-Verbose "Add User Pemission to NOC mailbox's without automap" 

# Add User Pemission to NOC mailbox's without automap
Add-MailboxPermission -Identity NOC@example.com -User $noc_user -AccessRights FullAccess -InheritanceType all -AutoMapping:$false | Out-Null
Add-MailboxPermission -Identity Tech-Alerts@example.com -User $noc_user -AccessRights FullAccess -InheritanceType all -AutoMapping:$false | Out-Null
Add-MailboxPermission -Identity Tech-support@example.com -User $noc_user -AccessRights FullAccess -InheritanceType all -AutoMapping:$false | Out-Null

# Add User SendAs Permission to mailbox

Add-RecipientPermission NOC@example.com -AccessRights SendAs -Trustee $noc_user
Add-RecipientPermission Tech-Alerts@example.com -AccessRights SendAs -Trustee $noc_user
Add-RecipientPermission Tech-support@example.com -AccessRights SendAs -Trustee $noc_user


foreach($mailbox in $NOC_LIST){
  $test_accessrights =  (Get-MailboxPermission -Identity $mailbox -User $noc_user).AccessRights
  $test_inherited =  (Get-MailboxPermission -Identity $mailbox -User $noc_user).IsInherited

  if (($test_accessrights -eq 'FullAccess') -and ($test_inherited -eq $false))

  {
    Write-Host 'The Process is successfully' -ForegroundColor Cyan
    $wshell.Popup("Operation Completed successfully for mailbox $mailbox",0,'Done',0x1)

  }
  else

  {

    Write-Host "The Process failed for $mailbox" -ForegroundColor Cyan
    $wshell.Popup('Operation failed',0,'Done',0x1)
  }
}
Get-PSSession | where {$_.ComputerName -eq 'outlook.office365.com'} | Remove-PSSession