﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2016 v5.2.118
	 Created on:   	07/04/2016 16:48
	 Created by:   	yaronw
	 Organization: 	example
	 Filename:   ChangeADUserToLowerCase.ps1  	
	===========================================================================
	.DESCRIPTION
		Script to change UserPrincipalName and SamAccountName to Lower Case .

	.NOTES
		Access to a domain controller is required is required(console or remoting)
		Set permission on AD users object is required
#>
function Set-AdlowerCase
{
	[cmdletbinding(SupportsShouldProcess = $true)]
	
	param
	(
		[parameter(Mandatory = $true,
				  HelpMessage = 'Specify full domain name')]
		[string]$DomainName,
		
		[parameter(Mandatory = $true,
				   HelpMessage = 'Specify OU path for the desired users')]
		[string]$LdapPath
	
	)
	
	
	Import-Module ActiveDirectory
	

	$DomainName = "@$DomainName"
	
	
	$domain_users = Get-ADUser -Filter * -SearchBase $LdapPath | Where-Object{ $_.UserPrincipalName -match $DomainName } | Select-Object -ExpandProperty SamAccountName
	
	foreach ($user in $domain_users) { Set-ADUser -Identity $user -SamAccountName ($user).tolower() }
	
	foreach ($user in $domain_users) { Set-ADUser -Identity $user -UserPrincipalName ($user.tolower() + $DomainName) }
	
	
	
}