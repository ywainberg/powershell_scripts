﻿<# 
#divided step by functions
#add checkpoint in critical steps
1.Create Virtual Server infrastracture -checkpoint
2.install vm os -checkpoint
3.join to domain - checkpoint
4.install desired roles -checkpoint
5.set security access
6.install application requirment -checkpoint
7.verify installation and application test



workflow <workflow name>
{
    parallel
    {
       <Activity1>
       <Activity2>

       sequence 
       {
           <Activity3>
           <Activity4>
           ...
       }
        ...
    }
}




#>

workflow new-srvset
{
	
	param (
		[Parameter(Mandatory = $true)]
		[string[]]$PSComputerName,
		[Parameter(Mandatory = $true)]
		[string[]]$Size,
		[Parameter(Mandatory = $true)]
		[string[]]$OS,
		[Parameter()]
		[string[]]$Vlan,
		[Parameter(ParameterSetName = 'disk')]
		[int]$AdditionalDisk,
		[Parameter(ParameterSetName = 'disk')]
		[string[]]$diskSize
		
		
	)
	
	
	foreach - parallel (item in collection)
	{
		
	}
	
	inlinescript
	{
		
		
		
		$rgName = "IT"
		$locName = "NorthEurope"
		$vnetName = "SC_NET"
		
		$subnetIndex = 1
		$name = "srv-azr-dbod21"
		
		$vnet = Get-AzureRmvirtualNetwork -Name $vnetName -ResourceGroupName $rgName
		
		$vm = New-AzureRmVMConfig -VMName $name -VMSize Standard_DS3
		$vm | Set-AzureRmVMOSDisk -VhdUri "https://scstandard.blob.core.windows.net/vhds/srv-azr-dbod21_OS.vhd" –Name $name -CreateOption attach -Windows -Caching ReadWrite
		#$vm | Set-AzureRmVMOSDisk -VhdUri "https://scstandard.blob.core.windows.net/vhds/srv-azr-redis2_os.vhd" –Name $name -CreateOption attach -Linux -Caching ReadWrite
		
		#Add-AzureRmVMDataDisk -VM $vm -VhdUri "https://scstandard.blob.core.windows.net/vhds/srv-azr-bopst1-01.vhd" –Name $name -Lun 0 -CreateOption attach 
		
		$nicName = $name + "_nic"
		$pipName = $name + "_pip"
		$domName = $name
		$pip = New-AzureRmPublicIpAddress -Name $pipName –ResourceGroupName $rgName -DomainNameLabel $domName -Location $locName -AllocationMethod Dynamic
		$nic = New-AzureRmNetworkInterface -Name $nicName –ResourceGroupName $rgName -Location $locName -SubnetId $vnet.Subnets[$subnetIndex].Id -PublicIpAddressId $pip.Id -PrivateIpAddress $privIP
		$vm = Add-AzureRmVMNetworkInterface -VM $vm -Id $nic.Id
		
		
		New-AzureRMVM -ResourceGroupName $rgName -Location $locName -VM $vm –Verbose
		
		
		
		
	}
	
	$drive1 = Get-WmiObject win32_volume -Filter "DriveLetter = 'D:'"
	$drive1.DriveLetter = 'G:'
	$drive1.put()
	
	$drive2 = Get-WmiObject win32_volume -Filter "DriveLetter = 'E:'"
	$drive2.DriveLetter = 'D:'
	$drive2.put()
	
	
}
