
$Path = "\\apps\gamefly\"
$Destination = "C:\Admin\GFA_Daily"




$files = Get-ChildItem -Path $Path |Where-Object{$_.Name -match 'GfsSubscriptions' -and $_.CreationTime -gt (Get-Date).AddDays(-1)}
 
 foreach($file in $files){
        $checks = Get-ChildItem -Path $Path -File $file 
            if($checks.Length /1kb -gt 10){
               if (Get-ChildItem -Path $Destination){Remove-Item -Path $Destination\* -Recurse}
                    copy-item -Path $checks.FullName -Destination $Destination -Force   
                    Rename-Item -Path $Destination\$checks -NewName "GFA_DAILY_EXTRACT.csv" -Force
            }
       else {
           $size = [math]::Round(($file.Length /1kb))
           
           Send-MailMessage -SmtpServer 'smtp-relay.gmail.com' -From 'QLIKVIEW@example.com' -To 'qv@example.com' -Subject 'GFA_Daily Error Report' `
           -BodyAsHtml ( @"
       <!DOCTYPE html>
<html>
<head>
<style>
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td {
    padding: 5px;
    text-align: left;
}
</style>
</head>
<body>

<table style="width:50%">
  <caption>GFA Daily Error Report</caption>
  <tr>
    <th>File Name</th>
    <th>Size</th>
  </tr>
  <tr>
    <td>$file</td>
    <td>$size kb</td>
  </tr>
</table>

</body>
</html>
"@)
       }
 }


