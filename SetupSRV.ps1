﻿#requires -version 4
write-host 'Can only run on v4 or up.'
function Set-SRV{

  <#
      .SYNOPSIS
      Setup server initial settings

      .DESCRIPTION
      Setup server initial setting including ip settings,domain and disks

      .PARAMETER ip
      desired ip for the host

      .PARAMETER DNS
      set the dns server for the host

      .PARAMETER ComputerName
      set the desired name for the host

      .PARAMETER Domain
      set your domain name

      .PARAMETER Department
      set the department for ou affiliation 

      .PARAMETER NewDrive
      Determend if adding a new drive is needed

      .EXAMPLE
      Set-SRV -IP '172.16.10.50' -DNS '172.16.10.10, 172.16.10.11' -ComputerName 'myserver'
 
      This command will set the network configuration for the computer 'myserver' 

      .EXAMPLE
      Set-SRV -CurrentIP 172.16.10.50 -ComputerName 'myserver' -Domain 'test.com' 

      This command will join computer 'myserver' to domain 'test.com' and set a new name .

      .EXAMPLE
      Set-SRV -ComputerName 'myserver' -LicenseKey '23sd3-rfrf4-5t554-6t54f-123ed'

      This command will set a license to computer 'myserver'
      
      .EXAMPLE
      Set-SRV -ComputerName 'myserver' -NewDrive

      This command will set a a new drive to computer 'myserver' 

      

      .NOTES
      + Require PS v.4 or up
      + permissiom to add computers to domain and query AD is required (domain admin prefered)
      + New Drive command requier a virtual disk created in VMware or Hyper-V 
      + PowerShell network access to AD
      + Script Exection Policy should be unrestricted
      + DC ip address based on dns ip address
  #>


  [CmdLetBinding()]

  param(
  
    
    [Parameter(Mandatory = $false,ParameterSetName = 'DomainJoin',HelpMessage = 'Please enter current ip address')]
    [ValidateNotNullOrEmpty()]
    [string]
    $CurrentIP,
    
    [Parameter(Mandatory = $false,ParameterSetName = 'Netwrok',Position = 1,HelpMessage = 'Please enter desired ip address')]
    [ValidateNotNullOrEmpty()]
    [string]
    $IP,

    [Parameter(Mandatory = $false,ParameterSetName = 'Netwrok',Position = 2,HelpMessage = 'Enter DNS servers ip addresses sperate by a comma')]
    [ValidateNotNullOrEmpty()]
    [string[]]
    $DNS,
    
    [Parameter(Mandatory = $true, ParameterSetName = 'Drive')]
    [Parameter(Mandatory = $true, ParameterSetName = 'KEY')]
    [Parameter(Mandatory = $true,ParameterSetName = 'Netwrok')]
    [Parameter(Mandatory = $true,ParameterSetName = 'DomainJoin')]
    [Parameter(Mandatory = $true,Position = 0,HelpMessage = 'Enter desired computer name(please keep naming convention)')]
    [ValidateNotNullOrEmpty()]
    [string]
    $ComputerName,
    
    [Parameter(Mandatory = $false,ParameterSetName = 'DomainJoin',HelpMessage = 'Enter full domain name')]
    [string]
    $Domain,
    
    
    [Parameter(Mandatory = $false, ParameterSetName = 'KEY')]
    [string]
    $LicenseKEY,

    
    [Parameter(Mandatory = $false, ParameterSetName = 'Drive')]
    [switch]
    $NewDrive
  
  )  
  
  if ($PsCmdlet.ParameterSetName -eq 'Netwrok'){

    $DG = $IP.Split('.')
    $DG[-1] = '254'
    $DG = $DG -join '.'
    $InterfaceAlias = 'Ethernet'
    $prefix = '24'

  
    $session = New-CimSession -ComputerName $ComputerName -Credential(Get-Credential)
  

    #Set Network Setings
    Write-Verbose 'Setting network configuration'

    New-NetIPAddress -CimSession $session -IPAddress $IP -DefaultGateway $DG -InterfaceAlias $InterfaceAlias -PrefixLength $prefix | Out-Null
  
    Set-DnsClientServerAddress -CimSession $session -InterfaceAlias $InterfaceAlias -ServerAddresses $DNS | Out-Null

    Write-Verbose 'Restarting Network Adapter' 
  
    Restart-NetAdapter -CimSession $session -Name $InterfaceAlias

    $hostip = Get-NetIPConfiguration -InterfaceAlias $InterfaceAlias -CimSession $session
    $host_address = ($hostip.IPv4Address).IPAddress
    $host_dg = ($hostip.IPv4DefaultGateway).nexthop
    $host_dns = ($hostip.DNSServer).ServerAddresses
  
    if ($host_address -eq $IP -and $host_dg -eq $DG -and $host_dns -eq $DNS)
    {

      Write-Host 'Network Settings changed Successfuly' -ForegroundColor Green 
      Write-Host "IPv4Address = $host_address" -ForegroundColor Magenta
      Write-Host "DefaultGateway = $host_dg" -ForegroundColor Magenta
      Write-Host "DNSServer = $host_dns" -ForegroundColor Magenta
   
     }
    else{Write-Host "$ComputerName ip address is $host_address"}

  }
  if($LicenseKEY ){
  
    
    Invoke-Command -ComputerName $ComputerName -ScriptBlock {slmgr.vbs -ipk $LicenseKEY}    
     
  }

 
  if($NewDrive){
        
    $session = New-CimSession -ComputerName $ComputerName -Credential(Get-Credential)
    
        
    Write-Verbose -Message 'Setting new drive'
            
    $offline = Get-Disk -CimSession $session | Where-Object{$_.PartitionStyle -eq 'raw'}
        
    foreach($disk in $offline){
        
      if($disk.IsOffline -eq $true){
        
      Set-Disk -CimSession $session -Number $disk.Number -IsOffline $false }
          
      $disk | Initialize-Disk -CimSession $session -PartitionStyle GPT -PassThru | New-Partition -UseMaximumSize -AssignDriveLetter | Format-Volume -CimSession $session -FileSystem NTFS -Confirm:$false -ErrorAction SilentlyContinue |Out-Null
          
 #     $volume = Get-Partition -CimSession $session| Where-Object{$_.DiskNumber -eq $disk.Number}
  
          
      
   #   $volume | Format-Volume -CimSession $session -FileSystem NTFS -Confirm:$false -ErrorAction SilentlyContinue |Out-Null

      Write-Host 'The new volume set succesfully'
    }
  }
 
  #Join Computer to Domain
  if($PsCmdlet.ParameterSetName -eq 'DomainJoin'){

    
    Write-Verbose 'Join computer to domain'
    
    #$Domain = 'example.com'
    #$CurrentIP = '172.16.0.149'
    #$ComputerName = 'srv-irg-test2'
    Invoke-Command -ComputerName $CurrentIP -Credential (Get-Credential -Message 'Enter local admin credentials') -ScriptBlock { Add-Computer -DomainName $args[0] -Credential (Get-Credential -Message 'Enter Domain Credentials') -NewName $args[1] -Restart} -ArgumentList $Domain, $ComputerName
    
  
  }
  
  
}


function Get-SRV{

  <#
      .SYNOPSIS
      Get server initial settings

      .DESCRIPTION
      Get server initial setting including ip settings and disks

      .PARAMETER ComputerName
      set the desired name for the host

      .PARAMETER DiskSetting
      Get current volumes info

      .PARAMETER NetSetting
      Get IP configuration settings

      .PARAMETER WORKGROUP
      Set different crederntilas for action

      .PARAMETER System
      Get operating system info 

      .EXAMPLE
      Get-SRV -ComputerName 'MyServerName' -NetSetting 
 
      This command will retrive the network configuration for the computer 'MyServerName' .

       .EXAMPLE
      Get-SRV -ComputerName 'MyServerName' -System 
 
      This command will retrive the system information for the computer 'MyServerName' .

      .EXAMPLE
      Get-SRV -ComputerName 'MyServerName' -DiskSetting

      This command will get the volume configuration for the computer 'MyServerName'  .

     
      .NOTES
      + Require PS v.4 or up
      + Script Exection Policy should be unrestricted
     
  #>



  [CmdLetBinding()]

  param(

    [Parameter(Mandatory = $true, Position = 0,HelpMessage='Enter one or more computer names separated by commas.')]
    [ValidateNotNullOrEmpty()]
    [string[]]
    $ComputerName = $env:COMPUTERNAME,

    [Parameter(Mandatory=$false)]
    [switch]
    $NetSetting,

    [Parameter(Mandatory=$false)]
    [switch]
    $DiskSetting,
    
    
    [Parameter(Mandatory=$false)]
    [switch]
    $WORKGROUP,

    [Parameter(Mandatory=$false)]
    [switch]
    $System
     )
     
     if($WORKGROUP){ 
       $session = New-CimSession -ComputerName $ComputerName -Credential(Get-Credential)
     }
     
     else{

            $session = New-CimSession -ComputerName $ComputerName 
     }
     
  if($NetSetting){

    $InterfaceAlias = 'Ethernet'
    $hostip = Get-NetIPConfiguration -CimSession $session -InterfaceAlias $InterfaceAlias 
    $host_address = ($hostip.IPv4Address).IPAddress
    $host_dg = ($hostip.IPv4DefaultGateway).nexthop
    $host_dns = ($hostip.DNSServer).ServerAddresses
    $dhcp = (Get-CimInstance Win32_NetworkAdapterConfiguration -Property * -CimSession $session | Where-Object{$_.IPAddress -eq $host_address}).DHCPEnabled
  

    $COMPUTER = New-Object psobject

    

          $COMPUTER | Add-Member NoteProperty  'ip address' ($host_address)
          $COMPUTER | Add-Member NoteProperty 'Default Gateway' ($host_dg)
          $COMPUTER | Add-Member NoteProperty 'DNS Servers' ($host_dns)
          $COMPUTER | Add-Member NoteProperty 'Adapter Name' ($InterfaceAlias)
          $COMPUTER | Add-Member NoteProperty 'DHCP Enabled' ($dhcp)

    $COMPUTER
           
           
           }

    if($DiskSetting){
    
        $sizing = Get-Volume -CimSession $session |Where-Object{($_.DriveType -like 'fixed') -and ($_.FileSystemLabel -ne 'System Reserved')}
        
        
        foreach($size in $sizing ){
        
          $Remain =  [math]::Round($size.SizeRemaining/ 1gb)
          $current =  [math]::Round($size.Size/ 1gb)
          
        
 
          $disk_info = New-Object psobject
          
      
          $disk_info | Add-Member NoteProperty 'Drive Letter' ($size.DriveLetter)
          $disk_info | Add-Member NoteProperty 'Used Space(GB)' ($current)
          $disk_info | Add-Member NoteProperty 'Free Space(GB)' ($Remain)
      
          $disk_info
      
        }
          
    
    }
    
    if($System){
  
      
      $platform =  (Get-CimInstance win32_computersystem -CimSession $session).partofdomain 
      $architecture = (Get-CimInstance Win32_OperatingSystem -CimSession $session |Select-Object OSArchitecture).OSArchitecture
      $windows_version = ((Get-CimInstance Win32_OperatingSystem -CimSession $session).Name).split('|')[0]
      $memory = [math]::Round(((Get-CimInstance Win32_PhysicalMemory -CimSession $session | Measure-Object -Property capacity -Sum).Sum / 1gb))
      $processor = (Get-CimInstance Win32_ComputerSystem -CimSession $session).NumberOfLogicalProcessors
      
      $mysystem = New-Object psobject
      
      $mysystem |Add-Member NoteProperty 'IsDomainJoint' ($platform)
      $mysystem |Add-Member NoteProperty 'OS Platform' ($architecture)
      $mysystem |Add-Member NoteProperty 'OS Version' ($windows_version)
      $mysystem |Add-Member NoteProperty 'Physical Memory(GB)' ($memory)
      $mysystem |Add-Member NoteProperty 'logical Processors' ($processor)
      
    
      $mysystem
    }
  
}