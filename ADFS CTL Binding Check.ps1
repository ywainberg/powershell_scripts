﻿param
( 
  [switch]$syncproxytrustcerts
)


function checkhttpsyscertbindings()
{

Write-Host; Write-Host("1 - Checking http.sys certificate bindings for potential issues")

$httpsslcertoutput = netsh http show sslcert
$adfsservicefqdn = (Get-AdfsProperties).HostName
$i = 1
$certbindingissuedetected = $false

While($i -lt $httpsslcertoutput.count)
{
        $ipport = $false
        $hostnameport = $false

        if ( ( $httpsslcertoutput[$i] -match "IP:port" ) ) { $ipport = $true }
        elseif ( ( $httpsslcertoutput[$i] -match "Hostname:port" ) ) { $hostnameport = $true }

        # Check for IP specific certificate bindings
        if ( ( $ipport -eq $true ) )
        {
            $httpsslcertoutput[$i]
            $ipbindingparsed = $httpsslcertoutput[$i].split(":")

            if ( ( $ipbindingparsed[2].trim() -ne "0.0.0.0" ) -and ( $ipbindingparsed[3].trim() -eq "443") )
            {
                $warning = "There is an IP specific binding on IP " + $ipbindingparsed[2].trim() + " which may conflict with the AD FS port 443 cert binding." | Write-Warning
                $certbindingissuedetected = $true
            }
            
            $i = $i + 14
            continue
        }

        # check that CTL Store is set for ADFS service binding
        elseif ( $hostnameport -eq $true ) 
        {
            $httpsslcertoutput[$i]
            $ipbindingparsed = $httpsslcertoutput[$i].split(":")
            If ( ( $ipbindingparsed[2].trim() -eq $adfsservicefqdn ) -and ( $ipbindingparsed[3].trim() -eq "443") -and ( $httpsslcertoutput[$i+10].split(":")[1].trim() -ne "AdfsTrustedDevices" ) )

            {
                Write-Warning "ADFS Service binding does not have CTL Store Name set to AdfsTrustedDevices"
                $certbindingissuedetected = $true
            }
        $i = $i + 14
        continue
        }
    $i++
}

If ( $certbindingissuedetected -eq $false ) { Write-Host "Check Passed: No certificate binding issues detected" }

}

function checkadfstrusteddevicesstore()
{
# check for CA issued (non-self signed) certs in the AdfsTrustedDevices cert store
Write-Host; Write-Host "2 - Checking AdfsTrustedDevices cert store for non-self signed certificates"

$certlist = Get-Childitem cert:\LocalMachine\AdfsTrustedDevices -recurse | Where-Object {$_.Issuer -ne $_.Subject}

If ( $certlist.count -gt 0 )
{
    Write-Warning "The following non-self signed certificates are present in the AdfsTrustedDevices store and should be removed"
    $certlist | Format-List Subject
}
Else { Write-Host "Check Passed: No non-self signed certs present in AdfsTrustedDevices cert store" }

}


function checkproxytrustcerts
{
    Param ([bool]$repair=$false)

    Write-Host; Write-Host("3 - Checking AdfsTrustedDevices cert store is in sync with ADFS Proxy Trust config")

    $doc = new-object Xml
    $doc.Load("$env:windir\ADFS\Microsoft.IdentityServer.Servicehost.exe.config")

    $connString = $doc.configuration.'microsoft.identityServer.service'.policystore.connectionString

    $command = "Select ServiceSettingsData from [IdentityServerPolicy].[ServiceSettings]"

    $cli = new-object System.Data.SqlClient.SqlConnection
    $cli.ConnectionString = $connString

    $cmd = new-object System.Data.SqlClient.SqlCommand
    $cmd.CommandText = $command
    $cmd.Connection = $cli

    $cli.Open()

    $configString = $cmd.ExecuteScalar()

    $configXml = new-object XML
    $configXml.LoadXml($configString)

    $rawCerts = $configXml.ServiceSettingsData.SecurityTokenService.ProxyTrustConfiguration._subjectNameIndex.KeyValueOfstringArrayOfX509Certificate29zVOn6VQ.Value.X509Certificate2


    #$ctl = dir cert:\LocalMachine\ADFSTrustedDevices

    $store = new-object System.Security.Cryptography.X509Certificates.X509Store("ADFSTrustedDevices","LocalMachine")
    $store.open("MaxAllowed")

    $atLeastOneMismatch = $false
    $badCerts = @()

    foreach($rawCert in $rawCerts)
    {   

        $rawCertBytes = [System.Convert]::FromBase64String($rawCert.RawData.'#text')
        $cert=New-Object System.Security.Cryptography.X509Certificates.X509Certificate2(,$rawCertBytes)

        $now = Get-Date

        if ( ($cert.NotBefore -lt $now) -and ($cert.NotAfter -gt $now))
        {
            $certThumbprint = $cert.Thumbprint
         $certSubject = $cert.Subject
         $ctlMatch = dir cert:\localmachine\ADFSTrustedDevices\$certThumbprint -ErrorAction SilentlyContinue
         if ($ctlMatch -eq $null)
         {
       $atLeastOneMismatch = $true
          Write-Warning "This cert is NOT in the CTL: $certThumbprint - $certSubject"
   
       if ($repair -eq $true)
       {
        write-Warning "Attempting to repair"
        $store.Add($cert)
        Write-Warning "Repair successful"
       }
                else
                {
                    Write-Warning ("Please install KB.2964735 or re-run script with -syncproxytrustcerts switch to add missing Proxy Trust certs to AdfsTrustedDevices cert store")
                }
   
         }
        }

    }
    $store.Close()
 
    if ($atLeastOneMismatch -eq $false)
    {
     Write-Host("Check Passed: No mismatched certs found. CTL is in sync with DB content")
    }


}

checkhttpsyscertbindings
checkadfstrusteddevicesstore
checkproxytrustcerts($syncproxytrustcerts)
Write-Host; Write-Host("All checks completed.")