﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2016 v5.2.127
	 Created on:   	11/09/2016 11:36
	 Created by:   	yaronw
	 Organization: 	example
	 Filename:     	
	===========================================================================
	.DESCRIPTION
		A description of the file.
#>

function Get-HardDriveSize
{
	
	[CmdletBinding()]
	param
	(
		[Parameter(Mandatory = $true, ValueFromPipeline = $true)]
		$ComputerName,
		
		$Credential
	)
	
	# get calculated properties:
	$prop1 = @{
		Name = 'DriveLetter'
		Expression = { $_.DeviceID }
	}
	
	$prop2 = @{
		Name = 'Free(GB)'
		Expression = { [Math]::Round(($_.FreeSpace / 1GB), 1) }
	}
	
	$prop3 = @{
		Name = 'Size(GB)'
		Expression = { [Math]::Round(($_.Size / 1GB), 1) }
	}
	
	$prop4 = @{
		Name = 'Percent'
		Expression = { [Math]::Round(($_.Freespace * 100 / $_.Size), 1) }
	}
	
	# get all hard drives
	Get-WmiObject -Class Win32_LogicalDisk @PSBoundParameters -Filter "DriveType=3" |
	Select-Object -Property $prop1, $prop2, $prop3, $prop4
	
}