$a = Get-Date -Format d
$zip = 'GameNetworkServer.zip'
$a = $a -replace "/", "_"
$folder="..\release\GNS_"+$a+"_"+ $Env:BUILD_BUILDNUMBER
New-Item -ItemType directory -Path "$folder\\GameNetworkServer"
$gns_folder = "$folder\GameNetworkServer"
Copy-Item -Path "$env:BUILD_ARTIFACTSTAGINGDIRECTORY\\Development-branch-future-build\\DbScripts" -Destination $gns_folder -Recurse
Copy-Item -Path "$env:BUILD_ARTIFACTSTAGINGDIRECTORY\\Development-branch-future-build\\GameNetworkServer\Config" -Destination $gns_folder -Recurse
Copy-Item -Path "$env:BUILD_ARTIFACTSTAGINGDIRECTORY\\Development-branch-future-build\\GameNetworkServer\Src\Bin" -Destination $gns_folder -Recurse
Compress-Archive -Path $gns_folder -DestinationPath $folder\\$zip
Get-ChildItem -Path $gns_folder -Force -Recurse |Sort-Object -Property FullName -Descending |Remove-Item -Recurse -Force
Get-Item -Path $gns_folder |Remove-Item -Recurse -Force


