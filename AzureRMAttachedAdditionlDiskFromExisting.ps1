﻿function Add-AzureExistingDisk
{
	
	
	param
	(
		[parameter(Mandatory = $true)]
		[string]$Name,
		[parameter(Mandatory = $true)]
		[string]$ResourceGroupName,
	)
	
	
	$VirtualMachine = Get-AzureRmVM -ResourceGroupName $ResourceGroupName -Name $Name
	
	$VirtualMachine | Add-AzureRmVMDataDisk -VhdUri 'https://scstoragemain.blob.core.windows.net/vhds/srv-azr-app1_02.vhd' -Name app1_02.vhd -Caching ReadOnly -CreateOption attach -DiskSizeInGB $null
	
	Update-AzureRmVM -ResourceGroupName 'IT' -VM $VirtualMachine -Verbose
	
}

function Add-AzureNewDisk {
	
}