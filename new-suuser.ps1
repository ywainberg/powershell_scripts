﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2016 v5.2.127
	 Created on:   	06/09/2016 10:36
	 Created by:   	yaronw
	 Organization: 	example
	 Filename:     	
	===========================================================================
	.DESCRIPTION
		A description of the file.
#>

	
	Import-Module activedirectory
	
	$username = Read-Host -Prompt 'enter the username'
[System.Security.Securestring]$temp_password = Read-Host -AsSecureString  'enter a temporary password'   
	
	
	###Create a new user based on a current user profile###
	$original = Get-ADUser -Identity $username
	$TargetUserName = ($username + '-su').ToLower()
	$UpnAddress = $TargetUserName + '@' + $env:USERDNSDOMAIN

New-ADUser -SamAccountName $TargetUserName -Instance $username -Enabled $true `
					 -UserPrincipalName $UpnAddress -GivenName $original.GivenName -Surname ($original.Surname + '-su')  `
					 -Name ($original.GivenName + ' ' + $original.Surname + '-su') -DisplayName ($original.GivenName + ' ' +   $original.Surname + '-su') -Description 'CyberArk User' -AccountPassword $temp_password


  #Set-ADAccountPassword -Identity $TargetUserName -NewPassword $temp_password
	##Copy Selected user Groups to the new user you just created### 
	
	Get-ADUser -Identity $username -Properties memberof | Select-Object -ExpandProperty memberof | Add-ADGroupMember -Members $TargetUserName
	
	Start-Sleep -Seconds 2
	
	$u = Get-ADUser -Identity $username
	$DN = $U.DistinguishedName
	$path = $DN.Substring($dn.indexof(',') + 1)
	
	Get-ADUser -Identity $TargetUserName | Move-ADObject -TargetPath $path

