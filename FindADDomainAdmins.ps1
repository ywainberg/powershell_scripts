﻿Import-Module activedirectory
$sender = 'AD_Alerts@example.com'
$recipient = 'yaronw@example.com'
$server = '172.16.0.38'

$new_member = Get-ADReplicationAttributeMetadata 'CN=Domain Admins,CN=Users,DC=example,DC=com' -Server srv-irg-dc1 -ShowAllLinkedValues |Where-Object {$_.FirstOriginatingCreateTime -gt (Get-Date).AddDays(-1)} | Out-File 'c:\NewDA.txt' -Force

Send-MailMessage -From $sender -To $recipient -Subject 'New Changes to domain admins group' -Attachments 'C:\NewDA.txt' -SmtpServer $server  






