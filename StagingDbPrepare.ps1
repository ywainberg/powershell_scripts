$instance_id = 'i-ad705ca3'
$ip = '" "'
$cred = (get-credential)
aws ec2 start-instances --instance-ids $instance_id
aws ec2 wait instance-status-ok --instance-ids $instance_id
Invoke-Command -ComputerName $ip -Credential $cred -Authentication Negotiate -ScriptBlock {Remove-Item 'C:\DownloadDB\WWD_OTT\*.bak'}
Start-Sleep -Seconds 5
Invoke-Command -ComputerName $ip -Credential $cred -Authentication Negotiate -ScriptBlock {powershell 'C:\Tools\OTTDBDownload.ps1'}
Start-Sleep -Seconds 5
Invoke-Command -ComputerName $ip -Credential $cred -Authentication Negotiate -ScriptBlock {Import-Module sqlps -WarningAction SilentlyContinue; Restore-SqlDatabase -ServerInstance "localhost" -Database "GNSDB" -BackupFile(Get-Item C:\DownloadDB\WWD_OTT\*.bak).FullName -ReplaceDatabase}
Invoke-Command -ComputerName $ip -Credential $cred -Authentication Negotiate -ScriptBlock {
$message = (Get-EventLog -LogName Application -EntryType Information -Source 'MSSQLSERVER' -Newest 5 -After (Get-Date).AddHours(-1) |Where-Object{$_.EventID -eq 18267}).Message;
Send-MailMessage -From 'AWS-STAGING@example.com' -To 'ywainberg@example.com' -Subject 'SQL AWS-STAGING IS REFRESHED' -Body $message -SmtpServer 'smtp-relay.gmail.com' -Port 25}
aws ec2 stop-instances --instance-ids $instance_id