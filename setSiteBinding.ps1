﻿<# Usage: Set-SiteBinding -HostName qa1-azr-intrp1.example.com -SiteName TechSupportTools -Port 4440 #>
function Set-SiteBinding{
param(
    [parameter(mandatory = $true)]
    [string]
    $HostName,

    [parameter(mandatory = $true)]
    [int]
    $Port,

    [parameter(mandatory = $true)]
    [string]
    $SiteName

)


 $BindingExsist = Get-WebBinding -name $SiteName -Port $Port | Where-object {$_.bindingInformation -match $Port }
 if(!$BindingExsist)
 {
    New-WebBinding -Name $SiteName  -Port $Port -Protocol https
 }
  
 $certificate = (Get-ChildItem cert:\LocalMachine\MY | where-object { $_.subject -match "CN=$HostName" } | Sort-Object -Property NotAfter | Select-Object -Last 1)
 $thumb = $certificate.Thumbprint
 cd IIS:\SslBindings

 $sslBindingExsist = Get-ChildItem | Where-Object {$_.port -eq $Port}
 if($sslBindingExsist)
 {
    $certificate | Set-Item 0.0.0.0!$Port
 }
 else
 {
    $certificate | New-Item 0.0.0.0!$Port
 }
 cd C:\
}
 
 
 