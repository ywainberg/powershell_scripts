$a = Get-Date -Format d
$zip = 'myuserGateway.zip'
$a = $a -replace "/", "_"
$folder="..\release\GW_"+$a+"_"+ $Env:BUILD_BUILDNUMBER
New-Item -ItemType directory -Path "$folder\\myuserGateway"
$gw_folder = "$folder\myuserGateway"

Copy-Item -Path "$env:BUILD_ARTIFACTSTAGINGDIRECTORY\\Development-branch-future-build\\GameNetworkServer\\Src\\myuserGatewayCommon\\Providers\\myuserGatewayService\*" -Destination $gw_folder -Recurse
Compress-Archive -Path $gw_folder -DestinationPath $folder\\$zip
Get-ChildItem -Path $gw_folder -Force -Recurse |Sort-Object -Property FullName -Descending |Remove-Item -Recurse -Force
Get-Item -Path $gw_folder |Remove-Item -Recurse -Force


