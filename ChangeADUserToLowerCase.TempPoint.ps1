﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2016 v5.2.118
	 Created on:   	07/04/2016 16:48
	 Created by:   	yaronw
	 Organization: 	example
	 Filename:     	
	===========================================================================
	.DESCRIPTION
		A description of the file.
#>
$gwusers = Get-ADUser -Filter * -SearchBase "ou=employees,dc=example,dc=com" | Where-Object{ $_.UserPrincipalName -match '@example.com' } | Select-Object -ExpandProperty SamAccountName
$scusers = Get-ADUser -Filter * -SearchBase "ou=employees,dc=example,dc=com" | Where-Object{ $_.UserPrincipalName -match '@example.com' } | Select-Object -ExpandProperty SamAccountName