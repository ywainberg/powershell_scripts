﻿Function Test-ADGroupMember {
[cmdletbinding()]
  Param (
  [Parameter(Mandatory = $true,Position = 0)]
  [string]$User,
  
  [Parameter(Mandatory = $true,Position = 1)]
  $Group
  
  )

  Trap {Return 'error'}

  If (

    Get-ADUser -Filter "memberOf -RecursiveMatch '$((Get-ADGroup $Group).DistinguishedName)'"  -SearchBase $((Get-ADUser $User).DistinguishedName)) 
  
  {Write-Host "The $User is part of $Group " -ForegroundColor Green}

    Else {Write-Host "The $User is not part of $Group " -ForegroundColor Red}

}
