#Run the following on windows nodes to unable ps-remoting from linux
winrm quickconfig
winrm set winrm/config/service/auth '@{Basic="true"}'
winrm set winrm/config/service '@{AllowUnencrypted="true"}'
winrm set winrm/config/client '@{TrustedHosts="31.168.137.34"}'

#check settings are configured properly
winrm get winrm/config