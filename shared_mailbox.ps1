﻿function set-sharedM {

  [CmdletBinding()]
  
  # Set necessary parameter for email address
  param (
    [parameter(Mandatory=$true)]
        [string]
        $EmailAddress 
      
  
  )
  $LiveCred = Get-Credential -Message 'Enter Global Admin credentials'
  $Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $LiveCred -Authentication Basic -AllowRedirection
  Import-PSSession $Session -AllowClobber -ErrorAction SilentlyContinue 
  Connect-MsolService -Credential $LiveCred
  
  Clear-Host
  
   foreach ($mb in $EmailAddress)
   
   {

     $quota = Set-Mailbox $EmailAddress -ProhibitSendReceiveQuota 10GB -ProhibitSendQuota 9.75GB -IssueWarningQuota 9.5GB -UseDatabaseQuotaDefaults $false
       
     $type = Set-Mailbox $EmailAddress -Type shared
    
     $output =  Get-Mailbox $EmailAddress | Format-Table @{Label = 'id'; Expression ={$_.PrimarySmtpAddress}} ,@{Label = 'Type'; Expression ={$_.RecipientTypeDetails}} -WarningAction SilentlyContinue
    
     Write-Host 'Checking if User type changed...' -ForegroundColor Magenta
     
     $output
     
     Remove-PSSession $Session
   }

}

set-sharedM