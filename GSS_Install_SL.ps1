
######################################################################################
########################### Set-NetworkCard #################################
######################################################################################
    
    
#Remove Mng netwrok card if exist
Write-Verbose -Message "Removing Mng key from registry"
Get-ChildItem hklm:\SYSTEM\ControlSet001\Control\Network\ -Recurse -ErrorAction SilentlyContinue |ForEach-Object{

    if ((Get-ItemProperty -Path $_.Pspath) -match "Mng") {
       Remove-Item -Path $_.Pspath
    }
}
#a json that created by post installation script 
Write-Verbose -Message "Get info from Json"
$net_info = Get-Content D:\net.json |ConvertFrom-Json

#get hardware id of the relevant server by ip address 
Write-Verbose -Message "Setting Network card configuration"
#private netwrok card detailes
$Private_IP = ($net_info |Where-Object{$_.port -eq 0 -and $_.Name -ne 'mgmt'}).primaryIpAddress
$Private_dg = ($net_info |Where-Object{$_.port -eq 0 -and $_.Name -ne 'mgmt'}).primarySubnet.gateway
$Private_sm = ($net_info |Where-Object{$_.port -eq 0 -and $_.Name -ne 'mgmt'}).primarySubnet.netmask
$SubnetMask = [Net.IPAddress]$Private_sm
$Bits = "$( $SubnetMask.GetAddressBytes() | ForEach-Object { [Convert]::ToString($_, 2) } )" -replace '[s0]'
$Private_pl = $Bits.Length

#public netwrok card detailes
$Public_ip = ($net_info |Where-Object{$_.port -eq 1}).primaryIpAddress
$Public_dg = ($net_info |Where-Object{$_.port -eq 1}).primarySubnet.gateway
$Public_sm = ($net_info |Where-Object{$_.port -eq 1}).primarySubnet.netmask
$SubnetMaskPU = [Net.IPAddress]$Public_sm
$Bits = "$( $SubnetMaskPU.GetAddressBytes() | ForEach-Object { [Convert]::ToString($_, 2) } )" -replace '[s0]'
$Public_pl = $Bits.Length

Write-Verbose -Message "remove old Net Team"
#Team Configuration
$Public_LBO = "PublicTeam"
$Private_LBO = "PrivateTeam"
Remove-NetLbfoTeam -Name $Public_LBO -Confirm:$false
Remove-NetLbfoTeam -Name $Private_LBO -Confirm:$false



$Private = 'Private-Team'
$Public = 'Mng'
$pl_dns = '8.8.8.8', '8.8.4.4'
$pr_dns = '10.0.80.11','10.0.80.12'

Write-Verbose -Message "Creating new Team "
#Set Private  Team
New-NetLbfoTeam -Name $Private -TeamMembers “Ethernet 2″,”Ethernet 4” -Confirm:$false
New-NetIPAddress -InterfaceAlias $Private -AddressFamily IPv4 -IPAddress $Private_IP -DefaultGateway $Private_dg -PrefixLength $Private_pl -Confirm:$false
Set-DnsClientServerAddress -InterfaceAlias $Private -ServerAddresses $pr_dns

#Apply windows license 

slmgr.vbs /skms 10.0.76.42:1688

slmgr.vbs /ato 



Remove-NetRoute -InterfaceAlias $Private -DestinationPrefix 0.0.0.0/0 -Confirm:$false

#Set Public Team
New-NetLbfoTeam -Name $Public -TeamMembers “Ethernet 3″,”Ethernet 6” -Confirm:$false

New-NetIPAddress -InterfaceAlias $Public -AddressFamily IPv4 -IPAddress $Public_ip -DefaultGateway $Public_dg -PrefixLength $Public_pl -Confirm:$false
Set-DnsClientServerAddress -InterfaceAlias $Public -ServerAddresses $pl_dns





######################################################################################
########################### function Set-Gamefly  ####################################
######################################################################################

Write-Verbose -Message "removing Veeam agent"
     #remove Veaam backup agent 
    (Get-WmiObject -Class Win32_Product -Filter "Name LIKE 'Veeam Endpoint Backup'").uninstall() |Out-Null
           
Write-Verbose -Message "Download new GSS Version"
Start-Process 'C:\Tools_new\Download_S3_version.bat' -Wait

Write-Verbose -Message "installing latest GSS "
$gss_path = "C:\Temp\GSS\"
$gss_version =  ((Get-ChildItem -Path $gss_path).CreationTime |Sort-Object -Descending)[0]
$gss_file = Get-ChildItem -Path $gss_path |Where-Object{$_.CreationTime -eq $gss_version} |Select-Object -ExpandProperty name

    

Set-Location "C:\temp\GSS"
.\test.exe $gss_file
 




#################################################################################
########################### set-slhost #################################
#################################################################################

Write-Verbose -Message "Set Proccesor Count"
# Set the processor count on windows boot settings
$proccesor = (Get-CimInstance win32_processor |Select-Object -ExpandProperty NumberOfLogicalProcessors)
$numberOfProcessors = $proccesor[0] + $proccesor[1]

bcdedit.exe /set numproc $numberOfProcessors
$Area = ($net_info.hardware.fullyQualifiedDomainName[0]).Substring(0,3)

Write-Verbose -Message "Set Hostname"
#Set Hostname
$ip_code = $Public_ip.Split('.')[3]    

$computerName = "SL-$Area-$ip_code"
Rename-Computer -ComputerName 'GSS-Temp' -NewName $computerName -Restart 






