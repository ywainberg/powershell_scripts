﻿$PageFileSize = ''

$check = Get-CimInstance win32_computersystem -Property automaticmanagedpagefile |Select-Object -ExpandProperty automaticmanagedpagefile

if ($check -eq $true){

$ComputerSystem.AutomaticManagedPagefile = $false

}
Set-CimInstance win32_PageFileSetting -Property @{InitialSize=$PageFileSize;MaximumSize=$PageFileSize}
