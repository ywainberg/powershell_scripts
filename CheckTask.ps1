Import-Module sqlps -WarningAction SilentlyContinue
$server = 
$db = 'GNSDB'
$user = 
$password = 
$query = "SELECT * FROM [GNSDB].[dbo].[ELASTICITY_TASKS]"


$check_table = Invoke-Sqlcmd -ServerInstance $server -Database $db -Username $user -Password $password -Query $query

if (!$check_table){
    
    Restart-Service 'GnsProvisionService'
    $output = write-host "The Session Table is Empty`nWe Started The Service Successfuly" -BackgroundColor Green
    return $output
}

else {
    $output = write-host "The Session Table is Not Empty`nPlease Try Again Later" -BackgroundColor Red
    return $output
}