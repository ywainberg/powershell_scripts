﻿$LiveCred = Get-Credential -Message 'Enter Global Admin credentials'
$exchangeSession = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri "https://outlook.office365.com/powershell-liveid/" -Credential $LiveCred -Authentication "Basic" -AllowRedirection
Import-PSSession $exchangeSession -DisableNameChecking -AllowClobber | Out-Null
Connect-MsolService -Credential $LiveCred