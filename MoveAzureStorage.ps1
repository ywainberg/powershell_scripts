﻿# VHD blob to copy #
$blobName = "example-rodc-srv-azure-rodc-os-1451304046573.vhd" 

# Source Storage Account Information #
$sourceStorageAccountName = "p3portalvhds1bs1h9xts297"
$sourceKey = "p7ZWJ1IHZhondzphvZZ+LiPPUYblNLr6d5Zfnie8pXC/O5FmD5sXFjL5yk8HvwSdT/DbuvO2f7IW+koXP9c6GA=="
$sourceContext = New-AzureStorageContext –StorageAccountName $sourceStorageAccountName -StorageAccountKey $sourceKey  
$sourceContainer = "vhds"

# Destination Storage Account Information #
$destinationStorageAccountName = "examplestorage"
$destinationKey = "V/TOWjEb1oIqPaodN5YF0Rc3rUN7+xv0U0QHp7mTmt2bvhDZxyIfH9xSCosPzlYd8aKXQOSSO/B9TekY+FQzRw=="
$destinationContext = New-AzureStorageContext –StorageAccountName $destinationStorageAccountName -StorageAccountKey $destinationKey  

# Create the destination container #
$destinationContainerName = "rodc"
New-AzureStorageContainer -Name $destinationContainerName -Context $destinationContext 

# Copy the blob # 
$blobCopy = Start-AzureStorageBlobCopy -DestContainer $destinationContainerName `
                        -DestContext $destinationContext `
                        -SrcBlob $blobName `
                        -Context $sourceContext `
                        -SrcContainer $sourceContainer