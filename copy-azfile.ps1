﻿
function copy-toazfs
{
	
<#	
	.NOTES
	===========================================================================
	 Created on:   	21/03/2016 12:44
	 Created by:   	yaronw
	 Organization: 	example
	===========================================================================
	 azcopy must be installed before using this (http://aka.ms/downloadazcopy) 	
	===========================================================================
	.DESCRIPTION
		copy files from azure fs to azure vm.
	
	.PARAMETER $Source
	set the source path for your files(Directory Only)
	
	.PARAMETER $Destination
	choose the desired share on azure for destination
	
	.PARAMETER $ManualDest
	set your own share destination on azure	

	.PARAMETER $Key
	set your own storage key for azure storage account
	
	.PARAMETER $ManualAccount
	set your own azure storage account name
	
	.PARAMETER $Account
    choose an existing classic storage account
	
	.PARAMETER $File
	specify your file you want to copy from azure share
	
	.PARAMETER $AllFiles
	specify that you want all the files in your source path to be upload 
	
	.EXAMPLE
	copy-toazfs -Source c:\ -Destination installs -Key "G+BGDHg0QT9mVTb/eJmrA+/oLGwj3JorEo6aAblWvok32JNDfV1RvLa51fHm2I/roh6AedA6vQW4QmRA+--awsdd" -Account scclassic -File RemoteInstall.log
	
	copy a file from your specifed path to azure with defined classic storage share value set
	
	.EXAMPLE
	copy-toazfs -Source c:\ -ManualDest 'https://scclassic.file.core.windows.net/installs' -Key "G+BGDHg0QT9mVTb/eJmrA+/oLGwj3JorEo6aAblWvok32JNDfV1RvLa51fHm2I/roh6AedA6vQW4QmRA+--awsdd" -ManualAccount scclassic -File RemoteInstall.log

	Copy a file using a manual parameter value with your own specifued storage account,account key and azure destination
	
	.EXAMPLE
	Copy-toazfs -Source c:\ -Destination installs -Key "G+BGDHg0QT9mVTb/eJmrA+/oLGwj3JorEo6aAblWvok32JNDfV1RvLa51fHm2I/roh6AedA6vQW4QmRA+--awsdd" -Account scclassic -AllFile
	
	copy all files in your spercified path to azure with defined classic storage share value set
	#>	
	
	
	[cmdletbinding()]
	
	# start of parameter defenition 
	param (
		
		[Parameter(
			Mandatory = $true,
			 Position = 0,
			 HelpMessage = 'Enter the source path of your file',
			 ParameterSetName = "Auto")]
		[Parameter(ParameterSetName = "Manual")]
		[string]$Source,
		
		[Parameter(
			Mandatory = $true,
			 Position = 1,
			 HelpMessage = 'choose azure destination path',
			 ParameterSetName = "Auto"	)]
		[ValidateSet("installs", "backup")]
		[string]$Destination = "installs",
		
		[Parameter(
				   Mandatory = $true,
				   Position = 1,
				   HelpMessage = 'Enter the destination path',
				   ParameterSetName = "Manual")]
		[string]$ManualDest,
		
	
		[Parameter(
			Mandatory = $true,
			 Position = 2,
			 HelpMessage = 'Hit "tab" to see the azure option',
			 ParameterSetName = "Auto")]
		[Parameter(ParameterSetName = "Manual")]
		[string]$Key,
		
		
		[Parameter(
				   Mandatory = $true,
				   Position = 3,
				   HelpMessage = 'Please enter azure storage account',
				   ParameterSetName = "Manual")]
		[string]$ManualAccount,
		
		
		[Parameter(
			Mandatory = $true,
			 Position = 3,
			 HelpMessage = 'Hit "tab" to see the azure account option',
			 ParameterSetName = "Auto"	)]	
		[ValidateSet("scclassic")]
		[string]$Account = "scclassic",
		
		[Parameter(
			 Position = 4,
			 HelpMessage = 'specify the file you want to copy',
			 ParameterSetName = "Auto")]
		[Parameter(ParameterSetName = "Manual")]
		[string]$File,
		[Parameter(
			 Position = 5,
			 HelpMessage = 'copy all files in the source recursively',
			 ParameterSetName = "Auto")]
		[Parameter(ParameterSetName = "Manual")]
		[switch]$AllFiles

	)
	#end of parameter defenition 
	
	
	# path to azcopy exe
	
	$azcopy = 'C:\Program Files (x86)\Microsoft SDKs\Azure\AzCopy\AzCopy.exe'
	$url = "http://aka.ms/downloadazcopy"
	$path = "$env:USERPROFILE\Downloads\MicrosoftAzureStorageTools.msi"
	$azcopypath = Test-Path $azcopy
	if ($azcopypath -eq $false)
	{
		Write-Host "`nazcopy utility is not installed 
						`nplease download the latest version from the following link 
								`nhttp://aka.ms/downloadazcopy" -ForegroundColor Magenta
		$title = "Download azcopy"
		$message = "Do you want to dowload it ?"
		$yes = New-Object System.Management.Automation.Host.ChoiceDescription "&Yes"
		$no = New-Object System.Management.Automation.Host.ChoiceDescription "&No"
		$options = [System.Management.Automation.Host.ChoiceDescription[]]($yes, $no)
		$result = $host.ui.PromptForChoice($title, $message, $options, 0)
		
		switch ($result) {
			0 {
				
				Invoke-WebRequest -Uri $url -OutFile $path
				Write-Host "azcopy was downloaded succesfuly to $path" -ForegroundColor Green 
				
				
			}
			1 {
				break 
			}
		
			default {
				Invoke-WebRequest -Uri $url -OutFile $path
				Write-Host "azcopy was downloaded succesfuly to $path" -BackgroundColor Green
			}
		}
						  
		
		
		
		
		
	} 
	else
	{
		if ($Destination -eq "installs")
		{
			$Dest = 'https://scclassic.file.core.windows.net/installs'
		}
		elseif ($Destination -eq "backup")
		{
			
			$Dest = 'https://scclassic.file.core.windows.net/backup'
		}
		
		if ($File)
		{
			& $azcopy /Source:$Source /Dest:$Dest /DestKey:$key /Pattern:$File
		}
		elseif ($AllFiles)
		{
			& $azcopy /Source:$Source /Dest:$Dest /DestKey:$key /S
			
		}
		
		
	}
}

#### END OF COPY-TOAZFS FUNCTION  #####



function copy-fromazfs
{
	
<#	
	.NOTES
	===========================================================================
	 Created on:   	21/03/2016
	 Created by:   	yaronw
	 Organization: 	example
	===========================================================================
	 azcopy must be installed before using this (http://aka.ms/downloadazcopy)
	===========================================================================
	.DESCRIPTION
		copy files from azure fs to azure vm.
	
	.PARAMETER $Source
	choose the desired share on azure for source path

	.PARAMETER $Destination
	set the destination path for your files(Directory Only)
	
	.PARAMETER $Manualsrc
	set your own share source on azure	

	.PARAMETER $Key
	set your own storage key for azure storage account
	
	
	.PARAMETER $ManualAccount
	set your own azure storage account name
	
	.PARAMETER $Account
    choose an existing classic storage account
	
	.PARAMETER $File
	specify your file you want to copy from azure share
	
	.PARAMETER $AllFiles
	specify that you want all the files in your source path to be upload 

	.EXAMPLE
	copy-fromazfs -Source installs -Destination C:\ -Key "G+BGDHg0QT9mVTb/eJmrA+/oLGwj3JorEo6aAblWvok32JNDfV1RvLa51fHm2I/roh6AedA6vQW4QmRA+--awsdd" -Account scclassic -File RemoteInstall.log
	
	copy a single file from pre-defined azure classic storage share to your specifed path 
	
	.EXAMPLE
    copy-fromazfs -ManualSrc 'https://scclassic.file.core.windows.net/installs' -Destination c:\ -Key "G+BGDHg0QT9mVTb/eJmrA+/oLGwj3JorEo6aAblWvok32JNDfV1RvLa51fHm2I/roh6AedA6vQW4QmRA+--awsdd" -ManualAccount scclassic -File RemoteInstall.log

	Copy a single file using a manual parameter value with your own specifued storage account,account key and azure destination
	
	.EXAMPLE
	copy-fromazfs -Source installs -Destination C:\ -Key "G+BGDHg0QT9mVTb/eJmrA+/oLGwj3JorEo6aAblWvok32JNDfV1RvLa51fHm2I/roh6AedA6vQW4QmRA+--awsdd" -Account scclassic -File RemoteInstall.log -AllFile
	
	copy all files in pre-defined classic storage share your spercified path to azure with 
	
	
	
	#>	
	
	[cmdletbinding()]
	# start of parameter defenition
	param (
		
		[Parameter(
				   Mandatory = $true,
				   Position = 0,
				   HelpMessage = 'Hit "TAB" for azure source path',
				   ParameterSetName = "Auto")]
		[ValidateSet("installs", "backup")]
		[string]$Source = "installs",
		
		[Parameter(
				   Mandatory = $true,
				   Position = 0,
			 	   HelpMessage = 'Enter azure source path of your file',
			 	   ParameterSetName = "Manual")]
		[string]$ManualSrc,
		
	
		[Parameter(
				   Mandatory = $true,
				   Position = 1,
				   HelpMessage = 'Enter destination path without specifice the file',
				   ParameterSetName = "Auto")]
		[Parameter(ParameterSetName = "Manual")]
		[string]$Destination,

		[Parameter(
				   Mandatory = $true,
				   Position = 2,
				   HelpMessage = 'Hit "tab" to see the option',
				   ParameterSetName = "Auto")]
		[Parameter(ParameterSetName = "Manual")]
		[string]$Key,
	
		[Parameter(
				   Mandatory = $true,
				   Position = 3,
				   HelpMessage = 'Hit "tab" to see the option',
				   ParameterSetName = "Manual")]
		[string]$ManualAccount,
		[Parameter(
				   Mandatory = $true,
				   Position = 3,
				   HelpMessage = 'Hit "tab" to see the option',
				   ParameterSetName = "Auto")]
		[ValidateSet("scclassic")]
		[string]$Account = "scclassic",
		[Parameter(
				   Position = 4,
				   HelpMessage = 'specify the file you want to copy',
				   ParameterSetName = "Auto")]
		[Parameter(ParameterSetName = "Manual")]
		[string]$File,
		[Parameter(
				   Position = 5,
				   HelpMessage = 'copy all files in the source recursively',
				   ParameterSetName = "Auto")]
		[Parameter(ParameterSetName = "Manual")]
		[switch]$AllFiles
		
	)
	#end of parameter defenition 
	
	
	# path to azcopy exe
	
	
	$azcopy = 'C:\Program Files (x86)\Microsoft SDKs\Azure\AzCopy\AzCopy.exe'
	$url = "http://aka.ms/downloadazcopy"
	$path = "$env:USERPROFILE\Downloads\MicrosoftAzureStorageTools.msi"
	$azcopypath = Test-Path $azcopy
	if ($azcopypath -eq $false)
	{
		Write-Host "`nazcopy utility is not installed 
						`nplease download the latest version from the following link 
								`nhttp://aka.ms/downloadazcopy" -ForegroundColor Magenta
		$title = "Download azcopy"
		$message = "Do you want to dowload it ?"
		$yes = New-Object System.Management.Automation.Host.ChoiceDescription "&Yes"
		$no = New-Object System.Management.Automation.Host.ChoiceDescription "&No"
		$options = [System.Management.Automation.Host.ChoiceDescription[]]($yes, $no)
		$result = $host.ui.PromptForChoice($title, $message, $options, 0)
		
		switch ($result)
		{
			0 {
				
				Invoke-WebRequest -Uri $url -OutFile $path
				Write-Host "azcopy was downloaded succesfuly to $path" -ForegroundColor Green
				
				
			}
			1 {
				break
			}
			
			default
			{
				Invoke-WebRequest -Uri $url -OutFile $path
				Write-Host "azcopy was downloaded succesfuly to $path" -BackgroundColor Green
			}
		}
		
		
		
		
		
		
	}

	else
	{
		if ($Source -eq "installs")
		{
			$src = 'https://scclassic.file.core.windows.net/installs'
		}
		elseif ($Source -eq "backup")
		{
			
			$src = 'https://scclassic.file.core.windows.net/backup'
		}
		
		if ($File)
		{
			& $azcopy /Source:$src /Dest:$Destination /SourceKey:$key /Pattern:$File
		}
		elseif ($AllFiles)
		{
			& $azcopy /Source:$src /Dest:$Destination /SourceKey:$key /S
			
		}
		
		
	}
}
#### END OF COPY-FROMAZFS FUNCTION  #####





