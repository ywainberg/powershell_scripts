﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2016 v5.2.118
	 Created on:   	10/04/2016 15:26
	 Created by:   	yaronw
	 Organization: 	example
	 Filename:     	
	===========================================================================
	.DESCRIPTION
		A description of the file.
#>
########################################################################

# Connect to an Azure subscription

Add-AzureRmAccount

# Select your subscription if required

Set-AzureRmContext -SubscriptionId "yoursubscriptionid"


########################################################################

Copy-Item a Vhd from an existing storage account to a new storage account

########################################################################


# Source VHD
$srcVhd = "https://sourcesa.blob.core.windows.net/vhds/sourcevhd.vhd"

# Destination VHD name

$destVhdName = "destvhd.vhd"

# Destination Container Name
$destContainerName = "vhds"

# Source Storage Account and Key

$srcStorageAccount = "sourcesa"
$srcStorageKey = "sourcesakey"

# Target Storage Account and Key

$destStorageAccount = "destsa"
$destStorageKey = "destsakey"

# Create the source storage account context (this creates the context, it does not actually create a new storage account)

$srcContext = New-AzureStorageContext -StorageAccountName $srcStorageAccount -StorageAccountKey $srcStorageKey


# Create the destination storage account context

$destContext = New-AzureStorageContext -StorageAccountName $destStorageAccount -StorageAccountKey $destStorageKey


# Start the copy 

$blob1 = Start-AzureStorageBlobCopy -Context $srcContext -AbsoluteUri $srcVhd -DestContainer $destContainerName -DestBlob $destVhdName -DestContext $destContext -Verbose


# check status of copy

$blob1 | Get-AzureStorageBlobCopyState


########################################################################

Create a Virtual Machine from an existing OS disk

########################################################################

$resourceGroupName = "dest-rg"
$location = "SouthEastAsia"
$storageAccountName = "destsa"

$vmName = "destvm"
$vmSize = "Standard_A1"

$vnetName = "dest-vnet"


# Get storage account configuration for the target storage account

$StorageAccount = Get-AzureRmStorageAccount -ResourceGroupName $resourcegroupName -Name $storageAccountNAme


#Get Virtual Network configuration

$vnet = Get-AzureRmVirtualNetwork -Name $vnetName -ResourceGroupName $resourceGroupName


# Create VM from an existing image


$OSDiskName = "$vmName-C-01"

$vm = New-AzureRmVMConfig -vmName $vmName -vmSize $vmSize

$nic = New-AzureRmNetworkInterface -Name $nicName -ResourceGroupName $resourceGroupName -Location $location -SubnetId $vnet.Subnets[1].Id

$vm = Add-AzureRmVMNetworkInterface -VM $vm -Id $nic.Id

$destOSDiskUri = "https://destsa.blob.core.windows.net/vhds/destvhd.vhd"

# Set the OS disk properties for the new VM. If you are migrating a Linux machine use the -Linux switch instead of -Windows

$vm = Set-AzureRmVMOSDisk -VM $vm -Name $OSDiskName -VhdUri $destOSDiskUri -Windows -CreateOption attach

New-AzureRmVM -ResourceGroupName $resourceGroupName -Location $location -VM $vm



########################################################################