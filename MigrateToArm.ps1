﻿#1
#run sysprep (we can use invoke-command) 
#c:\Windows\System32\Sysprep\sysprep.exe /oobe /generalize /shutdown /quiet 

#2
#Log in to azure and genralize the vm (login-azurermaccount)
#Stop-AzureRmVM -ResourceGroupName <resourceGroup> -Name <vmName>


#optional check
#$status = (Get-AzureRmVM -Name $Name -ResourceGroupName $ResourceGroup -Status | Select-Object -ExpandProperty statuses | Where-Object{ $_.code -match 'powerstate' }).code.split('/')[1]
#if ($status -ne 'deallocated'){Start-Sleep}


#Set-AzureRmVm -ResourceGroupName <resourceGroup> -Name <vmName> -Generalized

#3
#run validation test for vm state
#$vm = Get-AzureRmVM -ResourceGroupName <resourceGroup> -Name <vmName> -Status
#$vm.Statuses

#4
#Create the image 
#Save-AzureRmVMImage -ResourceGroupName <resourceGroupName> -Name <vmName> `
#	   -DestinationContainerName <destinationContainerName> -VHDNamePrefix <templateNamePrefix> `
#			-Path <C:\local\Filepath\Filename.json>
#for example : Save-AzureRmVMImage -ResourceGroupName IT -Name SRV-AZR-OLTP24 -DestinationContainerName vhds -VHDNamePrefix template

#5
#Create vm for the custom image
#run below command
#
#
#Add-Computer -ComputerName $env:COMPUTERNAME -DomainName example.com -OUPath "ou=Application,ou=azure,ou=servers,dc=example,dc=com"
#$list = Get-Content C:\Users\yaronw\Desktop\DB-QA-SERVER-LIST.txt
#
#cred and domain settings
$username = "scadmin"
$password = ConvertTo-SecureString "Safe_123456" -AsPlainText -Force
$cred = new-object -typename System.Management.Automation.PSCredential -argumentlist $username, $password


#env settings

	$resourceGroupName = "IT"
	$location = "NorthEurope"
	$storageAccountName = 'scstandard'
	$vmName = "srv-azr-risk21"
	$vmSize = "Standard_DS3"
	$vnetName = "SC_NET"
	$sku = "Standard_LRS"
	$computername = $vmName
	$imageuri = "https://scstandard.blob.core.windows.net/system/Microsoft.Compute/Images/sql-template/template-osDisk.7216d53d-2283-469d-bc81-baf5f998c2a3.vhd"
	$destOSDiskUri = "https://scstandard.blob.core.windows.net/vhds/$vmName" + "_os.vhd"
  #$DataDiskUri = "https://scstandard.blob.core.windows.net/system/Microsoft.Compute/Images/srv-azr-olap/template-dataDisk-0.258f00c3-b5f2-42f3-bbc0-180654d9ab9f.vhd"
  #$destDATADiskUri = "https://scstandard.blob.core.windows.net/vhds/$vmName" + "_data.vhd"

  #$DataDiskUri2 = "https://scstandard.blob.core.windows.net/system/Microsoft.Compute/Images/srv-azr-olap/template-dataDisk-1.258f00c3-b5f2-42f3-bbc0-180654d9ab9f.vhd"
  #$destDATADiskUri2 = "https://scstandard.blob.core.windows.net/vhds/$vmName" + "_data2.vhd"
	# Get storage account configuration for the target storage account
	
	$StorageAccount = Get-AzureRmStorageAccount -ResourceGroupName $resourcegroupName -Name $storageAccountNAme
	
	
	
	
	#Get Virtual Network configuration
	
	$vnet = Get-AzureRmVirtualNetwork -Name $vnetName -ResourceGroupName $resourceGroupName
	
	
	# Create VM from an existing image
	
	
	$OSDiskName = $vmName
	

$vmconfig = New-AzureRmVMConfig -vmName $vmName -vmSize $vmSize
#Add-AzureProvisioningConfig -VM $vmName -WindowsDomain -AdminUsername $username -Password $DomainPassword -Domain $Domain -DomainPassword $DomainPassword -DomainUserName $DomainUserName -JoinDomain $DomainDNS
#$vmconfig = Add-AzureRmVMDataDisk -VM $vmconfig -Name 'data1' -CreateOption FromImage -SourceImageUri $DataDiskUri -VhdUri $destDATADiskUri -Caching ReadOnly -DiskSizeInGB $null -Lun 1
#$vmconfig = Add-AzureRmVMDataDisk -VM $vmconfig -Name 'data2' -CreateOption FromImage -SourceImageUri $DataDiskUri2 -VhdUri $destDATADiskUri2 -Caching ReadOnly -DiskSizeInGB $null -Lun 2




	$nic = New-AzureRmNetworkInterface -Name "nic_$vmName" -ResourceGroupName $resourceGroupName -Location $location -SubnetId $vnet.Subnets[3].Id -DnsServer 10.60.1.81, 172.16.0.82
	
	
	
	$vm = Set-AzureRmVMOperatingSystem -VM $vmConfig -Windows -ComputerName $computername -Credential $cred -ProvisionVMAgent -EnableAutoUpdate
	$vm = Add-AzureRmVMNetworkInterface -VM $vm -Id $nic.Id
	
	
	
	$vm = Set-AzureRmVMOSDisk -VM $vm -Name $OSDiskName -VhdUri $destOSDiskUri -CreateOption fromImage -SourceImageUri $imageuri -Windows



	New-AzureRmVM -ResourceGroupName $resourceGroupName -Location $location -VM $vm |Out-Null

#Get-AzureRmVM -Name $vmName -ResourceGroupName $resourceGroupName | Set-AzureRmVMCustomScriptExtension -Name "CustomScriptExtension" -VMName $vmName -ResourceGroupName $resourceGroupName -StorageAccountName $storageAccountName -StorageAccountKey "Cp5icbkeYThSUIXf8+jAbEkhDqi12rKq76wTeyOAoTi0CkSbw0kPFwenVP7OCc/xfnHmgXq/2wgDFn85vzlxUg==" -ContainerName "scripts" -FileName "VmprovisionAzurerm.ps1" -Run "VmprovisionAzurerm.ps1"



#}

########################################################################