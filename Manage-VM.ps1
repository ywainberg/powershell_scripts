﻿<#

Author: Yaron Wainberg
Version: 1.0
Version History:

Purpose: manage your VMWare based virtual datacenter with powershell

#>
function Get-VMinfo
{
  
[CmdLetBinding()]

param(

[Parameter(Mandatory = $True,Position = 0)]
[String]$Vcenter,

[Parameter()]
[String[]]
$Host,

[Parameter()]
[String[]]
$VM,

[Switch]
$Store


)
# Import PowerCLI Snapin

    Add-PSSnapin VMware
  
# Connect to a Vcenter
   
    Write-Verbose 'Please wait while connecting to Vcenter instance' 

    Connect-VIServer -Server $Vcenter -WarningAction SilentlyContinue
    
    Write-Progress -Activity "Connecting to $Vcenter" -Status $Vcenter


# Action Block


  
    Get-VMHost -Name $vm | Get-Datastore

}

