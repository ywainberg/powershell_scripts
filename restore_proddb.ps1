﻿

$smtp = new-object Net.Mail.SmtpClient("mailhostname")

$db='DBNAME'
$send = 'sendermailaddress'
$recipient = 'recipientmailaddress'

$file_to_restore=(Get-ChildItem C:\DownloadDB | where{$_.Name -like "$db*"} |Sort-Object -Descending)[0].Name
$log_name=$db+'_log'
SQLCMD  -S localhost -d master -Q "RESTORE DATABASE [$db] FROM DISK='C:\DownloadDB\$file_to_restore' WITH  RECOVERY, MOVE N'$db' TO N'D:\DATA\$db.mdf',  MOVE N'$log_name' TO N'D:\DATA\$log_name.ldf',  NOUNLOAD,  REPLACE,  STATS = 100"

$smtp.Send($send, $recipient, "$db RESTORE", $file_to_restore.Split('.')[0] + " is now ready")

$test_current = SQLCMD  -S localhost -d master -Q "SELECT max(GSH_START_DATETIME) FROM [$db].[dbo].[SESSIONS_HISTORY_TB]"
$current_date = (Get-Date).AddDays(-2).tostring("yyyy-MM-dd")
if (-Not $test_current -match $current_date){$smtp.Send("$send", "$recipient ", "$db RESTORE", "$db is not up to date, last time stamp is $test_current" )}



$regdb='DBNAME'
$reg_file_to_restore=(Get-ChildItem C:\DownloadDB | where{$_.Name -like "$regdb*"} |Sort-Object -Descending)[0].Name
$reg_log_name=$regdb+'_log'
SQLCMD  -S localhost -d master -Q "RESTORE DATABASE [$regdb] FROM DISK='C:\DownloadDB\$reg_file_to_restore' WITH  RECOVERY, MOVE N'$regdb' TO N'D:\DATA\$regdb.mdf',  MOVE N'$reg_log_name' TO N'D:\DATA\$reg_log_name.ldf',  NOUNLOAD,  REPLACE,  STATS = 100"

$smtp.Send($send, $recipient, "$regdb Restore", $reg_file_to_restore.Split('.')[0] + " is now ready")




