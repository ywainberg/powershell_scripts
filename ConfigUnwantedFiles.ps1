﻿<#	
	.NOTES
	===========================================================================
	 Created by:   	yaronw
	 Organization: 	example
	 Filename:     	
	===========================================================================
	.DESCRIPTION
		Find unwanted config file and send a report.


<###

name not include .config ,.dll, .exe ,.pdb, .ashx, .asmx, .html, .aspx ,.asp, .asax
Path:
c:\GW
D:\GW



##>
#>
#Path for files containing the word 'config' in their names
$configpath_d = Get-ChildItem D:\GW\*.* -Recurse -Exclude '*.config', '*.dll', '*.exe','*.pdb', '*.ashx', '*.asmx', '*.html', '*.aspx', '*.asp', '*.asax'
$configpath_c = Get-ChildItem C:\GW\*.* -Recurse -Exclude '*.config', '*.dll', '*.exe', '*.pdb', '*.ashx', '*.asmx', '*.html', '*.aspx', '*.asp', '*.asax'

# HTML style configuration for reports
$Header = @"
<style>
TABLE {border-width: 1px;border-style: solid;border-color: black;border-collapse: collapse;}
TH {border-width: 1px;padding: 3px;border-style: solid;border-color: black;background-color: #6495ED;}
TD {border-width: 1px;padding: 3px;border-style: solid;border-color: black;}
</style>
"@


# Convert output to html and save it in the directory
$report_d = $configpath_d | Select  FullName, CreationTime, LastWriteTime | ConvertTo-HTML -Head $Header | Out-File "c:\ConfigReport\Report_for_d $(Get-Date -Format dd-MM-yyyy).html"
$report_c = $configpath_c | Select  FullName, CreationTime, LastWriteTime | ConvertTo-HTML -Head $Header | Out-File "c:\ConfigReport\Report_for_c_gw $(Get-Date -Format dd-MM-yyyy).html"


# Mail Variabels 
$recipient = 'yaronw@example.com'
$sender = 'config_log@example.com'
$subject = "Unwanted Config FileReport from $env:COMPUTERNAME $(get-date)"
$mail_server = '172.16.0.235'

# Send report to recipient

#Make sure that in older version of powershell we need to add -BodyAsHtml
Send-MailMessage -From $sender -To $recipient -SmtpHost $mail_server -Subject $subject -Body (Get-Content c:\ConfigReport\*.* | Out-String)



