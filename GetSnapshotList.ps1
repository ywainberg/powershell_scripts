﻿<#
.SYNOPSIS
	Retrieves a list of snaphsots associated with the virtual machine.
	
.DESCRIPTION
	Retrieves a list of snaphsots associated with the virtual machine.  This will iterate through each of the data and operating system disks retriving the BLOB snapshots that had previously been created.
	
.PARAMETER subscriptionName
	The user friendly subscription name as defined in the Subscriptions.csv file.

.PARAMETER cloudServiceName
	The cloud service that contains the virtual machine that has the snapshots.

.PARAMETER virtualMachineName
	The name of the virtual machine that has the snapshots.

.PARAMETER maximumDays 
	The maximum number of days of snapshots to display.  Snapshots older than the provided number of days will not be output.

.NOTES
	Author: Chris Clayton
	Date: 2013/08/30
	Revision: 1.1

.EXAMPLE
	./GetSnapshotList.ps1 -subscriptionName "ContosoSubscription" -cloudServiceName "ContosoCloud" -virtualMachineName "DC1" -maximumDays 15
#>
param
(
	[string]$subscriptionName,				# The user friendly name of the subscription that contains the virtual machine.  This must match the subscription name in the Subscriptions.csv file.
	[string]$cloudServiceName,				# The name of the cloud service that contains the virtual machine.
	[string]$virtualMachineName,			# The name of the virtual machine that is to has the snapshots taken of it.
	[int]$maximumDays = 30					# The maximum number of days to go back looking for snapshots
)

<#
================================================================================================================================================================
														Common Script Header
================================================================================================================================================================
#>

# Import the Windows Azure PowerShell cmdlets
Import-Module 'C:\Program Files (x86)\Microsoft SDKs\Windows Azure\PowerShell\ServiceManagement\Azure\Azure.psd1'

<#
.SYNOPSIS
	Determines the directory that the current script is running from.
	
.DESCRIPTION
	Determines the directory that the current script is running from.  If the call depth is not set or is set to 0 it assumes that this method is being called
	from within the script body.  If this is being called from within a function adjust the call depth to reflect how many levels deep the call chain is.

.PARAMETER callDepth
	The depth in the call chain that the script is at when this is being called.  If this is called from within the script body it should be set to 0 or from 
	within a function called from the script body it would be 1 etc.

.NOTES
	Author: Chris Clayton
	Date: 2013/07/18
	Revision: 1.0

.EXAMPLE
	[string]$scriptDirectory = Get-ScriptDirectory 0
#>
function Get-ScriptDirectory
{
	param
	(
		[int]$callDepth = 0		# 0 for main script body, add 1 for each call depth
	)
	
	$callDepth++
	
	# Retrieve the MyInvocation variable representitive of the call depth
	$invocation = (Get-Variable MyInvocation -Scope $callDepth).Value
	
	# return the directory portion of the script
	return Split-Path $invocation.MyCommand.Path
}

<#
.SYNOPSIS
	Determines the relative path of an file or directory that is based on the current location the script is running from.
	
.DESCRIPTION
	Determines the relative path of an file or directory that is based on the current location the script is running from.  If the call depth is not set 
	or is set to 0 it assumes that this method is being called from within the script body.  If this is being called from within a function adjust the 
	call depth to reflect how many levels deep the call chain is.

.PARAMETER callDepth
	The depth in the call chain that the script is at when this is being called.  If this is called from within the script body it should be set to 0 or from 
	within a function called from the script body it would be 1 etc.

.NOTES
	Author: Chris Clayton
	Date: 2013/07/18
	Revision: 1.0

.EXAMPLE
	[string]$scriptDirectory = Get-LiteralPath '..\Data\MyData.csv' 0
#>
function Get-LiteralPath
{
	param
	(
		[string]$relativePath,
		[int]$callDepth = 0		# 0 for main script body, add 1 for each call depth
	)
	$callDepth++	
	$scriptDirectory = Get-ScriptDirectory $callDepth 
	
	return [System.IO.Path]::GetFullPath((Join-Path $scriptDirectory $relativePath))
}

<#
================================================================================================================================================================
														Script specific functions
================================================================================================================================================================
#>

<#
.SYNOPSIS
	Displays a list of snapshots for a virtual machines drives.
	
.DESCRIPTION
	Displays a list of snapshots for a virtual machines drives.  If regular snapshots are taken it is beneficial to define a minimum date of the snapshot.

.PARAMETER cloudServiceName
	The name of the cloud service that contains the virtual machine to be queried.

.PARAMETER virtualMachineName
	The name of the virtual machine to be queried.

.PARAMETER minimumDate
	A date time representing the minimum date that the snapshots should be retrieved for.

.NOTES
	Author: Chris Clayton
	Date: 2013/07/18
	Revision: 1.0

.EXAMPLE
	Get-SnapshotList 'MyCloudService' 'MyVirtualMachine' [DateTime]::UtcNow.AddDays(-30)
#>
function Get-SnapshotList
{
	param
	(
		[string]$cloudServiceName,
		[string]$virtualMachineName,
		[DateTime]$minimumDate
	)	

	# Write the header to the console
	Write-Host "========================================================================="
	Write-Host "VHD Snapshots for $virtualMachineName in $cloudServiceName cloud service."
	Write-Host "========================================================================="

	$virtualMachine = Get-AzureVM -ServiceName $cloudServiceName -Name $virtualMachineName

	# if the virtual machine was retrieved successfully continue
	if($virtualMachine -ne $null)
	{
		# Write the operating system disk header.
		Write-Host ""
		Write-Host "Operating System Disk Snapshots."
		Write-Host "-------------------------------------------------------------------------"
		Write-Host ""
		
		# Get the snapshots for the virtual machines operating system disk
		$osDisk = $virtualMachine | Get-AzureOSDisk 
		$existingSnapshots = Get-BlobSnapshots $osDisk.MediaLink

		# Write the details of the operating system so the user can identify the specific blob.
		Write-Host "Disk Name: $($osDisk.DiskName)"
		Write-Host "VHD: $($osDisk.MediaLink)"

		# Iterate through each of the snapshots and write out the date.
		foreach($snapshot in $existingSnapshots)
		{
			if($snapshot.SnapshotTime -ge $minimumDate)
			{
				Write-Host "$($snapshot.SnapshotTime)"
			}
		}

		# Write the data disk section header.
		Write-Host ""
		Write-Host "Data Disk Snapshots."
		Write-Host "-------------------------------------------------------------------------"

		$dataDisks = $virtualMachine | Get-AzureDataDisk

		# Iterate through the data disks
		foreach($dataDisk in $dataDisks)
		{
			# Write the header fo the specific data disk
			Write-Host ""
			Write-Host "Disk Name: $($dataDisk.DiskName)"
			Write-Host "VHD: $($dataDisk.MediaLink)"
			
			$existingDataDiskSnapshots = Get-BlobSnapshots $dataDisk.MediaLink.AbsoluteUri				
			
			foreach($snapshot in $existingDataDiskSnapshots)
			{
				# If the data disk snapshots were retrieved successfully write the dates out
				if($snapshot.SnapshotTime -ge $minimumDate)
				{
					Write-Host "$($snapshot.SnapshotTime)"
				}
			}
		}	

		# Write the completion footer.
		Write-Host ""
		Write-Host "Done."
		Write-Host "-------------------------------------------------------------------------"
	}
	else
	{
		# An error happened retrieving the virtual machine so let the user know
		Write-Host "The virtual machine could not be retrieved."
		Write-Host "-------------------------------------------------------------------------"
	}
}

<#
================================================================================================================================================================
														Script Body
================================================================================================================================================================
#>


# Add the snapshot common script functions
.$(Get-LiteralPath '.\Common\RepositoryCommon.ps1')

# Determine the required file locations based on relative paths
[string]$subscriptionsFileName = Get-LiteralPath '.\Subscriptions.csv'
[string]$subscriptionDataFile = Get-LiteralPath '.\SubscriptionData.xml'

# Prepare the subscription data by moving entries from the Subscriptions.csv file into the Windows Azure data file.
Prepare-SubscriptionDataFile $subscriptionsFileName $subscriptionDataFile

# Set the current subscription context to the one that contains the virtual machine.
Select-AzureSubscription -SubscriptionName $subscriptionName -SubscriptionDataFile $subscriptionDataFile	

[DateTime]$minimumDate = [DateTime]::UtcNow.AddDays($maximumDays * -1)

# Print out a list of snapshots that relate to the requested virtual machine
Get-SnapshotList $cloudServiceName $virtualMachineName $minimumDate

# Clear subscription entries		
Clear-SubscriptionData $subscriptionDataFile
