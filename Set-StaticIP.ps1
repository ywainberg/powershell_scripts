﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2016 v5.2.120
	 Created on:   	15/05/2016 15:20
	 Created by:   	yaronw
	 Organization: 	example
	 Filename:     	
	===========================================================================
	.DESCRIPTION
		Set LAN Adapter setting to Static IP.
#>
function Set-LanIP  {
	
	param (
	
	[Parameter(Mandatory = $true)]
		[string]$ipaddress,
		
		[Parameter(Mandatory = $true)]
		[ValidateSet ('Home', 'Office')]
		[string]$location
		
		
		
	)
	
	$interface = 'Ethernet'
	
	if ($location -eq 'Office')
	{
		
		$status = (Get-NetIPAddress -InterfaceAlias $interface -AddressFamily IPv4).PrefixOrigin
		
		if ($status -eq 'Manual')
		{
			Set-NetIPAddress -InterfaceAlias $interface -IPv4Address $ipaddress -PrefixLength "24"
			Set-DnsClientServerAddress -InterfaceAlias $interface -ServerAddresses '172.16.0.81', '172.16.0.82'
		}
		else 
		{
			
			Write-Output "The interface	is set to $status"
			}
	}
	
	elseif ($location -eq 'Home')
	{
		
		Set-NetIPInterface -InterfaceAlias $interface -Dhcp Enabled
	}
	
}

