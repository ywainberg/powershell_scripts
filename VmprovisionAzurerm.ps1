﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2016 v5.2.128
	 Created on:   	13/11/2016 16:24
	 Created by:   	yaronw
	 Organization: 	example
	 Filename:     	
	===========================================================================
	.DESCRIPTION
		A description of the file.
#>
#winrm quickconfig -quiet
$username1 = "example\domainjoin"
$password1 = ConvertTo-SecureString "yQ(^N00)-71EV" -AsPlainText -Force
$cred1 = new-object -typename System.Management.Automation.PSCredential -argumentlist $username1, $password1
Add-Computer -ComputerName $env:COMPUTERNAME -DomainName example.com -OUPath "ou=Application,ou=azure,ou=servers,dc=example,dc=com" -Credential $cred1 -Restart
