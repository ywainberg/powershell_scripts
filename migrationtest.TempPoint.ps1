﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2016 v5.2.118
	 Created on:   	11/04/2016 12:01
	 Created by:   	yaronw
	 Organization: 	example
	 Filename:     	
	===========================================================================
	.DESCRIPTION
		A description of the file.
#>
#Resource allocation in Azure RM
$pipName = "test-web"
$rgName = "IT"
$location = "North Europe"
$pip = New-AzureRmPublicIpAddress -Name $pipName -ResourceGroupName $rgName -Location $location -AllocationMethod Dynamic
$subnet1Name = "Vlan2"
$nicname = "$pipName"+'-'+'nic1'
$vnetName = "SC_NET"
$vnetAddressPrefix = "10.60.0.0/16"
$vnetSubnetAddressPrefix = "10.60.2.0/24"

#Use for building new vnet, subnet and nic
#$nic = New-AzureRmNetworkInterface -Name $nicname -ResourceGroupName $rgName -Location $location -SubnetId $vnet.Subnets[0].Id -PublicIpAddressId $pip.Id
#$vnet = New-AzureRmVirtualNetwork -Name $vnetName -ResourceGroupName $rgName -Location $location -AddressPrefix $vnetAddressPrefix -Subnet $subnetconfig
#$subnetconfig = New-AzureRmVirtualNetworkSubnetConfig -Name $subnet1Name -AddressPrefix $vnetSubnetAddressPrefix

#!!Use below if using existing vnet, subnet and nic!!
$nic = Get-AzureRmNetworkInterface -Name $nicname -ResourceGroupName $rgName
$vnet = Get-AzureRmVirtualNetwork -Name $vnetName -ResourceGroupName $rgName
$subnetconfig = Get-AzureRmVirtualNetworkSubnetConfig -Name $subnet1Name -VirtualNetwork $vnet



#VM creation
$vmName = "test-web"
$storageAccName = "scstandard"
$computerName = "test-web"

#Enter a new username and password in the pop-up for the following
$cred = Get-Credential

#Get the storage account where the uploaded image is stored
$storageAcc = Get-AzureRmStorageAccount -ResourceGroupName $rgName -Name $storageAccName

#Set the VM name and size
#Use "Get-Help New-AzureRmVMConfig" to know the available options for -VMsize
$vmConfig = New-AzureRmVMConfig -VMName $vmName -VMSize "Standard_A2>"

#Set the Windows operating system configuration and add the NIC
$vmNeti = Add-AzureRmVMNetworkInterface -VM $vmConfig -Id $nic.Id

#Create the OS disk
$osDiskName = "test-web-os"

#Create the OS disk URI
$osDiskUri = "https://scstandard.blob.core.windows.net/vhds/test-web-001-test-web-os-1458030757737.vhd"

#Configure the OS disk to be created from image (-CreateOption fromImage) and give the URL of the uploaded image VHD for the -SourceImageUri parameter

#You can find this URL in the result of the Add-AzureRmVhd cmdlet above
$vmOsDisk = Set-AzureRmVMOSDisk -VM $vmNeti -Name $osDiskName -CreateOption frominage -SourceImageUri $osDiskUri -Windows

#Create the new VM
New-AzureRmVM -ResourceGroupName $rgName -Location $location -VM $vmOsDisk