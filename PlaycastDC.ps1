

function Play-DCclient () {

#START OF PARAMETERS
  param(


[Parameter(Mandatory= $true,HelpMessage="PROD-197,QA-65,STAG-134")]
[ValidateSet("ipaddress" , "ipaddress" , "" "")]
[net.ipaddress]$GW,


[Parameter(HelpMessage="select if the gw in https")]
[switch]
$SGW,

[Parameter(HelpMessage="Please enter Area ID")]
[int]
$Area=0,

[Parameter(HelpMessage="Please select a Custumer",ParameterSetName="external_cu")]
[ValidateSet("Samsung","Amazon","Swisscom")]
[string]
$CusIDWithStore,


[Parameter(HelpMessage="Please select a Custumer")]
[ValidateSet("Samsung","Amazon","Swisscom")]
[string]
$CusIdGf,

[Parameter(HelpMessage="enter User ID ",ParameterSetName="external_cu")]
[string]
$UID,

[Parameter(HelpMessage="enter Hardware ID ",ParameterSetName="external_cu")]
[string]
$HID,


[Parameter(HelpMessage="enter the desired GSS ID ")]
[int]
$GSS_ID,

#put in advance
#try to pull from internet list of ip from varius cuntries
[Parameter(HelpMessage="enter an ip address to simulate proxy")]
[net.ipaddress]
$Proxy,



[Parameter(HelpMessage=" ")]
[string]
$Language,

#check box for -osd parameter 

[Parameter(HelpMessage=" ")]
[string]
$osd

  )  
#END OF PARAMETERS

#Path to DC program
$DC_PATH = "C:\myuser\myuserDC\"
Set-Location $DC_PATH







    .\myuserdc.exe -gw $gw -sa AREAID=0 GSSID=26267
    






}

