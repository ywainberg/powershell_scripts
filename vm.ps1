﻿# Dynamic resource Pool Calculator by Eric Sloof (esloof@ntpro.nl) http://www.ntpro.nl
#
# Imagine you work in an environment with more than 1500 virtual machines and the numbers 
# are still growing rapidly. When you work with a lot of resource pools, it’s an administrative 
# burden to recalculate all your pools after you deploy some new virtual machines. 
# This Dynamic Resource Pool Calculator recalculates all your resource pools. 
# Depending on the number of virtual machines per resource pool, the number of reservations for 
# memory and CPU is automatically lifted after running this the Dynamic Resource Pool Calculator. 
# The script also respects the existing individual virtual machine reservations and adds those 
# value’s to the total RP sum.

Get-PSSnapin -registered | Add-PSSnapin -passthru -ErrorAction SilentlyContinue
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Drawing") 
$Voice = New-Object -com SAPI.SpVoice # This is a little add on, the script will actually talk to us.
$MemFactor = 2
$CPUFactor = 128

#When a virtual machine doesn’t have a reservation, the virtual machine memory is used to calculate 
#the Resource Pool reservation. When the $MemFactor is 2, the virtual machine memory is divided by 2. 
#This means a Virtual machine with 1024 MB lifts the Resource Pool  reservation with 512 MB.
#The $CPUFactor is multiplied by the number of CPU’s. This means a virtual machine with 2 CPU ‘s will 
#create a lift of 1024 MHz on the Resource Pool.

#This script can present a GUI to the end user or can be run interactive from the PS command line. 
#If no arguments are specified, the GUI starts. If a resource pool is passed as an argument, 
#this resource pool will be processed without the GUI.
$Server = Connect-VIServer -Server srv-irg-vc5.example.com
if ($args.count -eq 0)
	{$Server = Connect-VIServer -Server srv-irg-vc5.example.com
	$ResourcePool = Get-ResourcePool -Server $Server -Name $args
	$Mem = 0
	$Cpu = 0
	$NumberOfVirtualMachines = 0
   $23 = Get-VM -Location $ResourcePool
	foreach ($VM in $23) 
		{$MyVM = get-view $VM.ID;$VMMemReservation = $MyVM.Config.MemoryAllocation.Reservation
			if ($VMMemReservation -eq 0) 
				{$Mem = $Mem + ($VM.MemoryMB/$MemFactor)}
			else 
				{$Mem = $Mem + $VMMemReservation}
				$VMCPUReservation = $MyVM.Config.CpuAllocation.Reservation
			if ($VMCPUReservation -eq 0) 
				{$Cpu = $Cpu + ($VM.NumCpu*$CPUFactor)}
			else 
				{$Cpu = $Cpu + $VMCPUReservation}}
		Set-Resourcepool -Resourcepool $ResourcePool -MemReservationMB ($Mem) -CpuReservationMhz ($Cpu)
		Write-Output "Mem:" $ResourcePool.MemReservationMB "CPU:" $ResourcePool.CpuReservationMhz
	Disconnect-viserver -Server $Server -Confirm:$false}
else
	{# There are no arguments so, we are going to build a user interface based on Windows controls, so we have to include them.
	#Connect to an ESX or Virtual Center Server, if one is provided
		$Voice.Speak( "Welcome to the Dynamic Resource Pool Calculator......", 1 )
		$Server = Connect-VIServer
	#Let’s create a placeholder for all the objects and start with creating a form
	$objForm = New-Object System.Windows.Forms.Form 
	$objForm.Text = "    NTPRO.NL Dynamic Resource Pool Calculator"
	$objForm.Size = New-Object System.Drawing.Size(294,258) 
	$objForm.StartPosition = "CenterScreen"
	$objForm.FormBorderStyle = "FixedToolWindow" 

	#Let's create a button, this one also sets the resource pool
	$OKButton = New-Object System.Windows.Forms.Button
	$OKButton.Location = New-Object System.Drawing.Size(12,194)
	$OKButton.Size = New-Object System.Drawing.Size(120,23)
	$OKButton.Text = "Set Resource Pool"
	$objForm.Controls.Add($OKButton)
	$OKButton.Add_Click({$SelectedResourcePool = Get-ResourcePool -Name ($objListBox.SelectedItem)
		# The script has become event drivven, at this point the OK button is clicked
		Set-Resourcepool -Resourcepool $SelectedResourcePool -MemReservationMB ($AddMem) -CpuReservationMhz ($AddCpu)
		$objText.Text = "Done"
		$Voice.Speak( "The resource pool is adjusted", 1 )
		$objProgressBar.Value = 0})

	#Let's create an exit button.
	$CancelButton = New-Object System.Windows.Forms.Button
	$CancelButton.Location = New-Object System.Drawing.Size(152,194)
	$CancelButton.Size = New-Object System.Drawing.Size(120,23)
	$CancelButton.Text = "Exit"
	$CancelButton.Add_Click({$objForm.Close()})
	$objForm.Controls.Add($CancelButton)

	#Let's create a label.
	$objLabel = New-Object System.Windows.Forms.Label
	$objLabel.Location = New-Object System.Drawing.Size(9,6) 
	$objLabel.Size = New-Object System.Drawing.Size(252,13) 
	$objLabel.Text = "Please select the Resource Pool to be recalculated:"
	$objForm.Controls.Add($objLabel) 

	#Let's create a Listbox.
	$objListBox = New-Object System.Windows.Forms.ListBox 
	$objListBox.Location = New-Object System.Drawing.Size(12,22) 
	$objListBox.Size = New-Object System.Drawing.Size(260,69) 
	$objListBox.Height = 80
	$objListBox.Sorted = $True
	$objListBox.Font = "Verdana"
	$objListBox.Forecolor = "blue"
	$objForm.Controls.Add($objListBox) 

	#When the end user selects a resource pool in the listbox. This resource pool is pre-calculated. 
	#The selection events starts the pre-calculation procedure.
	$objListBox.Add_Click({$SelectedResourcePool = Get-ResourcePool -Name ($objListBox.SelectedItem)
		$Voice.Speak($objListBox.SelectedItem, 1)
		$VirtualMachines = (Get-VM -Location $SelectedResourcePool)
		$objProgressBar.Value = 0
		$AddMem = 0
		$AddCpu = 0
		$OKButton.Enabled = $False
		$NumberOfVirtualMachines = $VirtualMachines.Length
		if ($VirtualMachines.Name.Length -gt 1) 
			{$NumberOfVirtualMachines = 1}
		if ($NumberOfVirtualMachines -ge 1) 
			{$VMChunk = (100/$NumberOfVirtualMachines)
				foreach ($VM in $VirtualMachines) 
					{if ($objProgressBar.Value -le (100-$VMChunk)) 
						{$objProgressBar.Value = $objProgressBar.Value + $VMChunk}
					$MyVM = get-view $VM.ID
					$VMMemReservation = $MyVM.Config.MemoryAllocation.Reservation
					if ($VMMemReservation -eq 0) 
						{$AddMem = $AddMem + ($VM.MemoryMB/$MemFactor)}
					else 
						{($AddMem = $AddMem + $VMMemReservation)}
					$VMCPUReservation = $MyVM.Config.CpuAllocation.Reservation
					if ($VMCPUReservation -eq 0) 
						{$AddCpu = $AddCpu + ($VM.NumCpu * $CPUFactor)}
					else 
					{($AddCpu = $AddCpu + $VMCPUReservation)}}
				$objProgressBar.Value = 100
				$MyText = "Resource Pool " + $SelectedResourcePool.Name + " contains " + $NumberOfVirtualMachines + " VM's" + "`r`n"
				$MyText = $MyText + "Old Resource Pool Mem Reservation MB" + ": " + ($SelectedResourcePool.MemReservationMB)+ "`r`n" 
				$MyText = $MyText + "Old Resource Pool CPU Reservation Mhz" + ": " + ($SelectedResourcePool.CpuReservationMhz)+ "`r`n"
				$MyText = $MyText + "New Resource Pool Mem Reservation MB" + ": " + ($AddMem)+ "`r`n"
				$MyText = $MyText + "New Resource Pool CPU Reservation Mhz" + ": " + ($AddCpu)}
			else
				{$MyText = "Resource Pool " + $SelectedResourcePool.Name + " contains no VM's" + "`r`n"}
			$OKButton.Enabled = $True; 
			$objText.Text = $MyText			
			#$Voice.Speak( $MyText, 1 ) Little to much ;-)
		})

	#Let's create a Progressbar
	$objProgressBar = New-Object System.Windows.Forms.ProgressBar
	$objProgressBar.Value = 0
	$objProgressBar.Location = New-Object System.Drawing.Size(12,97)
	$objProgressBar.Size = New-Object System.Drawing.Size(260,14) 
	$objForm.Controls.Add($objProgressBar) 
	$objForm.Topmost = $True
	$objForm.Add_Shown({$objForm.Activate()})

	#Let's create a Textbox
	$objText = New-Object System.Windows.Forms.Textbox
	$objText.Location = New-Object System.Drawing.Size(12,117)
	$objText.Multiline = $true
	$objText.Size = New-Object System.Drawing.Size(260,71) 
	$objForm.Controls.Add($objText) 
	
	#Let's fill the listbox with the resourcepool list
	#First check if there are any resource pools
	$FirstresourcePool = Get-ResourcePool -Server $Server
	foreach ($ResourcePool in Get-ResourcePool -Server $Server | where-Object {$_.Name -ne "Resources"})
		{[void] $objListBox.Items.Add($ResourcePool.Name)}
	#This is the point where the form becomes active and the script flow stops.
	if ($objListBox.Items.Count -gt 0) 
		{[void] $objForm.ShowDialog()}
	else 
		{Write-Output "No Resource Pools available"
		$Voice.Speak( "No Resource Pools available......", 1 )}
	#At this point we get the flow back, somebody clicked the Exit or Cancel button. Let's say goodbye and close te connection
	$Voice.Speak( "Bye, see you in Vegas greetings Eric Sloof", 1 )
	Disconnect-viserver -Server $Server -Confirm:$false}
