﻿

function set-iisPoolIdentity {
 [cmdletbinding()]

param(
[parameter(mandatory="true")] 
[string]
$userNameToSearch,

[parameter(mandatory="true")] 
[string]
$userNameToReplace,

[parameter(mandatory="true")] 
[string]
$password
)

$securedPassword = $password | ConvertTo-SecureString -AsPlainText -Force
Import-Module webadministration

$Credentials = New-Object System.Management.Automation.PSCredential `
     -ArgumentList $userNameToReplace, $securedPassword


Start-Sleep -Seconds 1

$Pools = Get-ChildItem "iis:\AppPools" | Where-Object{$_.processModel.username -match [Regex]::Escape($userNameToSearch) }

foreach($pool in $Pools)
{
        $pool.processModel.userName = $Credentials.UserName
        $pool.processModel.password =  $Credentials.GetNetworkCredential().Password
        $pool.processModel.identitytype = 3
        $pool | Set-Item
}
}
