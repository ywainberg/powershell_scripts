﻿#Log in to Microsoft online services

$LiveCred = Get-Credential -Message 'Enter Global Admin credentials'
$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://outlook.office365.com/powershell-liveid/ -Credential $LiveCred -Authentication Basic -AllowRedirection
Import-PSSession $Session -AllowClobber
Connect-MsolService -Credential $LiveCred

#set Variables for Your License
#change the Numbers in the [] according to amount of license that you have
#run the 'Get-MsolAccountSku' to set the location of each license 

$SKU = Get-MsolAccountSku 
$old = $SKU[2].AccountSkuId 
$new = $SKU[1].AccountSkuId 



$users = Get-MsolUser -MaxResults 5000 | Where-Object { $_.isLicensed -eq "TRUE" -and $_.UsageLocation -eq 'LocationID' } 

foreach($user in $users)

{
  if($user.Licenses.AccountSkuId -eq $old){
 
  
  Set-MsolUserLicense -UserPrincipalName $user.UserPrincipalName -RemoveLicenses $old
  Set-MsolUserLicense -UserPrincipalName $user.UserPrincipalName -AddLicenses $new  
  }
  }
