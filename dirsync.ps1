﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2016 v5.2.124
	 Created on:   	20/07/2016 14:47
	 Created by:   	yaronw
	 Organization: 	example
	 Filename:     	
	===========================================================================
	.DESCRIPTION
		Active Directory sync with Azure AD connect tool for hybrid deployment.
#>

function start-dirsync
{
	[cmdletbinding()]
	param (
		[Parameter(Mandatory = $true,
							 ValueFromPipeline = $true,
							 ValueFromPipelineByPropertyName = $true,
							 HelpMessage = 'Enter dirsync computer name')]
		[string]$ComputerName,
		
		[Parameter(Mandatory = $true)]
		[ValidateSet('Initial', 'Delta')]
		[string]$SyncType
	)
	BEGIN { }
	PROCESS
	{
		
		
		try
		{
			if ($SyncType -eq 'Initial')
			{
				Invoke-Command -ComputerName $ComputerName -ScriptBlock {
					
					
					Import-Module "C:\Program Files\Microsoft Azure AD Sync\Bin\ADSync\ADSync.psd1"
					
					Write-Verbose 'Starting Full Sync'
					Start-ADSyncSyncCycle -PolicyType Initial
				} -ErrorAction Stop
				
			}
			elseif ($SyncType -eq 'Delta')
			{
				Invoke-Command -ComputerName $ComputerName -ScriptBlock {
					
					
					Import-Module "C:\Program Files\Microsoft Azure AD Sync\Bin\ADSync\ADSync.psd1"
					
					Write-Verbose 'Start Only Delta Sync'
					Start-ADSyncSyncCycle -PolicyType Delta
				} -ErrorAction Stop
				
				
				
			}
		}
		catch
		{
			Write-Verbose -Message "Connection to $ComputerName failed"
			
		}
		
		finally
		{
			$Event = Get-WinEvent -ComputerName $ComputerName -LogName application -MaxEvents 100 | `
			Where-Object{
				$_.ProviderName -eq 'Directory Synchronization' -and `
				$_.Message -match 'Scheduler::StartSyncCycle : Started sync cycle.' -and `
				$_.TimeCreated -gt (Get-Date).AddMinutes(-1)
			}
			
			if ($Event) { Write-Output  "`nStarted sync cycle at "$Event.TimeCreated"" }
			else { Write-Output "`n Sync did not start`n  `n Please check that the serevr in running and you have access permission`n `n Also check the Application log file`n `n for Directory Synchronization errors`n" }
			
		}
	}
	END { }
}

